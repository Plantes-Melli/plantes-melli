package fr.iut.plantes_melli.modele;

import android.annotation.SuppressLint;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Environment;
import android.util.Log;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.collections4.Equator;
import org.json.JSONArray;
import org.json.JSONException;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Serializable;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;

import androidx.room.Database;
import androidx.room.RoomDatabase;
import io.reactivex.subjects.PublishSubject;

@Database(entities = {Plante.class,PlanteId.class}, version = 1)
public abstract class AppDatabase extends RoomDatabase implements Serializable {
    public abstract  PlanteDAO planteDAO();
    public abstract ListeDAO listeDAO();

    @SuppressLint("StaticFieldLeak")
    public void update() {
        new AsyncTask() {
            @Override
            protected Object doInBackground(Object[] objects) {
                Log.d("UPDATE","Checking for new version");
                try {
                    StringBuffer ressource = getRessource("http://192.168.43.5/api/plantes.php");
                    if (ressource == null) { //impossible de contacter le serveur
                        DataBaseFactory.setState(DataBaseFactory.DATABASE_STATE.UPDATED);
                        return false;
                    }

                    JSONArray plantes = new JSONArray(ressource.toString());

                    Collection<Plante> nouvellesPlantes = new ArrayList<Plante>();
                    for (int i = 0; i < plantes.length(); i++) {
                        nouvellesPlantes.add(new Plante(plantes.getJSONObject(i)));
                    }

                    Collection<Plante> plantesActuelles = planteDAO().getAll();

                    //On supprime toutes les plantes qui ne sont pas dans la liste nouvellesPlantes
                    planteDAO().deletePlantes(CollectionUtils.removeAll(plantesActuelles,nouvellesPlantes).toArray(new Plante[plantesActuelles.size()]));


                    //on recupere les nouvelles versions de plantes existantes
                    nouvellesPlantes = CollectionUtils.removeAll(nouvellesPlantes, plantesActuelles, new Equator<Plante>() {
                        @Override
                        public boolean equate(Plante plante, Plante t1) {
                            return plante.hash == t1.hash;
                        }

                        @Override
                        public int hash(Plante plante) {
                            return plante.hashCode();
                        }
                    });
                    //remplace les 'vieilles' plantes
                    planteDAO().insertPlantes(nouvellesPlantes.toArray(new Plante[nouvellesPlantes.size()]));


                    Log.i("AppDatabase", "Looking for new images");


                    File sdCard = Environment.getExternalStorageDirectory();
                    File normalDir = new File(sdCard.getAbsolutePath() + "/plantesmelli/res/images/normal/");
                    File miniDir = new File(sdCard.getAbsolutePath() + "/plantesmelli/res/images/mini/");
                    normalDir.mkdirs();
                    miniDir.mkdirs();


                    final List<String> listenormal = Arrays.asList(normalDir.list());
                    final List<String> listemini = Arrays.asList(miniDir.list());

                    //pour chaque plante dans la bdd apres mise a jour
                    for (Plante p : planteDAO().getAll()) {

                        //l'image n'a pas été téléchargé ou la plante a été mise a jour
                        if (!listenormal.contains(p.getImage()) || (nouvellesPlantes.contains(p)) ) {
                            downloadRessource("http://192.168.43.5/vue/imagesPlantes/", p.getImage(), "normal");
                        }

                        //pareil
                        if (!listemini.contains(p.getImage()) || (nouvellesPlantes.contains(p))) {
                            downloadRessource("http://192.168.43.5/vue/miniatures/", p.getImage(), "mini");
                        }
                    }


                } catch (JSONException e) {
                    System.err.println(e);
                }

                DataBaseFactory.setState(DataBaseFactory.DATABASE_STATE.UPDATED);
                System.out.println("DONE");
                return true;
            }

        }.execute();

    }


    //recupere une ressource distante (un fichier JSON dans notre cas)
    public StringBuffer getRessource(String urlS) {
        try {
            URL url = new URL(urlS);
            URLConnection urlConnection = url.openConnection();
            urlConnection.setConnectTimeout(1000);
            InputStream stream = urlConnection.getInputStream();
            String str;

            StringBuffer buf = new StringBuffer();
            try {
                BufferedReader reader = new BufferedReader(new InputStreamReader(stream));
                if (stream != null) {
                    while ((str = reader.readLine()) != null) {
                        buf.append(str + "\n");
                    }
                }
            } finally {
                try {
                    stream.close();
                } catch (Throwable ignore) {
                }
            }

            return buf;

        } catch (Exception ex) {
            System.err.println(ex.toString());
        }

        return null;

    }

    //Télécharge une image
    public void downloadRessource(final String url, final String fileName, final String folder) {
        new AsyncTask() {
            @Override
            protected Boolean doInBackground(Object[] objects) {
                try {
                    URL r_url = new URL(url + fileName);
                    Bitmap image = BitmapFactory.decodeStream(r_url.openConnection().getInputStream());
                    File sdCard = Environment.getExternalStorageDirectory();

                    File dir = new File(sdCard.getAbsolutePath() + "/plantesmelli/res/images/" + folder + "/");
                    dir.mkdirs();

                    File imageFile = new File(dir, fileName);
                    FileOutputStream fs = new FileOutputStream(imageFile,false); //false = reecrire le fichier si il existe

                    String temp[] = fileName.split("\\.");

                    Log.d("Download", fileName + "...");

                    String formatImg = temp[temp.length - 1];
                    Bitmap.CompressFormat format = null;

                    if (formatImg == "png") {
                        format = Bitmap.CompressFormat.PNG;
                    } else {
                        format = Bitmap.CompressFormat.JPEG;
                    }

                    image.compress(format, 90, fs); //compression de l'image, maxVal = 100
                    fs.close();
                    return true;

                } catch (MalformedURLException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }
                return false;
            }

            protected void onPostExecute(Boolean b) {
                super.onPostExecute(b);
                Log.d("AppDataBase - Download", "Image dowloaded: " + b);
            }
        }.execute();
    }
}
