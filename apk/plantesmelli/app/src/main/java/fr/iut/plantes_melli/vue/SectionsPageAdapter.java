package fr.iut.plantes_melli.vue;

import java.util.ArrayList;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;

//Adapter pour gerer les onglets
class SectionsPageAdapter extends FragmentPagerAdapter {
    private final List<Fragment> framentsList = new ArrayList<>();
    private final  List<String> fragmentTitleList = new ArrayList<>();

    public SectionsPageAdapter(@NonNull FragmentManager fm) {
        super(fm);
    }

    public void addFragment(Fragment fr, String title){
        framentsList.add(fr);
        fragmentTitleList.add(title);
    }
    @Override
    public CharSequence getPageTitle(int position){
        return fragmentTitleList.get(position);
    }


    @NonNull
    @Override
    public Fragment getItem(int position) {
        return  framentsList.get(position);
    }

    @Override
    public int getCount() {
        return  framentsList.size();
    }
}
