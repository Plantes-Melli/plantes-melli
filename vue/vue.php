<?php



class Vue{
	private $mois =array("1" => "Janvier",
	"2" => "Fevrier",
	"3" => "Mars",
	"4" => "Avril",
	"5" => "Mai",
	"6" => "Juin",
	"7" => "Juillet",
	"8" => "Août",
	"9" => "Septembre",
	"10" => "Octobre",
	"11" => "Novembre",
	"12" => "Decembre");


	function afficherHead(){
		?>
		<html>
		<head>
			<title>Plantes mellifères</title>
			<!--<BASE href="<?php echo SITENAME;?>">  -->
			<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
			<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
			<script src='https://www.google.com/recaptcha/api.js'></script>

			<link href="https://fonts.googleapis.com/css?family=Kanit:200" rel="stylesheet">
			<link href="https://fonts.googleapis.com/css?family=Quicksand:300,400" rel="stylesheet">




			<script src="controleur/js/jquery-1.12.4.js"></script>
			<script src="controleur/js/jquery-ui.js"></script>
			<link href="vue/css/style.css" rel="stylesheet" type="text/css" media="all" />
			<link rel="stylesheet" href="vue/css/jquery-ui.css">
			<script src="controleur/js/slider.js"></script>
			<script src="controleur/js/cookiesManager.js"></script>

			<script src="controleur/js/autocompletion.js"></script>


			<noscript>
				<?php
				if(!isset($_GET['page'])){
					echo "<meta http-equiv=\"refresh\" content=\"0; url=index.php?page=errorjs\" />";
				}else{
					if($_GET['page'] != "errorjs"){
						echo "<meta http-equiv=\"refresh\" content=\"0; url=index.php?page=errorjs\" />"; //pas de script et sur une page autre que errorjs
					}
				}

				?>
			</noscript>
		</head>
		<body>
			<?php
		}


		function afficherMenu(){
			?>
			<div id="header">
				<!-- start slider -->
				<div id="slider">
					<!-- Slideshow container -->
					<div class="slideshow-container">
						<?php
						$baseFolder = "vue/uploads/slider";
						$scanned_directory = array_diff(scandir($baseFolder), array('..', '.'));
						$nbSlides = count($scanned_directory);
						$i = 0;
						foreach($scanned_directory as $image){
							$style ="";
							if($i ==0){
								$style = "style='display:block'";
							}

							$i++;
							?>
							<!-- Full-width images with number and caption text -->
							<div class="mySlides fade" <?php echo $style; ?>>

								<img src="<?php echo $baseFolder.'/'.$image;?>" class="slide">
							</div>
							<?php
						}
						?>
						<!-- Next and previous buttons -->
						<a class="prev" onclick="plusSlides(-1)">&#10094;</a>
						<a class="next" onclick="plusSlides(1)">&#10095;</a>
					</div>

					<!-- The dots/circles -->
					<div id ="dots">
						<?php
						for( $i= 0; $i < $nbSlides;$i++){
							echo '<span class="dot" onclick="currentSlide('.$i.')"></span>';
						}
						?>
					</div>
				</div>

				<!-- start header -->
				<div id="top-nav-placeholder">
				</div>
				<div class="header" id="header_bg">
					<?php
					?>
					<div class="wrap">
						<div class="menu"  id="customMenu">
							<div class="wrapperMenu" id="wrapperMenu" >
								<ul id="top-nav">
									<a  href="index.php?page=Accueil"><li>Accueil</li></a>
									<a  href="index.php?page=Plantes"><li>Plantes</li></a>
									<a  class="hsubs" href="index.php?page=Compte"><li>Compte</li></a>
								</ul>
							</div>
							<div id="menu_placeHolder"></div>
							<script type="text/javascript">
									function closeMenu(event){
											menu = document.getElementById("top-nav");
											placeholder = document.getElementById("menu_placeHolder");
											console.log(this.id+" "+menu.className);
											if(menu.className == "visible"){
												 menu.className = "invisible";
												 placeholder.className = "invisible";
											}else{
												menu.className = "visible";
												placeholder.className = "visible";
											}
									}


									document.getElementById("wrapperMenu").addEventListener("click",closeMenu,false);
									document.getElementById("menu_placeHolder").addEventListener("click",closeMenu,false);
							</script>

							<a id="titreSiteAlternatif" href="index.php?page=Accueil"><h1 >Plantes Mellifères</h1></a>


						</div>
						<div id="logos">
							<?php echo $this->getLink(FACEBOOK_LINK,'facebook');?>
							<?php echo $this->getLink(TWITTER_LINK,'twitter');?>
							<?php echo $this->getLink(INSTAGRAM_LINK,'instagram');?>
							<?php echo $this->getLink(PINTEREST_LINK,'pinterest');?>
						</div>
					</div>
				</div>

				<script src="controleur/js/sticky.js"></script>
			</div>
			<div id="content">
				<?php
			}

			function afficherDeconnexion($pseudo){
				if ($pseudo != NULL ){?>
					<div class="popUpConnecte">
						<?php echo "$pseudo"; ?>
						<form action="index.php?page=Accueil" method="POST">
							<button type="submit" name="deconnexion"/>Se déconnecter</button>
						</form>
					</div>
					<?php
				}
			}

			function afficherFooter(){
				?>
			</div>
			<div class="footer">
				<p>
				   <a href="index.php?page=MentionsLégalesCGU" > Mentions Légales / CGU</a> |
				   <a href="index.php?page=PolitiqueConfidentialite" > La politique de confidentialité</a> |
				   <a href="index.php?page=SavoirPlus">En savoir plus</a> |
				   <a href="index.php?page=Contact">Contact</a> |
				   <a href="https://iutnantes.univ-nantes.fr/iut-nantes/">IUT Nantes</a>
				</p>
				<p>Plantes Mellifères  ©  2018-2019</p>
			</div>
		</body>
		</html>
		<?php
	}

	function afficheContact(){
		?>
		<html>
		<header>
		</header>
			<body class="rangement">
				<h1 style="text-align: center">Contactez-nous</h1>
					<h2>Contributeurs</h2><br/>
					<p>M. Hadj-Rabia : nassim.hadjrabia@univ-nantes.fr <p>
					<p>M. Supinski : romain.supinski@etu.univ-nantes.fr <p>
					<p>M. Viaud : lois.viaud@etu.univ-nantes.fr <p>
					<p>M. Gauthier : thomas.gauthier@etu.univ-nantes.fr <p>
					<p>M. Soulard : axel.soulard@etu.univ-nantes.fr <p>
					<br>
				<h2>IUT Nantes</h2></br>
					<p>Adresse : 3 Rue Maréchal Joffre, 44000 Nantes<p>
					<p>Téléphone : 02 40 30 60 90</p>
			</body>
		</html>
		<?php
	}

	function afficheSavoirPlus(){
		?>
		<html>
		<header>
			<body class="rangement">
				<h1 style="text-align:center">En savoir plus</h1>
			</br>
			<p style="text-align:justify">Le site plantes_Mellifère est un projet réalisé dans le cadre du semestre 3 du DUT Informatique à l'IUT de Nantes, pour développer nos compétences
				en terme de programmation frontend / backend aux niveau web. Notre but était d'améliorer le site déjà existant, l'améliorer en terme
				de design, responsivité, ergonomie ainsi que des rajouts de foncitonnalités. Ce projet tuteuré réalisé
				sous la supervision de M Hadj Rabia.</p>
			</body>
		</header>
		</html>

		<?php
	}
		function afficheMentionsLégalesCGU(){
			?>
			<html>
			<header>
			</header>
				<body>
					<div class="rangement">
						<h1 style="text-align:center">MENTIONS LEGALES / CGU :</h1>
					</br>
						<p style="text-align:justify">Conformément aux dispositions des articles 6-III et 19 de la Loi n° 2004-575 du 21 juin 2004 pour la Confiance dans l'économie numérique, dite L.C.E.N., nous portons à la connaissance des utilisateurs et visiteurs du site <a href="	../plantes-melli/index.php?page=Accueil" target="_blank">www.plantes_Mellifère.com</a> les informations suivantes :</p>
					</br>
					<hr></br>
						<h2 style="text-align:justify">1. Informations légales :</h2></br>
						<p style="text-align:justify">Statut du propriétaire : particulier<br/>
						Le Propriétaire est : l' IUT de Nantes<br />
						Adresse postale du propriétaire : IUT Nantes 44400 Nantes<br/>
						Les créateurs du site sont des étudiants de l'IUT Nantes<br/><br/></p>
					<hr></br>
						<h2 style="text-align:justify">2. Présentation et principe :</h2></br>
						<p style="text-align:justify">Tout internaute se connectant et utilisant le site susnommé : <a href="	../plantes-melli/index.php?page=Accueil" target="_blank">www.Plantes-Mellifère.com</a>.<br />
						Le site <strong>Plante-Mellifère</strong> regroupe un ensemble de services, dans l'état,  mis à la disposition des utilisateurs. Il est ici précisé que ces derniers doivent rester courtois et faire preuve de bonne foi tant envers les autres utilisateurs qu'envers le webmaster du site Plante-Mellifère sous peine d'avertissement ou de ban. Le site Plante-Mellifère est mis à jour régulièrement par l'équipe de plante mellifère.<br />
						Le support s’efforce de fournir sur le site Plante-Mellifère des informations les plus précises possibles (sous réserve de modifications apportées depuis leur mise en ligne), mais ne saurait garantir l'exactitude, la complétude et l'actualité des informations diffusées sur son site, qu’elles soient de son fait ou du fait des tiers partenaires qui lui fournissent ces informations. En conséquence, l'utilisateur reconnaît utiliser ces informations données (à titre indicatif, non exhaustives et susceptibles d'évoluer) sous sa responsabilité exclusive.</p><br/>
						<hr></br>
						<h2 style="text-align:justify">3. Accessibilité :</h2></br>
					  <p>Le site Plante-Mellifère est par principe accessible aux utilisateurs 24/24h, 7/7j, sauf interruption, programmée ou non, pour les besoins de sa maintenance ou en cas de force majeure. En cas d’impossibilité d’accès au service, le support s’engage à faire son maximum afin de rétablir l’accès au service et s’efforcera alors de communiquer préalablement aux utilisateurs les dates et heures de l’intervention.  N’étant soumis qu’à une obligation de moyen, le support ne saurait être tenu pour responsable de tout dommage, quelle qu’en soit la nature, résultant d’une indisponibilité du service.</p></br>
						<hr></br>
						<h2 style="text-align:justify">4. Propriété intellectuelle :</h2></br>
				    <p>L'IUT Nantes est propriétaire exclusif de tous les droits de propriété intellectuelle ou détient les droits d’usage sur tous les éléments accessibles sur le site, tant sur la structure que sur les textes, images, graphismes, logo, icônes, sons, logiciels…<br />
						Toute reproduction totale ou partielle du site Plante-Mellifère, représentation, modification, publication, adaptation totale ou partielle de l'un quelconque de ces éléments, quel que soit le moyen ou le procédé utilisé, est interdite, sauf autorisation écrite préalable de l'IUT de Nantes, propriétaire du site à l'email : plantes.mellifère@gmail.com, à défaut elle sera considérée comme constitutive d’une contrefaçon et passible de poursuite conformément aux dispositions des articles L.335-2 et suivants du Code de Propriété Intellectuelle.</p></br>
						<hr></br>
						<h2 style="text-align:justify">5. Liens hypertextes et cookies :</h2></br>
						<p>Le site Plante-Mellifère contient un certain nombre de liens hypertextes vers d’autres sites (partenaires, informations …) mis en place avec l’autorisation de l'IUT. Cependant, le support n’a pas la possibilité de vérifier l'ensemble du contenu des sites ainsi visités et décline donc toute responsabilité de ce fait quand aux risques éventuels de contenus illicites.<br />
						L’utilisateur est informé que lors de ses visites sur le site Plante-Mellifère, un ou des cookies sont susceptibles de s’installer automatiquement sur son ordinateur par l'intermédiaire de son logiciel de navigation. Un cookie est un bloc de données qui ne permet pas d'identifier l'utilisateur, mais qui enregistre des informations relatives à la navigation de celui-ci sur le site. <br />
						Le paramétrage du logiciel de navigation permet d’informer de la présence de cookie et éventuellement, de la refuser de la manière décrite à l’adresse suivante : <a href="http://www.cnil.fr">www.cnil.fr</a>. L’utilisateur peut toutefois configurer le navigateur de son ordinateur pour refuser l’installation des cookies, sachant que le refus d'installation d'un cookie peut entraîner l’impossibilité d’accéder à certains services. Pour tout bloquage des cookies, tapez dans votre moteur de recherche : bloquage des cookies sous IE ou firefox et suivez les instructions en fonction de votre version.</p><br/>
						<hr></br>
						<h2 style="text-align:justify">6. Protection des biens et des personnes - gestion des données personnelles :</h2></br>
						<p>En France, les données personnelles sont notamment protégées par la loi n° 78-87 du 6 janvier 1978, la loi n° 2004-801 du 6 août 2004, l'article L. 226-13 du Code pénal et la Directive Européenne du 24 octobre 1995.</p>
						<p style="text-align:justify">Sur le site Plante-Mellifère, l'IUT Nantes ne collecte des informations personnelles ( suivant l'article 4 loi n°78-17 du 06 janvier 1978) relatives à l'utilisateur que pour le besoin de certains services proposés par le site Plante-Mellifère. L'utilisateur fournit ces informations en toute connaissance de cause, notamment lorsqu'il procède par lui-même à leur saisie. Il est alors précisé à l'utilisateur du site Plante-Mellifère l’obligation ou non de fournir ces informations.<br />
						Conformément aux dispositions des articles 38 et suivants de la loi 78-17 du 6 janvier 1978 relative à l’informatique, aux fichiers et aux libertés, tout utilisateur dispose d’un droit d’accès, de rectification, de suppression et d’opposition aux données personnelles le concernant. Pour l’exercer, adressez votre demande à Plante-Mellifère par email : <strong><a href="plantes-mellifère@gmail.com">plantes.mellifere@gmail.com</a></strong> ou par écrit dûment signée, accompagnée d’une copie du titre d’identité avec signature du titulaire de la pièce, en précisant l’adresse à laquelle la réponse doit être envoyée.</p>

							<p style="text-align:justify">Aucune information personnelle de l'utilisateur du site Plante-Mellifère n'est publiée à l'insu de l'utilisateur, échangée, transférée, cédée ou vendue sur un support quelconque à des tiers. Seule l'hypothèse du rachat du site Plante-Mellifère et de ses droits autorise l'IUT Nantes à transmettre les dites informations à l'éventuel acquéreur qui serait à son tour tenu à la même obligation de conservation et de modification des données vis à vis de l'utilisateur du site Plante-Mellifère.<br /></p>
							<p style="text-align:justify">Les bases de données sont protégées par les dispositions de la loi du 1er juillet 1998 transposant la directive 96/9 du 11 mars 1996 relative à la protection juridique des bases de données.</p>
						</div>
				</body>
			</html>

			<?php
		}

		function affichePolitiquesConfidentialites(){
		?>
		<html>
	<body>
				<div class="rangement">
				<h1 style="text-align:center">Politique modèle de confidentialité</h1><br/>
				<h2>Introduction</h2><br />
				Devant le développement des nouveaux outils de communication, il est nécessaire de porter une attention particulière à la protection de la vie privée.
				C'est pourquoi, nous nous engageons à respecter la confidentialité des renseignements personnels que nous collectons.
			</p><br/><hr/></br>
				<h2>1. Collecte des renseignements personnels</h2></br>
				<p>Nous collectons les renseignements suivants :</p>
				<ul>
				<li>Nom</li>
				<li>Prénom</li>
				<li>Adresse électronique</li>
				<li>Genre / Sexe</li>
				<li>âge / Date de naissance</li>
				<li>Origines (sociales, ethniques, etc.)</li
				</ul>
		</ul>
	<p>
		Les renseignements personnels que nous collectons sont recueillis au travers de formulaires et grâ;ce à
		l'interactivité établie entre vous et notre site Web.
		Nous utilisons également, comme indiqué dans la section suivante, des fichiers témoins et/ou journaux
		pour réunir des informations vous concernant.
	</p></br><hr/></br>
	<h2>2. Formulaires et interactivité </h2></br>
	<p>
		Vos renseignements personnels sont collectés par le biais de formulaire, à savoir :
	</p>
		<ul>
						<li>Formulaire d'inscription au site Web</li>
				</ul>
	<p>
		Nous utilisons les renseignements ainsi collectés pour les finalités suivantes :
	</p>
	<ul>
				<li>Statistiques</li>
				<li>Contact</li>
				<li>Gestion du site Web (présentation, organisation)
	</li>
		</ul>
	<p>
		Vos renseignements sont également collectés par le biais de l'interactivité pouvant s'établir entre vous et notre site Web et ce, de la fa&ccedil;on suivante:
	</p>
	<ul>
				<li>Contact</li>
				<li>Gestion du site Web (présentation, organisation)</li>
		</ul>
	<p>
		Nous utilisons les renseignements ainsi collectés pour les finalités suivantes :<br />
	</p>
		<ul>
						<li>Forum ou aire de discussion</li>
						<li>Commentaires
	</li>
						<li>Correspondance</li>
				</ul>
	</br><hr/></br>
	<h2>3. Fichiers journaux et témoins </h2></br>
	<p>
	Nous recueillons certaines informations par le biais de fichiers journaux (log file) et de fichiers témoins (cookies). Il s'agit principalement des informations suivantes :
	</p>
	<ul>
							<li>Adresse IP</li>
									<li>Pages visitées et requêtes</li>
									<li>Heure et jour de connexion</li>
				</ul>

	<br />
	<p>
	Le recours à de tels fichiers nous permet :
	</p>
	<ul >
				<li>Amélioration du service et accueil personnalisé</li>
				<li>Profil personnalisé de consommation</li>
				<li>Suivi de commande</li>
				<li>Statistique</li>
		</ul></br><hr/></br>
	<h2>4. Droit d'opposition et de retrait</h2></br>
	<p>
		Nous nous engageons à vous offrir un droit d'opposition et de retrait quant à vos renseignements personnels.<br />
		Le droit d'opposition s'entend comme étant la possiblité offerte aux internautes de refuser que leurs renseignements
		personnels soient utilisées à certaines fins mentionnées lors de la collecte.<br />
	</p>
	<p>
		Le droit de retrait s'entend comme étant la possiblité offerte aux internautes de demander à ce que leurs
		renseignements personnels ne figurent plus, par exemple, dans une liste de diffusion.<br />
	</p>
	<p>
		Pour pouvoir exercer ces droits, vous pouvez : <br />
		Code postal : Maréchal Leclerc, Nantes 44000<br />	Courriel : 02 40 30 60 90<br />	Téléphone : 02 40 30 60 90<br />	Section du site web : Plantes Mellifères/<br />	</p></br><hr/></br>
	<h2>5. Droit d'accès</h2></br>
	<p>
		Nous nous engageons à reconnaêtre un droit d'accès et de rectification aux personnes
		concernées désireuses de consulter, modifier, voire radier les informations les concernant.<br />


		L'exercice de ce droit se fera :<br />
		Code postal : Maréchal Leclerc, Nantes 44000<br />	Courriel : 02 40 30 60 90<br />	Téléphone : 02 40 30 60 90<br />	Section du site web : Plantes Mellifères/<br />	</p></br><hr/></br>
	<h2>6. Sécurité</h2></br>
	<p>

		Les renseignements personnels que nous collectons sont conservés
		dans un environnement sécurisé. Les personnes travaillant pour nous sont tenues de respecter la confidentialité de vos informations.<br />
		Pour assurer la sécurité de vos renseignements personnels, nous avons recours aux mesures suivantes :
	</p>
		<ul>
										<li>Protocole SSL (Secure Sockets Layer)</li>
													<li>Protocole SET (Secure Electronic Transaction) </li>
													<li>Gestion des accès - personne autorisée</li>
													<li>Gestion des accès - personne concernée</li>
													<li>Logiciel de surveillance du réseau</li>
													<li>Sauvegarde informatique</li>
													<li>Développement de certificat numérique</li>
													<li>Identifiant / mot de passe</li>
													<li>Pare-feu (Firewalls)</li>
							</ul>

	<p>
		Nous nous engageons à maintenir un haut degré de confidentialité en intégrant les dernières innovations technologiques permettant d'assurer
		la confidentialité de vos transactions. Toutefois, comme aucun mécanisme n'offre une sécurité maximale, une part de risque est toujours présente
		lorsque l'on utilise Internet pour transmettre des renseignements personnels.
	</p></br><hr/></br>
	<h2>7. Label</h2></br>
	<p>
		Nos engagements en matière de protection des renseignements personnels répondent aux exigences du programme suivant :
	</p>
	<ul>
						</ul></br><hr/></br>
	<h2>8. Législation</h2></br>
	<p>
		Nous nous engageons à respecter les dispositions législatives énoncées dans :
		<br />France
	</p>
		</div>
	</body>
	</html>

		<?php
	}


	function afficherChatBox(){
		?>
		<div id="chatBox">
			<div id="messages">
				<!-- afficher les messages dans une boucle for-->
			</div>
			<form id="message" action="" method="">
				<input type="text" placeholder="Votre message" <?php if(!isset($_SESSION["compte"])) echo "value='Vous devez être connecté pour envoyer des messages' disabled " ?> />
				<input type="submit" />
			</form>
		</div>
		<?php
	}

	function afficherBarreRecherche(){
		?>
		<div id="searchbar">
			<h1 class="searchbar-help">Quelle plante voulez-vous rechercher?</h1>
			<form action="index.php?page=Plantes" method="post">
				<input id="champPlante" class="champ" type="text" name="nomPlant" value="Eucalyptus..." onfocus="this.value = '';" onblur="if (this.value == '') {this.value = 'Eucalyptus...';}"/>
				<div class="radioboutonLangue">
					<input type="radio" checked="checked"  name="langRecherche" value="fr"> Nom Français</input>
					<input type="radio"  name="langRecherche" value="lat"> Nom Latin</input>
				</div>
				<input type="submit" class="bouton" name="recherchePlante" value="Rechercher"/>

			</form>
		</div>

		<?php
	}

	function afficherPanel($parties = "filtres",$filtre = null){
		$parties = explode("-",$parties);
		echo "<div id='sidePan'>";
		foreach($parties as $part){
			switch ($part) {
				case 'filtres':
				$this->afficherFiltres($filtre);
				break;
				case 'historique':
				$this->afficherHistorique($filtre);
				break;
			}
		}
		echo "</div>";
		?>
		<script src="controleur/js/filters.js"></script>
		<?php
	}

	function afficherFiltres($filtres){
		$traduction =  array( "type" => "Type" ,
		"famille" => "Famille",
		"inter_apicole" => "Interet apicole",
		"moyenne_kgHa" => "Moyenne Kg/Ha",
		"nectarifere" => "Nectarifère",
		"pollinifere" => "Pollinifere",
		"debut_floraison" => "Début floraison",
		"fin_floraison" => "Fin floraison");

		?>
		<fieldset class="sidePanElem">
			<legend class="visible" >Mode de Vue</legend>
			<form id="modeDeVue" action="index.php?page=Plantes" method="post">
				<input id="mosaic" class="typeVue" name="mosaic" type="submit" title="Vue en Mosaique" value="" >
				<input id="list" class="typeVue" name="list" type="submit"   title="Vue en Liste" value="">
			</form>
		</fieldset>

		<?php
		if (isset($_SESSION["compte"]) && $_SESSION["compte"]->hasPrivileges(ADMINISTRATOR) && isset($_POST["list"])){
			?>
			<fieldset class="sidePanElem">
				<legend class="visible" >Selection</legend>
				<form id="adminPanel" action="index.php?page=Plantes" method="post" onsubmit="return confirmAction();">
					<select id="action" name="selectType">
						<option selected value="supprimer">Supprimer</option>
					</select>
					<input type="hidden" name="list" />
					<button type="button" class="filterBtn" onclick="checkAll();">Tout sélectionner</button>
					<button type="button" class="filterBtn" onclick="reverse();">Inverser la sélection</button>
					<input type="submit" value="Valider"/>
				</form>
			</fieldset>
			<?php
		}
		?>

		<fieldset class="sidePanElem">
			<legend class="visible">Filtres</legend>


			<form  method="post">
				<input type="hidden" name="filters" value="true"/>
				<?php
				foreach ($filtres as $key => $filtre) {
					echo "<span>".$traduction[$key]."<br/>
					<select name='".$key."' >";
					echo "<option selected value='tous' >Tous</option>";
					foreach ($filtre as $key2 => $value) {
						$selected = "";

						if(isset($_POST[$key]) && $_POST[$key] == $value[$key] )
						$selected = "selected";

						if($key == "debut_floraison" || $key == "fin_floraison"){
							echo "<option ".$selected." value='".$value[$key]."'>". $this->mois[$value[$key]]."</option>";
						}else {
							echo "<option ".$selected." value='".$value[$key]."'>".$value[$key]."</option>";
						}

					}

					echo "</select></span>";
				}


				?>
				<br/>

				<span>Trier par:</span>
				<select name="tri">
					<option selected value='nomFr' >Nom Français</option>
					<option value='nomLat' >Nom Latin</option>
					<?php
					foreach ($filtres as $key => $filtre) {
						$selected ="";
						if(isset($_POST["tri"]) && $_POST["tri"] == $key )
						$selected = "selected";
						echo "<option ".$selected." value='".$key."' >".$traduction[$key]."</option><br/>";
					}
					?>
				</select>
				<div>

					<div>
						<input <?php if (isset($_POST["triV"]) && $_POST["triV"] == "asc"){echo "checked";} ?> type="radio" id="asc" name="triV" value="asc" checked>
						<label for="asc">Croissant</label>
					</div>

					<div>
						<input <?php  if (isset($_POST["triV"]) && $_POST["triV"] == "desc"){echo "checked";} ?> type="radio" id="desc" name="triV" value="desc">
						<label for="desc">Décroissant</label>
					</div>
				</div>
				<?php
				//TODO: a changer par list si le mode par defaut devient list
				$vue = "mosaic";
				if(isset($_POST["list"]))
				$vue = "list";

				echo "<input type='hidden' name='".$vue."'/>";
				echo "<input type='hidden' name='numPage' value='1'/>";
				?>
				<input type="submit" value="Filtrer" />
			</form>
		</fieldset>


		<?php


	}

	function afficherHistorique(){
		?>
		<fieldset class="sidePanElem">
			<legend class="visible">Historique</legend>
			<script type="text/javascript">
			ck = getCookie("historique");
			ck = ck.substring(12,ck.length);

			var ckArr = ck.split("|");

			ckArr = ckArr.filter(function(value, index, arr){
				return value != "";
			});  // => remove the void element after someting|


			for(var i = ckArr.length-1; i >=0;i--) {
				var valueAr = ckArr[i].split("_");
				document.write("<span class=\"histoLink\"><img class='micro' src='vue/miniatures/"+valueAr[2]+"' /><a href=\"index.php?page=Plantes&id="+valueAr[0]+"\">"+valueAr[1]+"</a></span>");
			}
			</script>
		</fieldset>
		<?php
	}

	function afficherPageAccueil($troisPlantesRand){

		echo "
		<h4>Plantes de Saison</h4>
		<div class='plantesAccueil'>";
		foreach ($troisPlantesRand as $key => $plante) {
			?>
			<div class="planteAccueil" onclick="window.location = 'index.php?page=Plantes&id=<?php echo $plante["id"]; ?>' ">
				<span class="plantWrapper">
					<img src="vue/imagesPlantes/<?php echo $plante["image"]?>" alt="photo de <?php echo $plante["nomFr"]?>">
				</span>
				<div>

					<p>Nom Français:<br/><span><?php echo $plante["nomFr"] ?></span></p>
					<p>Nom Latin:<br/><span><i><?php echo $plante["nomLat"] ?></i></span></p>
					<div class="hexagone2"><span class="value"><?php echo $plante["inter_apicole"] ?></span><span class="leg">Interet Apicole</span></div>
				</div>
			</div>
			<?php
		}

		echo "</div>";
	}

	function afficherListePlantes($tableauPlantes,$nbPages,$type="mosaic",$message = NULL){
		if($message !=NULL)
		echo "<center><p>".$message."</p></center><br/>";
		echo "<div class='miniatures' >";


		if($type == "mosaic" ){
			foreach($tableauPlantes as $plante){
				if(isset($_GET["user"]) && $_GET["page"] == "Listes"){
					?>

					<a class="wrapperMiniature" style='background: url(vue/miniatures/<?php echo $plante["image"] ?>); background-size:cover;' href="index.php?page=Listes&id=<?php echo $plante["id"];?>&user=<?php echo $_GET["user"];?>" >;
					<?php
				}else{
					?>
						<a class="wrapperMiniature" style='background: url(vue/miniatures/<?php echo $plante["image"] ?>); background-size:cover;' href="index.php?page=Plantes&id=<?php echo $plante["id"];?>" >;
					<?php
				}
				?>


					<div  >

						<div class='content'>
							<div class="text">
								<p><?php echo $plante["nomFr"]; ?></p>
							</div>

						</div>
					</div>
				</a>

				<?php
			}
		}else if($type == "list"){
			//Les tableaux c'est moche mais ordre du client :/
			echo "<div id='tableWrapper'> <table>";
			echo "<tr>
			<th>
			Photo
			</th>

			<th>
			Nom
			</th>

			<th>
			Interet Apicole
			</th>

			<th>
			Interet Nectarifère
			</th>
			<th>
			Interet Pollinifère
			</th>
			<th>
			Moyenne Kg/Ha
			</th>
			<th>
			Exposition
			</th>
			<th>
			Début de Floraison
			</th>
			<th>
			Fin de Floraison
			</th>";

			if (isset($_SESSION["compte"]) && $_SESSION["compte"]->hasPrivileges(ADMINISTRATOR)){
				echo "<th>Selection</th>";
			}
			echo "</tr>";
			foreach($tableauPlantes as $plante){
				?>

				<tr>

					<td><a href="index.php?page=Plantes&id=<?php echo $plante["id"];?>" ><img class="miniatureListe" src="vue/miniatures/<?php echo $plante["image"]; ?>">	</a> </td>
						<td><a href="index.php?page=Plantes&id=<?php echo $plante["id"];?>" ><p><?php echo $plante["nomFr"]; ?></p>	</a></td>
							<td><p><?php echo $plante["inter_apicole"]; ?></p></td>
							<td><p><?php echo $plante["nectarifere"]; ?></p></td>
							<td><p><?php echo $plante["pollinifere"]; ?></p></td>
							<td><p><?php echo $plante["moyenne_kgHa"]; ?></p></td>
							<td><p><?php echo $plante["exposition"]; ?></p></td>
							<td><p><?php echo $this->mois[$plante["debut_floraison"]]; ?></p></td>
							<td><p><?php echo $this->mois[$plante["fin_floraison"]]; ?></p></td>
							<?php
							if (isset($_SESSION["compte"]) && $_SESSION["compte"]->hasPrivileges(ADMINISTRATOR)){
								?> <td>
									<input form='adminPanel' type='checkbox' name="<?php echo $plante["id"];?>" />
								</td> <?php
							}
							?>
						</div>
					</div>
				</tr>
			</a>
			<?php
		}
		echo "</table></div>";
	}
	?>
</div>

<form method='post' class='pages'>


	<?php
	if ($nbPages > 0){
		foreach ($_POST as $key => $value) {
			echo "<input type='hidden' name='".$key."' value='".$value."' />";
		}
		for($i = 0; $i <= $nbPages;$i++){
			echo "<input type='submit' class='page' name='numPage' value='".intval($i+1)."'/>";
		}
	}
	echo "</form>";

}

function afficherPageConnexion($message = null){
	?>

	<!-- start main -->
	<div class="connexion">

		<div class="bandeauConnection">


			<form class="form" method="post"  action="index.php?page=Compte">
				<h3>Connectez-vous à votre compte</h3>
				<?php
				if($message != null){
					echo "
					<br/>
					<p class='errorMsg'>
					".$message."
					</p>
					";
				}
				?>
				<input class="txtConnection" name="pseudo" placeholder="Identifiant"  type="text" <?php if(isset($_POST["pseudo"])) echo "value='".$_POST["pseudo"]."'";?>>
				<input class="txtConnection" type="password" name="mdp" placeholder="Mot de passe"  type="password">
				<input id="boutonCoEnvoyer" type="submit" name="soumettre" value="Se connecter">
				<p >Vous n'avez pas de compte? <a href="index.php?page=inscription">Créer un compte</a></p>
				<p >Mot de passe oublié? <a href="index.php?page=forgotpass">Rénitialiser mon mot de passe</a></p>
			</form>
		</div>
	</div>
	<?php
}

function afficherEmailChForm($message = NULL){

	?>
		<form action="index.php?page=forgotpass" class="form" method="post">
			<h3>Renitialisation du mot de passe</h3>
			<?php
			if($message != null){
				echo "
				<br/>
				<p class='errorMsg'>
				".$message."
				</p>
				";
			}
			?>
			<input type="hidden" name="mode" value="sendMail" />
			<input type="email" class="txtConnection" name="email" placeholder="email@gmail.com *" />
			<input type="submit" class="bigButton" value="Envoyer" />
			<small><p>* Un lien de renitialisation vous sera envoyé à l'email fourni si nous trouvons un compte associé</p></small>
		</form>

	<?php
}


function afficherMDPChForm($message = NULL){

	?>
		<form action="index.php?page=forgotpass" class="form" method="post">
			<h3>Renitialisation du mot de passe</h3>
			<?php
				if($message != NULL){
					echo "<p class='errorMsg'>".$message."</p>";
				}
			 ?>
			<input type="hidden" name="mode" value="changePassword" />
			<input type="hidden" name="key" value="<?php if(isset($_GET["key"])){ echo $_GET["key"];}else{echo $_POST["key"];}?>"/>
			<input type="password" class="txtConnection" name="mdp" placeholder="motdepasse" />
			<input type="password" class="txtConnection" name="mdpConf" placeholder="motdepasse" />
			<input type="submit" class="bigButton" value="Changer mon mot de passe" />
		</form>

	<?php
}


function afficherPageInscription($message = null){
	?>


	<form id="registerForm" class="form" action="index.php?page=inscription" method="post" onsubmit="return verify()">
		<h3>Inscription</h3>

		<?php
		if($message != NULL){
			echo "<p class='errorMsg' >".$message."</p>";
		}
		?>

		<input onchange="verifierPseudo();" id="pseudoForm" class="txtConnection" name="pseudoIns" placeholder="Nom de compte" <?php if(isset($_POST["pseudoIns"])){echo "value=".$_POST["pseudoIns"];} ?> required>
		<input onchange="verifierPseudo();" id="pseudoForm" class="txtConnection" name="prenom" placeholder="Prenom" <?php if(isset($_POST["prenom"])){echo "value=".$_POST["prenom"];} ?> required>
		<input onchange="verifierPseudo();" id="pseudoForm" class="txtConnection" name="nom" placeholder="Nom" <?php if(isset($_POST["nom"])){echo "value=".$_POST["nom"];} ?> required>
		<input onchange="verifierEmail();" id="Email" class="txtConnection" name="Email" placeholder="Votre adresse mail" <?php if(isset($_POST["Email"])){echo "value=".$_POST["Email"];} ?> required>
		<input onchange="verifiermdp();" id="mdp1" class="txtConnection" type="password" name="mdpIns" placeholder="Votre mot de passe " required/>
		<input onchange="verifierConfMdp();" id="mdp2" class="txtConnection" type="password" name="mdpInsConf" placeholder="Confirmation du mdp" required/>
		<span id="lienCond"><input type="checkbox" name ="condition" required> J'accepte les <a href="index.php?page=MentionsLégalesCGU">Conditions générales d'utilisations</a></input></span>
		<div class="wrapperContent"> <div class="g-recaptcha" data-sitekey="<?php echo CAPTCHA_PUBLIC?>"></div>
	</div>
	<input id="boutonSoumettreIns" type="submit" name="soummtreInscri" value ="Créer compte" >
	<p>Vous avez déjà un compte? <a href="index.php?page=Compte" >Se connecter</a></p>
</form>
<?php
}

function afficherErreur(){
	?>
	<br/>
	<h1>Activez votre Javascript</h1>
	<br/>
	<p>Veuillez activer le javascript pour pouvoir acceder au site avec toutes ses fonctionnalitées <p/>


		<?php
	}



	function afficherMessage($titre,$message){

		?>
		<div id="messageWrapper">
			<h1><?php echo $titre; ?></h1>
			<p><?php echo $message;?></p>
			<br/>
			<p><small>Vous allez &ecirc;tre redirig&eacute; automatiquement dans 5 secondes</small> </p>
			<meta http-equiv="refresh" content="5; URL=index.php"/>
		</div>
		<?php
	}

	function afficherDetailPlante($plante){
			if(sizeof($plante) != 15){
				$this->afficherMessage("Plante Introuvable","La plante demandée n'existe pas ou a été supprimée.");
				return;
			}


		?>
		<script type="text/javascript">


		var ck = getCookie("historique");
		ck = ck.substring(12,ck.length);

		var ckArr = ck.split("|");

		ckArr = ckArr.filter(function(value, index, arr){
			return value != "";
		});  // => remove the void element after someting|

		var val= "<?php echo $plante['id'].'_'.$plante['nomFr'].'_'.$plante["image"];?>";
		if(val != ckArr[ckArr.length-1]){
			ckArr.push(val);
			while(ckArr.length > 10){
				ckArr.shift();
			}


			ck2 ="";
			var index = 0
			ckArr.forEach(function(element){
				ck2 = ck2+element+"|";
				index++;
			});

			ck2 = ck2.substring(0,ck2.length-1);
			setCookie("historique",ck2,1);
		}
		</script>

		<fieldset id="ficheDetail">
			<legend>
				<div class="legend">
					<div class="hexagon" style ="background-image: url(vue/imagesPlantes/<?php echo $plante["image"]?>);" >
						<div class="hexTop"></div>
						<div class="hexBottom"></div>
					</div>
					<div class="NomsPlante" >
						<h1><?php echo $plante["nomFr"]; ?></h1>
						<h3><?php echo $plante["nomLat"]; ?></h3>
						<h3>Famille: <?php echo $plante["famille"]; ?></h3>
					</div>
				</div>
			</legend>

			<ul class="caracs">
				<div id="firstElem" ><div class="hexagone2"><span class="value"><?php echo $plante["inter_apicole"] ?></span><span class="leg">Interet Apicole</span></div></div>
				<div><div class="hexagone2"><span class="value"><?php echo $plante["nectarifere"] ?></span><span class="leg">Nectarifère</span></div></div>
				<div><div class="hexagone2"><span class="value"><?php echo $plante["pollinifere"] ?></span><span class="leg">Pollinifere</span></div></div>
				<div><div class="hexagone2"><span class="value"><?php echo $plante["moyenne_kgHa"] ?></span><span class="leg">Moyenne kg/ha</span></div></div>
				<div><div class="hexagone2"><span class="value small"><?php echo $this->mois[$plante["debut_floraison"]] ?></span><span class="leg">Début de floraison</span></div></div>
				<div><div class="hexagone2"><span class="value small"><?php echo $this->mois[$plante["fin_floraison"]] ?></span><span class="leg">Fin de floraison</span></div></div>
				<div><div class="hexagone2"><span class="value small"><?php echo $plante["exposition"] ?></span><span class="leg">Exposition</span></div></div>
				<h2 class="caracDescri"> Description: </h2>
				<p class="resum">
					<?php echo $plante["description"]; ?>
				</p>
			</ul>


			<?php
			if(isset($_SESSION["compte"])){
				?>
				<h2 class="caracDescri"> Gestion de la Plante: </h2>
				<div class="plantBtn">
					<div class="usrPlantBtn">
						<form action="index.php?page=Plantes&id=<?php echo $plante["id"];?>" method="post">
							<?php
							if($_SESSION["compte"]->isInList($plante)){
								echo '<input type="submit" name="modifierListe" value="Modifier" />';
								echo '<input type="submit" name="supprimer" value="Supprimer" />';
							}else{
								echo '<input type="submit" name="ajouter" value="Ajouter" />';
							}
							?>
						</form>
					</div>

					<?php
					if($_SESSION["compte"]->hasPrivileges(ADMINISTRATOR)){
						?>
						<div class="adminPlantBtn">
							<strong>Modifications globales: </strong>
							<form action="index.php?page=Plantes&id=<?php echo $_GET["id"]?>" method="post">

								<input type="submit" name="selectType" value="Modifier" />
								<input type="hidden" name="<?php echo $_GET["id"]?>" value="on" />
								<input type="submit" name="selectType" value="Supprimer" />
							</form>
						</div>
						<?php
					}
					?>



				</div>
				<?php
			}
			?>

		</fieldset>
		<?php
	}

	function afficherModifsLocales($plante,$message=NULL){
		if($message != NULL){
			echo $message;
		}
		?>
		<script src="controleur/js/checkForm.js"></script>
		<fieldset id="ficheDetail">
			<legend>
				<div class="legend">
					<div class="hexagon" style ="background-image: url(vue/imagesPlantes/<?php echo $plante["image"]?>);" >
						<div class="hexTop"></div>
						<div class="hexBottom"></div>
					</div>
					<div class="NomsPlante" >
						<h1><?php echo $plante["nomFr"]; ?></h1>
						<h3><?php echo $plante["nomLat"]; ?></h3>
						<h3>Famille: <?php echo $plante["famille"]; ?></h3>
					</div>
				</div>
			</legend>

			<form class="form addPlantForm"  action="index.php?page=Plantes&id=<?php echo $_GET["id"]; ?>" method="post" enctype="multipart/form-data" onsubmit="return verifyPlantForm()">
					<?php

				echo "<label for='debut_floraison'>Debut Floraison</label>
				<select class='txtConnection' id='debut_floraison' name='debut_floraison' placeholder='Début floraison' required>";
				for($i = 1; $i <= 12;$i++){
					if($plante["debut_floraison"] == $i){
						echo "<option value='".$i."' selected>".$this->mois[$i]."</option>";
					}else{
						echo "<option value='".$i."'>".$this->mois[$i]."</option>";
					}

				}
				echo "</select>";

				echo "<label for='fin_floraison'>Fin Floraison</label>
				<select class='txtConnection' id='fin_floraison' name='fin_floraison' placeholder='Fin floraison' required>";
				for($i = 1; $i <= 12;$i++){
					if($plante["fin_floraison"] == $i){
						echo "<option value='".$i."' selected>".$this->mois[$i]."</option>";
					}else{
						echo "<option value='".$i."'>".$this->mois[$i]."</option>";
					}
				}
				echo "</select>";

				?>
				<input type="hidden" name="id" value="<?php echo $plante["id"];?>" />
				<input type="submit" class="bigButton" value="Modifier la plante" name="savePlantLocally">
				<a class="bigButton discardLink" href="index.php?page=Plantes&id=<?php echo $_GET["id"];?>">Annuler</a>
			</form>

		</fieldset>
		<?php
	}

	function afficherCompte($compte,$listes,$users = null,$message = null){


		if($users != null){
			$listUsers = "<datalist id='users' name='user' >";
			$listAdmins = "<datalist id='admins' name='admin' >";

			foreach ($users as $key => $user) {
				if($user["privileges"] == 0)
				$listUsers = $listUsers."<option value='".$user["pseudo"]."'>";
				else
				$listAdmins = $listAdmins."<option value='".$user["pseudo"]."'>";
			}

			$listUsers  = $listUsers."</datalist>";
			$listAdmins = $listAdmins."</datalist>";

			echo $listUsers;
			echo $listAdmins;
		}



		?>
		<h4>Mon Compte</h4>
		<div class="wrapperContent">
			<?php
			if($message != null)
			echo $message;
			?>
			<div class="menuBoutons">
				<div class="boutons">
					<button  onclick="openAll();">ouvrir tout</button>
					<button  onclick="closeAll();">fermer tout</button>
				</div>
			</div>
			<script src=""></script>
			<div class="compteSection" >
				<div class="titre" onclick="openTab(0);" >Gerer mon Compte <div> > </div></div>
				<section>
					<form class="form" action="index.php?page=Compte" method="post">
						<label for="pseudo">Pseudo: </label>
						<input id="pseudo" name="pseudo" class="txtConnection" type="text" value="<?php echo $compte->getPseudo();?>" <?php if(!isset($_POST["editProfil"])){ echo "disabled"; }?>/>

						<label for="prenom">Prenom: </label>
						<input id="prenom" name="prenom" class="txtConnection" type="text" value="<?php echo $compte->getPrenom();?>" <?php if(!isset($_POST["editProfil"])){ echo "disabled"; }?>/>

						<label for="nom">Nom: </label>
						<input id="nom" name="nom" class="txtConnection" type="text" value="<?php echo $compte->getNom();?>" <?php if(!isset($_POST["editProfil"])){ echo "disabled";} ?>/>

						<label for="email">Email: </label>
						<input id="email" name="email" class="txtConnection" type="text" value="<?php echo $compte->getMail();?>" <?php if(!isset($_POST["editProfil"])){ echo "disabled"; }?>/>
					  <input type="submit" value="Editer" />
						<?php
							if(!isset($_POST["editProfil"])){
								echo "<input type='hidden' name='editProfil' />";
							}else{
								echo '<a class="bigButton discardLink" href="index.php?page=Compte">Annuler</a>';
								echo "<input type='hidden' name='saveProfil' />";
							}

						?>

					</form>
				</section>
			</div>

			<div class="compteSection">
				<div class="titre" onclick="openTab(1);">Gerer ma Liste <div> > </div></div>
				<section>

					<form id="shareForm" action="index.php?page=Compte" method="post">

						<input type="hidden" name="mode" value="share"/>
						<label for="sharelist">Partager ma liste avec un utilisateur</label>
						<input type="text" id="sharelist" name="pseudo" placeholder="pseudo ou * pour une liste publique" />
						<input type="submit" value="partager" />
					</form>

					<?php
					foreach ($_SESSION["compte"]->getListPlants() as $key => $value) {
						echo "<a class='histoLink' href=\"index.php?page=Plantes&id=".$value["id"]."\">".$value["nomFr"]."</a><br/>";
					}
					?>
				</section>
			</div>

			<div class="compteSection">
				<div class="titre" onclick="openTab(2);">Listes Partagées <div> > </div></div>
				<section>
					<?php
					foreach ($listes as $key => $value) {
						echo "<a class='histoLink' href=\"index.php?page=Listes&user=".$value["src"]."\">".$value["src"]."</a><br/>";
					}
					?>
				</section>
			</div>

			<?php if($_SESSION["compte"]->hasPrivileges(ADMINISTRATOR)){ ?>
				<div class="compteSection">
					<div class="titre" onclick="openTab(3);" >Administration<div> > </div></div>
					<section>
						<div class="compteSection">
							<div class="titre" onclick="openTab(4);" >Ajouter une plante<div> > </div></div>
							<section>
								<script src="controleur/js/checkForm.js"></script>
								<form class="form addPlantForm"  action="index.php?page=Administration" method="post" enctype="multipart/form-data" onsubmit="return verifyPlantForm()">
									<input type="hidden" name="mode" value="addPlant" />
									<label for="fileToUploadBig">Photo</label>
									<div>
										<img  id="output1" class="imgPlant"/>
									</div>
									<input type="file" accept="image/*" id="fileToUploadBig" name="fileToUploadBig" class="fileToUpload" onchange="loadImage(event,1)" required />

									<label for="fileToUploadMini">Miniature</label>
									<div>
										<img  id="output2" class="imgPlant" />
									</div>
									<input type="file" accept="image/*" id="fileToUploadMini" name="fileToUploadMini" class="fileToUpload" onchange="loadImage(event,2)" required>
									<?php $this->contentPlantForm(NULL); ?>
									<input type="submit" class="bigButton" value="Ajouter la plante" name="submit">
								</form>

							</section>
						</div>
						<div class="compteSection">
							<div class="titre" onclick="openTab(5);" >Envoyer un mail aux utilisateurs<div> > </div></div>
							<section>
								<form class="form" method="post" action="index.php?page=Administration">
									<label for="Header">Sujet :</label><input type="text" class="txtConnection" placeholder="Ecrivez votre mail" name="Header" id="Header" value="<?php if(isset($_POST["Header"])){echo $_POST["Header"];}?>" required/>
									<label for="Body">Corps :</label><textarea type="" class="description" placeholder="Ecrivez votre mail" name="Body" id="Body" required/><?php if(isset($_POST["Body"])){echo $_POST["Body"];}?></textarea>
									<input type="hidden" name='mode' value="sendMailAllUser" />
									<input type="submit" />
								</form>
							</section>
						</div>
						<div class="compteSection">
							<div class="titre" onclick="openTab(6);" >Nommer un administrateur<div> > </div></div>
							<section>
								<form action="index.php?page=Administration" method="post">
									<label for="nameAdmin">Nom de l'utilisateur </label><input id="nameAdmin" list="users" name="users"/>
									<input type="hidden" name='mode' value="nameAdmin" />
									<input type="submit" />
								</form>
							</section>
						</div>
						<div class="compteSection">
							<div class="titre" onclick="openTab(7);" >Retrograder un administrateur <div> > </div></div>
							<section>
								<form action="index.php?page=Administration" method="post">
									<label for="demoteAdmin">Nom de l'administrateur </label><input id="demoteAdmin" list="admins" name="admins"/>
									<input type="hidden" name='mode' value="demoteAdmin" />
									<input type="submit" />
								</form>
							</section>
						</div>
						<div class="compteSection">
							<div class="titre" onclick="openTab(8);" >Bannir un utilisateur<div> > </div></div>
							<section>
								<form action="index.php?page=Administration" method="post">
									<label for="banUser">Nom de l'utilisateur </label><input id="banUser" list="users" name="users"/>
									<input type="hidden" name='mode' value="banUser" />
									<input type="submit" />
								</form>
							</section>
						</div>
						<div class="compteSection">
							<div class="titre" onclick="openTab(9);" >Gracier un utilisateur<div> > </div></div>
							<section>
								<form action="index.php?page=Administration" method="post">
									<label for="banUser">Nom de l'utilisateur </label><input id="banUser" list="users" name="users"/>
									<input type="hidden" name='mode' value="unbanUser" />
									<input type="submit" />
								</form>
							</section>
						</div>


					</section>
				</div>
			<?php } ?>
			<div class="compteSection">
				<a href="index.php?deconnexion" >Déconnexion</a>
			</div>
			<script src="controleur/js/compte.js"></script>
		</div>
	</div>

	<?php


}



public function afficherModifsGlobales($plante,$message){
		if($message != NULL){
			echo $message;
		}
	?>
	<script src="controleur/js/checkForm.js"></script>
	<fieldset id="ficheDetail">

		<form class="form addPlantForm"  action="index.php?page=Plantes&id=<?php echo $_GET["id"];?>" method="post" enctype="multipart/form-data" onsubmit="return verifyPlantForm()">
			<input type="hidden" name="selectType" value="savePlantGlobally"/>
			<label for="fileToUploadBig">Photo</label>
			<div>
				<img  id="output1" class="imgPlant" src="vue/imagesPlantes/<?php echo $plante["image"]?>"/>
			</div>
			<input type="file" accept="image/*" id="fileToUploadBig" name="fileToUploadBig" class="fileToUpload" onchange="loadImage(event,1)" />

			<label for="fileToUploadMini">Miniature</label>
			<div>
				<img  id="output2" class="imgPlant" src="vue/miniatures/<?php echo $plante["image"]?>" />
			</div>
			<input type="file" accept="image/*" id="fileToUploadMini" name="fileToUploadMini" class="fileToUpload" onchange="loadImage(event,2)">
			<input type="hidden" name="defaultLink" value="<?php echo $plante["image"]; ?>" />
			<?php $this->contentPlantForm($plante); ?>
			<input type="hidden" name="id" value="<?php echo $plante["id"]; ?>"/>
			<input type="submit" class="bigButton" value="Modifier la plante" name="submit">
			<a class="bigButton discardLink" href="index.php?page=Plantes&id=<?php echo $_GET["id"];?>">Annuler</a>
		</form>
	</fieldset>
	<?php
}

function contentPlantForm($plante = NULL){
	?>
				<script type="text/javascript">
				function loadImage(event,id){
					var output = document.getElementById("output"+id);
					output.src=URL.createObjectURL(event.target.files[0]);
				}
				</script>
				<label for="nomPlantFr">Nom de la plante</label>
				<input type="text" class="txtConnection" id="nomPlantFr" name="nomPlantFr" placeholder="Nom Français"  required value='<?php if(isset($_POST["nomPlantFr"])){echo $_POST["nomPlantFr"];}else if($plante != NULL){ echo $plante["nomFr"];}?>'/>
				<label for="nomPlantLat">Nom latin</label>
				<input type="text" class="txtConnection" id="nomPlantLat" name="nomPlantLat" placeholder="Nom Latin"  required value='<?php if(isset($_POST["nomPlantLat"])){echo $_POST["nomPlantLat"];}else if($plante != NULL){ echo $plante["nomLat"];}?>'/>
				<label for="famille">Famille</label>
				<input type="text" class="txtConnection" id="famille" name="famille" placeholder="Famille" required value='<?php if(isset($_POST["famille"])){echo $_POST["famille"];}else if($plante != NULL){ echo $plante["famille"];}?>'/>
				<label for="type">Type</label>
				<input type="text" class="txtConnection" id="type" name="type" placeholder="Type"  required value='<?php if(isset($_POST["type"])){echo $_POST["type"];}else if($plante != NULL){ echo $plante["type"];}?>'/>
				<label for="apicole">Interet Apicole</label>
				<input type="number" class="txtConnection" placeholder="Interet Apicole" name="apicole" id="apicole" required value='<?php if(isset($_POST["apicole"])){echo $_POST["apicole"];}else if($plante != NULL){ echo $plante["inter_apicole"];}?>'/>
				<label for="pollinifere">Interet Pollinifère</label>
				<input type="number" class="txtConnection" placeholder="Interet Pollinifere" name="pollinifere" id=="pollinifere" required value='<?php if(isset($_POST["pollinifere"])){echo $_POST["pollinifere"];}else if($plante != NULL){ echo $plante["pollinifere"];}?>'/>
				<label for="nectarifere">Interet Nectarifère</label>
				<input type="number" class="txtConnection" placeholder="Interet Nectarifère" name="nectarifere" id="nectarifere" required value='<?php if(isset($_POST["nectarifere"])){echo $_POST["nectarifere"];}else if($plante != NULL){ echo $plante["nectarifere"];}?>'/>
				<label for="moyenne_kgHa">Moyenne Kg/Ha</label>
				<input type="number" class="txtConnection" placeholder="Moyenne Kg/Ha" name="moyenne_kgHa" id="moyenne_kgHa" required value='<?php if(isset($_POST["moyenne_kgHa"])){echo $_POST["moyenne_kgHa"];}else if($plante != NULL){ echo $plante["moyenne_kgHa"];}?>'/>
				<label for="exposition">Exposition</label>
				<select class="txtConnection" name="exposition" id="exposition">
					<?php
					$array = ["Soleil","Mi-Ombre","Mi-Ombre/Soleil"];
					foreach($array as $key => $val){
						if(isset($_POST["exposition"])){
							if($_POST["exposition"] == $val)
								echo "<option value='".$val."' selected>".$val."</option>";
						}else{
							if($plante != NULL && $plante["exposition"] == $val)
								echo "<option value='".$val."' selected>".$val."</option>";
							else
								echo "<option value='".$val."'>".$val."</option>";
						}
					}
					?>
				</select>

				<?php

				echo "<label for='debut_floraison'>Debut Floraison</label>
				<select class='txtConnection' id='debut_floraison' name='debut_floraison' placeholder='Début floraison'>";
				for($i = 1; $i <= 12;$i++){
					if(isset($_POST["debut_floraison"])){
						if($_POST["debut_floraison"] == $i)
							echo "<option value='".$i."' selected>".$this->mois[$i]."</option>";
					}else{
							if($plante != NULL && $plante["debut_floraison"] == $i)
								echo "<option value='".$i."' selected>".$this->mois[$i]."</option>";
							else
								echo "<option value='".$i."'>".$this->mois[$i]."</option>";
					}

				}
				echo "</select>";

				echo "<label for='fin_floraison'>Fin Floraison</label>
				<select class='txtConnection' id='fin_floraison' name='fin_floraison' placeholder='Fin floraison'>";
				for($i = 1; $i <= 12;$i++){
					if(isset($_POST["debut_floraison"])){
						if($_POST["debut_floraison"] == $i)
							echo "<option value='".$i."' selected>".$this->mois[$i]."</option>";
					}else{
						if($plante != NULL && $plante["debut_floraison"] == $i)
							echo "<option value='".$i."' selected>".$this->mois[$i]."</option>";
						else
							echo "<option value='".$i."'>".$this->mois[$i]."</option>";
					}
				}
				echo "</select>";

				?>
				<label for="description">Description</label>
				<textarea type="" placeholder="Ecrivez la description de votre plante" name="description" class="description"  id="description" required/><?php if(isset($_POST["description"])){echo $_POST["description"];}else if($plante != NULL){ echo $plante["description"];}?></textarea>
<?php
}

function getLink($link,$imageName){
	if(!empty($link) && $link != "#"){
			return "<a href='".$link."' target='_blank'><img class='logo' src='vue/images/".$imageName.".png'/></a>";
	}
}
}

?>
