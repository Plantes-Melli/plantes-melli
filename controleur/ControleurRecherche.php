<?php

require_once PATH_MODELE."/DAO.php";
require_once PATH_VUE."/vue.php";



class ControleurRecherche{
	private $modele;
	private $vue;

	public function __construct() {
		$this->modele = new modele();
		$this->vue= new Vue();

	}
	//Afficher une plante
	public function afficherUneplante($lang,$mot,$page){


		if($lang=="fr"){

			$nomLatin=($this->modele->getNomLat($mot));
			$planteAAfficher =$this->modele->afficherPlante($nomLatin['nomLat']);
			$details=$this->modele->afficherInfoSuppPlante($nomLatin['nomLat']);

			return $this->vue->afficherInfoPlante($planteAAfficher,$details,$page);
		}
		else if($lang=="lat"){

			$details=$this->modele->afficherInfoSuppPlante($mot);

			return $this->vue->afficherInfoPlante($this->modele->afficherPlante($this->modele->levenshteinLat($mot)),$details,$page);

		}
		return null;

	}

	//Recherche réalisé par le biais de la barre de recherche, on utilise des méthode de complétion automatique avec l'utilisation d'algorithme comme levenshteinFr
	public function recherche($lang,$mot){
		if($lang=="fr"){
			return $this->modele->levenshteinFr($mot);
		}
		else if($lang=="lat"){
			return $this->modele->levenshteinLat($mot);

		}
		return null;

	}
}
