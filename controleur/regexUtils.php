<?php
class RegexUtils{
	function __construct(){
	}


		public static function isNumber($elem,& $message,$max = 5){
			if($elem >= 0 && $elem <= $max){
				return true;
			}else{
					$message = "<p class='errorMsg'>merci de saisir un nombre entre 0 et 5</p>";
					return false;
			}

		}

		public static function isText($elem,& $message){
			$regex = "/^[a-zA-ZÀ-ÖØ-öø-ÿœŒ\'\s]{3,}$/";
			if(preg_match($regex,$elem)){
				return true;
			}else{
				$message = "<p class='errorMsg'>caractère(s) interdi(s) dans un champs texte</p>";
				return false;
			}
		}

		public static function verifTextArea($elem,& $message){
					return true;
		}

		public static function isValidMonth($elem,& $message){
			if($elem >= 1 && $elem <= 12){
				return true;
			}else{
				$message ="<p class='errorMsg'>Le mois n'est pas valide</p>";
				return false;
			}
		}

		 public static function isValidExpo($exposition,& $message){
			 $array = ["Soleil","Mi-Ombre","Mi-Ombre/Soleil"];
			 if(in_array($exposition,$array)){
				 return true;
			 }else{
				$message = "<p class='errorMsg'>L'exposition selectionnée n'est pas valide</p>";
				 return false;
			 }
		 }

		 static function isUploadable($file, & $message){
	 		$uploadOk = true;


	 		if(empty($file["size"])){
	 			 $error = "L'image est vide";
	 			 $uploadOk = false;
	 		}

	 		$error = "";

	 		if(isset($file)){


	 			$imageFileType = strtolower(pathinfo($file["name"],PATHINFO_EXTENSION));



	 			// Check if image file is a actual image or fake image
	 			if(isset($_POST["submit"])) {
	 				$check = getimagesize($file["tmp_name"]);
	 				if($check === false) {
	 						$error = "Echec le fichier n'est pas une image";
	 						$uploadOk = false;
	 				}
	 			}
	 			// Check file size
	 			if ($file["size"] > 500000) {
	 				$error = "Echec le fichier est trop lourd";
	 				$uploadOk = false;
	 			}
	 			// Allow certain file formats
	 			if($imageFileType != "jpg" &&  $imageFileType != "jpeg" && $imageFileType != "png" ) {
	 				 $error = "Echec le fichier n'est ni du .jpg ni du .jpeg ni du png";
	 				 $uploadOk = false;
	 			}


	 			$message = "<p class='errorMsg'>.$error.</p>";
	 			return $uploadOk;

	 		}
	 	}


	 	static function upload( & $file,$path,$name){
	 		$target_file = $path.$name;
	 		move_uploaded_file($file["tmp_name"], $target_file);
	 		$file["name"] = $name;
	 	}

		static function checkPseudo($ndc){
			return preg_match('`^([a-zA-Z0-9-_]{2,36})$`', $ndc);
		}

		static function checkNom($ndc){
				return preg_match('`^([a-zA-Z\-]{2,20})$`', $ndc);
		}

		//!TODO: ajouter une liste de fournisseur d'emails banni ex: yopmail.com
		static function checkMail($mail){
			return filter_var($mail, FILTER_VALIDATE_EMAIL);
		}

		static function emailVerified($key){
			$this->vue->afficherVerification($this->modele->verifMail($key));
		}

		static function checkPassword($mdp){
				return preg_match('`^([a-zA-Z0-9\(\)\[\]\{\}\\\.\?\+,;/:!\-\*]{2,20})$`', $mdp);
		}
}




 ?>
