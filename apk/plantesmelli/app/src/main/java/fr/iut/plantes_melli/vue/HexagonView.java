package fr.iut.plantes_melli.vue;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.Rect;
import android.view.Gravity;
import android.view.View;
import android.widget.TextView;

import androidx.appcompat.widget.AppCompatTextView;
import fr.iut.plantes_melli.R;


//Regardez pas c'est complexe
//Un texte view en forme d'hexagone
public class HexagonView extends AppCompatTextView {
    private int radius;
    private String value;
    private int color;

    public HexagonView(Context context) {
        super(context);
        this.value = "";
        this.color = getResources().getColor(R.color.lightYellow);
        this.radius = 150;
    }

    public HexagonView(Context context, int radius) {
        this(context);
        this.radius = radius;
    }

    public HexagonView(Context context, int radius, String value) {
        this(context, radius);
        this.value = value;
        setText(this.value);
        setGravity(Gravity.CENTER);
        setTextColor(Color.BLACK);
        setTextSize(17);
    }

    public HexagonView(Context context, int radius, String value, int color) {
        this(context, radius);
        this.color = color;
        this.value = value;
        setText(this.value);
        setGravity(Gravity.CENTER);
        setTextColor(Color.BLACK);
        setTextSize(20);
    }


    //methode appelé par android quand le composant est ajouté a le ecran
    @Override
    protected void onDraw(Canvas canvas) {
        Paint paint = new Paint();
        paint.setColor(this.color);
        drawHexagon(canvas, paint, this.radius);
        super.onDraw(canvas);
    }

    //dessine un hexagone sur le canvas
    public void drawHexagon(Canvas canvas, Paint paint, int radius) {
        int centerX = radius;
        int centerY = radius;

        Path path = new Path();
        path.moveTo(radius * 2, centerY);
        for (int i = 0; i <= 6; i++) {
            path.lineTo(centerX + (int) (Math.cos(i * (Math.PI / 3)) * radius), centerY + (int) (Math.sin(i * (Math.PI / 3)) * radius));
        }


        path.close();

        canvas.drawPath(path, paint);
    }


    //calcul la taille que doit faire le texteView
    @Override
    public void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        setMeasuredDimension(this.radius * 2, this.radius * 2);
    }


}
