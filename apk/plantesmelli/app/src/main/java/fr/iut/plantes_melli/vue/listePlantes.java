package fr.iut.plantes_melli.vue;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;

import com.google.android.material.tabs.TabLayout;

import androidx.appcompat.app.AppCompatActivity;
import androidx.viewpager.widget.ViewPager;
import fr.iut.plantes_melli.R;

public class listePlantes extends AppCompatActivity {
    private SectionsPageAdapter mSectionsPageAdapter;
    private ViewPager viewPager;

    public static final String KEY_DB = "fr.iut.plantes_melli.vue.listePlantes.db";

    @SuppressLint("StaticFieldLeak")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_liste_plantes);
    }

    @Override
    protected void onResume() {
        super.onResume();


        mSectionsPageAdapter = new SectionsPageAdapter(getSupportFragmentManager());
        viewPager = findViewById(R.id.container);
        setupViewPager(viewPager);

        TabLayout tableLayout = findViewById(R.id.tabs);
        tableLayout.setupWithViewPager(viewPager);
    }

    //Ajoute les onglets sur la liste des plantes
    private void setupViewPager(ViewPager viewPager) {
        SectionsPageAdapter sectionsPageAdapter = new SectionsPageAdapter(getSupportFragmentManager());

        MozaicFragment mozaicFragment = new MozaicFragment();
        sectionsPageAdapter.addFragment(mozaicFragment, "Vue Mozaique");
        ListFragment listFragment = new ListFragment();
        sectionsPageAdapter.addFragment(listFragment, "Vue Liste");

        viewPager.setAdapter(sectionsPageAdapter);

    }






}
