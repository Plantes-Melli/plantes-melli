package fr.iut.plantes_melli.modele;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.widget.Toast;

import androidx.core.content.ContextCompat;
import androidx.room.Room;
import fr.iut.plantes_melli.R;
import io.reactivex.Observer;
import io.reactivex.disposables.Disposable;
import io.reactivex.subjects.BehaviorSubject;
import io.reactivex.subjects.ReplaySubject;


//Design pattern Singleton
//fabrique une base de donnée et la met a jour si possible
public class DataBaseFactory {

    public enum DATABASE_STATE {
        UNKNOWN,
        STARTING,
        BUILDING,
        UPDATING,
        UPDATED,
        ERROR
    }

    static AppDatabase db = null;
    static DATABASE_STATE state = DATABASE_STATE.UNKNOWN;
    static ReplaySubject<DATABASE_STATE> stateObservable;

    public DataBaseFactory() {
        stateObservable = ReplaySubject.create();
    }


    public static void createDataBase(Context context) {
        setState(DATABASE_STATE.STARTING);
        if (db == null) {
            setState(DATABASE_STATE.UPDATING);
            db = Room.databaseBuilder(context, AppDatabase.class, "info2-2016-melli-db").build();
            if (ContextCompat.checkSelfPermission(context, Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {
                db.update();
            } else {
                setState(DATABASE_STATE.ERROR);
                Toast.makeText(context, R.string.permissiondenied, Toast.LENGTH_LONG).show();
            }
        } else {
            setState(DATABASE_STATE.UPDATED);
        }
    }


    public static AppDatabase getDataBase() {
        return db;
    }

    public static void closeDataBase() {
        db.close();
    }


    public static void setState(DATABASE_STATE state) {
        DataBaseFactory.state = state;
        stateObservable.onNext(state);
    }

    public static ReplaySubject<DATABASE_STATE> getStateObservable() {
        return stateObservable;
    }
}
