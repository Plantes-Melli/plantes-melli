package fr.iut.plantes_melli.modele;

import java.io.Serializable;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.ForeignKey;
import androidx.room.PrimaryKey;

import static androidx.room.ForeignKey.CASCADE;

@Entity(tableName = "listePlantes",foreignKeys = @ForeignKey(entity = Plante.class,
                                    parentColumns = "id",
                                    childColumns = "id",
                                    onDelete = CASCADE))

    public class PlanteId implements Serializable {

        public PlanteId(int id) {
            this.id = id;
        }

        @PrimaryKey
        public int id;

    }