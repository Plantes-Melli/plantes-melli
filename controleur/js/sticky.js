// When the user scrolls the page, execute myFunction

	var header = document.getElementsByClassName("menu")[0];

	console.log(header);
	var sticky = header.offsetTop;

window.onscroll = function() {myFunction()};

// Add the sticky class to the header when you reach its scroll position. Remove "sticky" when you leave the scroll position
function myFunction() {

  if (window.pageYOffset > sticky) {
		document.getElementById("top-nav-placeholder").style.display = "block";
    header.classList.add("sticky");
  } else {
			document.getElementById("top-nav-placeholder").style.display = "none";
    header.classList.remove("sticky");
  }

}
