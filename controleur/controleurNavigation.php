<?php

require_once PATH_MODELE."/DAO.php";
require_once PATH_VUE."/vue.php";



class ControleurNavigation {
	private $modele;
	private $vue;

	public function __construct() {
		$this->modele = new modele();
		$this->vue= new Vue();
		//	$this->vue = new Vue();

	}
	/*
	* Pré-condition : reçoit une chaine de caractère correspondant au pseudo et une chaine de caractère correspondant au mot de passe
	* Post-condition : des variables de sessions sont initialisées, un booléen est retourné, il vaut vrai si l'utilisateur est bien connecté et faux sinon
	*/
	//Appel aux fonctions contenu dans vue pour afficher sur le site web
	public function afficherHeader($page,$pseudo){
		$this->vue->afficherHead();
		$this->vue->afficherMenu();
	}
	public function afficherFooter(){
		$this->vue->afficherFooter();
	}

	public function afficheMentionsLégalesCGU(){
		$this->vue->afficheMentionsLégalesCGU();
	}

	public function afficheSavoirPlus(){
		$this->vue->afficheSavoirPlus();
	}

	public function afficheContact(){
		$this->vue->afficheContact();
	}

	public function affichePolitiquesConfidentialites(){
		$this->vue->affichePolitiquesConfidentialites();
	}


	public function afficherAccueil(){
		//$this->vue->afficherChatBox();
		$this->vue->afficherBarreRecherche();
		$this->vue->afficherPageAccueil($this->modele->get3PlantesSaison());
	}

	public function afficherNavigation($pseudo){
		$this->vue->afficherNavigation($pseudo);
	}

	public function afficherLoginPage(){
		$this->vue->afficherLoginPage();
	}

	public function afficherCompte($compte){
			$this->vue->afficherCompte($compte->getPseudo(),$compte->getNom(),$compte->getPrenom(),$compte->getMail());
	}

	public function afficherErreur(){
		$this->vue->afficherErreur();
	}

	public function afficherApropos(){
		$this->vue->afficherApropos();
	}

	public function afficherInfoPlante($plante){
		$this->vue->afficherInfoPlante($plante);
	}
}
