<?php
require_once PATH_MODELE."/DAO.php";
require_once PATH_VUE."/vue.php";
require_once "sendMail.php";


class ControleurForgot{
	private $modele;
	private $vue;
	private $mailer;


	function __construct(){
			$this->modele = new modele();
			$this->vue = new Vue();
			$this->mailer = new Mailer();
	}

	function route(){
		if(isset($_POST["mode"])){
			switch ($_POST["mode"]) {
				case 'sendMail':
						$id= $this->modele->getIdByEmail($_POST["email"]);
						$key = uniqid('',true);
						if($id == ""){
							$this->vue->afficherEmailChForm("Aucun compte n'est associé à cet email");
						}else if($this->mailer->newPassEmail($_POST["email"],$key)){

							$this->modele->getNewPass($id,$key);
							$this->vue->afficherMessage("Email envoyé","L'email de renitialisation à bien été envoyé sur ".$_POST["email"]." ");
						}else{
							$this->vue->afficherMessage("Erreur serveur","L'email n'a pas pu être envoyé. Conctactez un administrateur via notre page de <a class='link' href=\"index.php?page=Contact\">contact</a>");
						}

					break;

				case 'changePassword':
					$message = NULL;
					if($this->verifPassword($_POST["mdp"],$_POST["mdpConf"],$message)){
							$this->modele->changePassword($_POST["key"],$_POST["mdp"]);
							$this->vue->afficherMessage("Mot de passe renitialisé","Votre mot de passe à bien été changé.");
							unset($_SESSION["compte"]);
					}else{
						$this->vue->afficherMDPChForm($message);
					}
					break;


				default:

					break;
			}
			return;

		}else if(isset($_GET["key"])){
			if($this->modele->existsForgotkey($_GET["key"])){
					$this->vue->afficherMDPChForm();
			}else{
				$this->vue->afficherMessage("Erreur","Ce lien à déjà été utilisé ou est invalide.");
			}
		}else{
			$this->vue->afficherEmailChForm();
		}
	}


	function verifPassword($mdp1,$mdp2,& $message){
		if($mdp1 != $mdp2){
			$message="Les mots de passes sont differents";
			return false;
		}


		if(preg_match('`^([a-zA-Z0-9\(\)\[\]\{\}\\\.\?\+,;/:!\-\*]{2,20})$`', $mdp1)){
				return true;
		}

		$message ="Le mot de passe comporte des caracteres autres que a-z A-Z 0-9 () [] {} \ . ? + , ; / : ! - * ou est d'une taille non comprise entre 0 et 20";
		return false;

	}
}



 ?>
