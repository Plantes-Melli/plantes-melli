package fr.iut.plantes_melli.vue;

import android.app.Activity;
import android.graphics.Bitmap;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.google.android.flexbox.FlexboxLayoutManager;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import fr.iut.plantes_melli.R;
import fr.iut.plantes_melli.controleur.OnClickItemAccueilControleur;
import fr.iut.plantes_melli.modele.Plante;
import fr.iut.plantes_melli.utils.ImageDecoder;

public class PlanteMozaicAdapter extends RecyclerView.Adapter<PlanteMozaicAdapter.PlanteMozaicViewHolder> {
    private List<Plante> dataModel;
    private Activity act;

    public static class PlanteMozaicViewHolder extends RecyclerView.ViewHolder{
        protected ImageView mImageView;

        public PlanteMozaicViewHolder(View layout) {
            super(layout);
            mImageView = layout.findViewById(R.id.image);
        }

        public void bind(Activity act, Plante p){
            mImageView.setOnClickListener(new OnClickItemAccueilControleur(act,p.getPlanteId()));
        }
    }


    public PlanteMozaicAdapter(Activity act,List<Plante> l) {
        this.act = act;
        this.dataModel = l;
    }

    @Override
    public PlanteMozaicViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.plante_mozaic,parent,false);
        return new PlanteMozaicViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull PlanteMozaicViewHolder holder, int position) {
        Plante p = dataModel.get(position);
        holder.mImageView.setImageBitmap(ImageDecoder.getBitMapImage("/mini/"+p.getImage()));
        holder.bind(this.act,p);
    }


    @Override
    public int getItemCount() {
        return dataModel.size();
    }
}
