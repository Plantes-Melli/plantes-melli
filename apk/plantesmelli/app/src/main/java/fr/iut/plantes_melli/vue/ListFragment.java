package fr.iut.plantes_melli.vue;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import java.util.Collections;
import java.util.List;

import androidx.fragment.app.Fragment;
import fr.iut.plantes_melli.R;
import fr.iut.plantes_melli.modele.DataBaseFactory;
import fr.iut.plantes_melli.modele.Plante;

public class ListFragment extends Fragment {
    List<Plante> planteList;
    PlanteAdapter planteAdapter;
    public ListFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        final View view = inflater.inflate(R.layout.fragment_list, container, false);
        setHasOptionsMenu(true);

       // final AppDatabase db = (AppDatabase) getArguments().getSerializable(listePlantes.KEY_DB);
        new AsyncTask(){
            @Override
            protected List<Plante> doInBackground(Object[] objects) {
                return  DataBaseFactory.getDataBase().planteDAO().getAll();

            }

            @Override
            protected void onPostExecute(Object o) {
                super.onPostExecute(o);
                planteList = (List<Plante>) o;
                Collections.sort(planteList, (Plante o1,Plante o2) -> o1.getNomFr().compareToIgnoreCase(o2.getNomFr()));

                ListView listView = view.findViewById(R.id.liste);
                planteAdapter = new PlanteAdapter(getContext(),planteList);
                listView.setAdapter(planteAdapter);
                listView.setOnItemClickListener((parent, view1, position, id) -> {
                    Intent intent = new Intent(getContext(),detailPlante.class);
                    intent.putExtra(accueil.PLANTE,planteList.get(position));
                    startActivity(intent);
                });
            }
        }.execute();
        return view;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu,MenuInflater menuInflater) {
        menuInflater.inflate(R.menu.liste_plante_menu, menu);
        super.onCreateOptionsMenu(menu,menuInflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle item selection
        switch (item.getItemId()) {
            case R.id.triAlA:
                Collections.sort(planteList, (Plante o1,Plante o2) -> o1.getNomFr().compareToIgnoreCase(o2.getNomFr()));
                break;
            case R.id.triAlD:
                Collections.sort(planteList,(Plante o1,Plante o2) -> o2.getNomFr().compareToIgnoreCase(o1.getNomFr()));
                break;
            case R.id.triApA:
                Collections.sort(planteList, (Plante o1,Plante o2) -> o1.getInter_apicole() - o2.getInter_apicole());
                break;
            case R.id.triApD:
                Collections.sort(planteList, (Plante o1,Plante o2) -> o2.getInter_apicole()-o1.getInter_apicole());
                break;
            default:
                return super.onOptionsItemSelected(item);
        }

        Log.d("test","Click");
        planteAdapter.notifyDataSetChanged();
        return true;
    }


}
