package fr.iut.plantes_melli.vue;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.android.flexbox.FlexboxLayout;

import java.util.Iterator;
import java.util.LinkedHashMap;

import androidx.appcompat.app.AppCompatActivity;
import fr.iut.plantes_melli.R;
import fr.iut.plantes_melli.modele.DataBaseFactory;
import fr.iut.plantes_melli.modele.Plante;
import fr.iut.plantes_melli.modele.PlanteId;
import fr.iut.plantes_melli.utils.ImageDecoder;

@SuppressLint("StaticFieldLeak")
public class detailPlante extends AppCompatActivity {
    private Button ajout;
    private Button supp;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_plante);




        Intent intent = getIntent();
        final Plante p = (Plante) intent.getSerializableExtra(accueil.PLANTE);
        getSupportActionBar().setTitle(p.getNomFr());








        ImageView img = findViewById(R.id.image);
        TextView description = findViewById(R.id.description);

        FlexboxLayout flexboxLayout = findViewById(R.id.infosLayout);
        Log.d("Plant detail","Loading "+p.getNomFr()+"...");
        int mois[] = { R.string.janvier,
                R.string.fevrier,
                R.string.mars,
                R.string.avril,
                R.string.mai,
                R.string.juin,
                R.string.juillet,
                R.string.aout,
                R.string.septembre,
                R.string.octobre,
                R.string.novembre,
                R.string.decembre
        };



        TextView nomFr = findViewById(R.id.nomFr);
        nomFr.setText(p.getNomFr());
        TextView nomLat = findViewById(R.id.nomLat);
        nomLat.setText(p.getNomLat());
        TextView famille = findViewById(R.id.famille);
        famille.setText("Famille: "+p.getFamille());
        TextView type = findViewById(R.id.type);
        type.setText("Type: "+p.getType());
        TextView expo = findViewById(R.id.expo);
        expo.setText("Exposition: "+p.getExposition());

        img.setImageBitmap(ImageDecoder.getBitMapImage("/mini/"+p.getImage()));
        LinkedHashMap<String,HexagonView> hexagons = new LinkedHashMap<>();
        hexagons.put("Nectarifère",new HexagonView(getApplicationContext(),100,chiffres2Plus(p.getNectarifere())));
        hexagons.put("Apicole",new HexagonView(getApplicationContext(),100,chiffres2Plus(p.getInter_apicole()),getResources().getColor(R.color.orange)));
        hexagons.put("Pollinifère", new HexagonView(getApplicationContext(),100,chiffres2Plus(p.getPollinifere())));



        hexagons.put("Moyenne Kg/Ha",new HexagonView(getApplicationContext(),100,p.getMoyenne_kg_Ha()+""));
        hexagons.put("Debut Floraison",new HexagonView(getApplicationContext(),100,getResources().getString(mois[p.getDebut_floraison()-1])));
        hexagons.put("Fin Floraison",new HexagonView(getApplicationContext(),100,getResources().getString(mois[p.getFin_floraison()-1])));
        Iterator<String> it= hexagons.keySet().iterator();
        int i = 0;
        while(it.hasNext()){
            String legend = it.next();
            System.out.println(legend);
            TextView textView = new TextView(getApplicationContext());
            textView.setText(legend);
            textView.setTextAlignment(View.TEXT_ALIGNMENT_CENTER);
            textView.setTypeface(null,Typeface.BOLD);
            textView.setTextColor(getResources().getColor(R.color.black));

            LinearLayout layout = new LinearLayout(getApplicationContext());
            layout.setOrientation(LinearLayout.VERTICAL);
            LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(
                    LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);

            layoutParams.setMargins(20, 5, 20, 5);
            layout.addView(hexagons.get(legend),layoutParams);
            layout.addView(textView);

            flexboxLayout.addView(layout,i);
            i++;

        }

        description.setText(p.getDescription());

        lockBtn(p.getPlanteId());

        ajout = findViewById(R.id.ajout);
        supp = findViewById(R.id.suppression);

        ajout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new AsyncTask(){

                    @Override
                    protected Object doInBackground(Object[] objects) {
                        DataBaseFactory.getDataBase().listeDAO().insertPlanteId(new PlanteId(p.getPlanteId()));
                        lockBtn(p.getPlanteId());
                        return null;
                    }
                }.execute();

            }
        });

        supp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new AsyncTask(){

                    @Override
                    protected Object doInBackground(Object[] objects) {
                        DataBaseFactory.getDataBase().listeDAO().removePlanteId(new PlanteId(p.getPlanteId()));
                        lockBtn(p.getPlanteId());
                        return null;
                    }
                }.execute();

            }
        });

    }


    // bloque/débloque les boutons d'ajout à la liste
    public void lockBtn(final int myId){
        new AsyncTask(){

            @Override
            protected Object doInBackground(Object[] objects) {
               PlanteId idPlante = DataBaseFactory.getDataBase().listeDAO().getPlanteId(myId);
               return idPlante == null;
            }

            @Override
            protected void onPostExecute(Object o) {
                boolean bool = (boolean) o;
                ajout.setEnabled(bool);
                supp.setEnabled(!bool);
            }
        }.execute();
    }

    //au click du bouton retour, detruit l'activité
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                break;
        }
        return true;
    }


    public String chiffres2Plus(int n){
        String str= "";

        if(n == 0)
            return "-";

        for(int  i=0; i < n; i++){
            str += "+";
        }

        return str;
    }
}
