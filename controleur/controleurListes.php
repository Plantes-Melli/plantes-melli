<?php
require_once PATH_MODELE."/DAO.php";
require_once PATH_VUE."/vue.php";
include_once "controleurNavigation.php";

class ControleurListes{

	private $modele;
  private $vue;
	private $ctrlNavigation;


  public function __construct(){
    $this->modele = new modele();
    $this->vue= new Vue();
		$this->ctrlNavigation  = new ControleurNavigation();
  }

	public function route(){
		if(!isset($_SESSION["compte"]) ||
		   !isset($_GET["user"]) ||
			 (isset($_GET["user"]) && !$this->modele->existsPseudo($_GET["user"])) ||
		 (!$this->modele->canSeeList($_SESSION["compte"]->getPseudo(),$_GET["user"])) ){

			$this->ctrlNavigation->afficherAccueil();

		}else{
				if(isset($_GET["id"])){
					$this->vue->afficherDetailPlante($this->modele->getPlanteById($_GET["id"],$_GET["user"]));
					$this->vue->afficherPanel("historique");
				}else{

					$pseudo = $_GET["user"];
					$liste = $this->modele->getListUser($pseudo);
					echo "<h4>Liste de ".$pseudo."</h4>";
					$this->vue->afficherListePlantes($liste,0,"mosaic",NULL);
				}

		}
	}


}



 ?>
