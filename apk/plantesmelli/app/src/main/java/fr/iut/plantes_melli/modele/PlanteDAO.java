package fr.iut.plantes_melli.modele;


import java.io.Serializable;
import java.util.List;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Entity;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;

@Dao
public interface PlanteDAO {

    @Query("SELECT * FROM plant_melli_apk;")
    List<Plante> getAll();

    @Query("SELECT * FROM plant_melli_apk where id = :myId")
    Plante getPlantById(int myId);

    @Query("SELECT * FROM plant_melli_apk where nomFr LIKE :nom OR nomLat LIKE :nom")
    Plante getPlantByName(String nom);

    @Query("SELECT * FROM plant_melli_apk WHERE (debut_floraison-:ecart <:mois) and fin_floraison+:ecart>:mois")
    List<Plante> get3PlantesSaison(int ecart,int mois);

    @Query("SELECT nomFr FROM plant_melli_apk")
    List<String> getNomsFr();

    @Query("SELECT nomLat FROM plant_melli_apk")
    List<String> getNomsLat();

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertPlantes(Plante... plantes);

    @Delete
    void deletePlantes(Plante... plantes);




}
