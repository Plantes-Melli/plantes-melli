package fr.iut.plantes_melli.vue;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.view.KeyEvent;
import android.view.MenuItem;
import android.view.inputmethod.EditorInfo;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.material.navigation.NavigationView;

import java.io.File;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.SortedSet;
import java.util.TreeSet;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import fr.iut.plantes_melli.R;
import fr.iut.plantes_melli.controleur.OnClickItemAccueilControleur;
import fr.iut.plantes_melli.modele.DataBaseFactory;
import fr.iut.plantes_melli.modele.Plante;
import fr.iut.plantes_melli.utils.ImageDecoder;
import io.reactivex.Observer;
import io.reactivex.disposables.Disposable;

public class accueil extends AppCompatActivity implements  NavigationView.OnNavigationItemSelectedListener{
    public  static final String PLANTE = "fr.iut.plantes_melli.PLANTE";


    public DrawerLayout drawerLayout;
    public Toolbar toolbar;

    @SuppressLint("StaticFieldLeak")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setTheme(R.style.AppTheme_NoActionBar);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_accueil);
        loadActionBar(findViewById(R.id.drawerLayout), findViewById(R.id.toolbar));

        //menu lateral
        NavigationView mNavigationView = findViewById(R.id.menu);

        if (mNavigationView != null) {
            mNavigationView.setNavigationItemSelectedListener(this);
        }

        //genere une nouvelle base de données (design pattern singleton)
        new DataBaseFactory();
        DataBaseFactory.createDataBase(getApplicationContext());



        //On charge les noms des plantes dans un adapter pour autocomplétion
        new AsyncTask(){
            @Override
            protected Object doInBackground(Object[] objects) {
                List<String> l = DataBaseFactory.getDataBase().planteDAO().getNomsFr();
                l.addAll(DataBaseFactory.getDataBase().planteDAO().getNomsLat());
                return l;
            }

            @Override
            protected void onPostExecute(Object o) {
                super.onPostExecute(o);
                List<String> liste = (List<String>) o;
                SortedSet<String> finalList = new TreeSet<String>(new Comparator<String>() {
                    @Override
                    public int compare(String o1, String o2) {
                        return o1.compareToIgnoreCase(o2);
                    }
                });
                finalList.addAll(liste);
                String[] planteNamesAr =  finalList.toArray(new String[finalList.size()]);
                ArrayAdapter<String> adapter = new ArrayAdapter<String>(accueil.this,android.R.layout.simple_list_item_1,planteNamesAr);
                AutoCompleteTextView textView = findViewById(R.id.search_bar);
                textView.setAdapter(adapter);

            }
        }.execute();

        //AutoCompletion
        AutoCompleteTextView editText = findViewById(R.id.search_bar);
        editText.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                boolean handled = false;
                if (actionId == EditorInfo.IME_ACTION_SEND) {
                    sendMessage(v);
                    handled = true;
                }
                return handled;
            }

            private void sendMessage(TextView v) {
                final String nomPlante = v.getText().toString()+"%";



                        new AsyncTask() {
                            @Override
                            protected Plante doInBackground(Object[] objects) {
                                return DataBaseFactory.getDataBase().planteDAO().getPlantByName(nomPlante);
                            }

                            protected void onPostExecute(Object o) {
                                super.onPostExecute(o);
                                Plante p = (Plante) o;

                                if(p != null){
                                    Intent intent = new Intent(accueil.this, detailPlante.class);
                                    intent.putExtra(PLANTE, p);
                                    startActivity(intent);
                                }else{
                                    Toast.makeText(getApplicationContext(),"Aucune plante trouvée", Toast.LENGTH_LONG).show();
                                }
                            }
                        }.execute();
            }
        });
    }

    //Affichage plantes de saison
    @SuppressLint("StaticFieldLeak")
    @Override
    protected void onResume(){
        super.onResume();

        DataBaseFactory.getStateObservable().subscribe(new Observer<DataBaseFactory.DATABASE_STATE>() {
            @Override
            public void onSubscribe(Disposable d) { }

            @Override
            public void onNext(DataBaseFactory.DATABASE_STATE database_state) {
                System.out.println("DataBase say: "+database_state);
                if(database_state == DataBaseFactory.DATABASE_STATE.UPDATING){
                    Toast.makeText(getApplicationContext(),getResources().getString(R.string.majBDD),Toast.LENGTH_SHORT).show();
                }else if(database_state == DataBaseFactory.DATABASE_STATE.UPDATED){
                    //recupere 3 plantes de saisons aléatoirement
                    new AsyncTask(){
                        @Override
                        protected List<Plante> doInBackground(Object[] objects) {
                            List<Plante> planteSaison;
                            int i = 1;

                            java.util.Date date= new Date();
                            Calendar cal = Calendar.getInstance();
                            cal.setTime(date);
                            int month = cal.get(Calendar.MONTH);
                            Log.d("Accueil","Getting seasonal plants...");
                            do{
                                planteSaison = DataBaseFactory.getDataBase().planteDAO().get3PlantesSaison(i,month);
                                i++;
                            }while(planteSaison.size() <3);

                            Collections.shuffle(planteSaison);

                            Log.d("Accueil","Done!");
                            return planteSaison.subList(0,3);
                        }

                        //affiche les plantes
                        //TODO: degeu! passer par un recyclerView et implemanter un PlanteAccueilAdapter
                        @Override
                        protected void onPostExecute(Object listplantes) {
                            super.onPostExecute(listplantes);
                            List<Plante> liste = (List<Plante>) listplantes;
                            File folder = new File(Environment.getExternalStorageDirectory().getAbsolutePath(),"/plantesmelli/res/images/");
                            int ids[][] = {{R.id.id1,R.id.imageView1,R.id.nomFr,R.id.nomLat1,R.id.famille1},
                                    {R.id.id2,R.id.imageView2,R.id.nomFr2,R.id.nomLat2,R.id.famille2},
                                    {R.id.id3,R.id.imageView3,R.id.nomFr3,R.id.nomLat3,R.id.famille3}};

                            int layouts_ids[] = {R.id.item1,R.id.item2,R.id.item3};

                            for(int i = 0; i < 3;i++){

                                TextView id = findViewById(ids[i][0]);
                                ImageView img = findViewById(ids[i][1]);
                                TextView nomFr = findViewById(ids[i][2]);
                                TextView nomLat = findViewById(ids[i][3]);
                                TextView famille = findViewById(ids[i][4]);
                                ConstraintLayout layout = findViewById(layouts_ids[i]);

                                Plante p =  liste.get(i);

                                Log.d("Season plants","Loading "+p.getNomFr()+"...");

                                id.setText(p.getPlanteId()+"");
                                Bitmap image = ImageDecoder.getBitMapImage("/normal/"+p.getImage());
                                if(image != null )
                                    img.setImageBitmap(image);
                                nomFr.setText(p.getNomFr());
                                nomLat.setText(p.getNomLat());
                                famille.setText(p.getFamille());

                                layout.setOnClickListener(new OnClickItemAccueilControleur(accueil.this,p.getPlanteId()));
                            }
                        }
                    }.execute();
                }
            }

            @Override
            public void onError(Throwable e) {

            }

            @Override
            public void onComplete() {

            }
        });
    }


    //menu latéral lors du click d'un item
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        Intent act = null;
        // Handle item selection
        switch (item.getItemId()) {
            case R.id.plantes:
                act = new Intent(this,listePlantes.class);
                break;
            case R.id.liste:
                act = new Intent(this,MaListe.class);
                break;


        }
            startActivity(act);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                drawerLayout.openDrawer(GravityCompat.START);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    //icone pour ouvrir le menu lateral
    public void loadActionBar(DrawerLayout drawerLayout,Toolbar toolbar){
        this.drawerLayout = drawerLayout;
        this.toolbar =  toolbar;
        setSupportActionBar(this.toolbar);
        this.toolbar.setTitleTextColor(0xFFFFFFFF);
        ActionBar actionbar = getSupportActionBar();
        actionbar.setDisplayHomeAsUpEnabled(true);
        actionbar.setHomeAsUpIndicator(R.drawable.ic_menu_black_24dp);
    }

    //on ferme la base de données quand l'activité principale s'arrete
    @Override
    protected void onStop() {
        super.onStop();
        DataBaseFactory.closeDataBase();
    }

}
