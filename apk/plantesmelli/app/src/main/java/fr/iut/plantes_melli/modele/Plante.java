package fr.iut.plantes_melli.modele;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;
import java.util.Objects;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity(tableName = "plant_melli_apk")
public class Plante implements Serializable {

    public Plante(int id,
                  String type,
                  String nomFr,
                  String nomLat,
                  String famille,
                  String image,
                  int moyenne_kg_Ha,
                  int inter_apicole,
                  String exposition,
                  int nectarifere,
                  int pollinifere,
                  int debut_floraison,
                  int fin_floraison,
                  String description,
                  String hash
    ) {

        this.id = id;
        this.type = type;
        this.nomFr = nomFr;
        this.nomLat = nomLat;
        this.famille = famille;
        this.image = image;
        this.moyenne_kg_Ha = moyenne_kg_Ha;
        this.inter_apicole = inter_apicole;
        this.exposition = exposition;
        this.nectarifere = nectarifere;
        this.pollinifere = pollinifere;
        this.debut_floraison = debut_floraison;
        this.fin_floraison = fin_floraison;
        this.description = description;
        this.hash = hash;
    }

    public Plante(JSONObject obj) throws JSONException {
        this(obj.getInt("id"),
                obj.getString("type"),
                obj.getString("nomFr"),
                obj.getString("nomLat"),
                obj.getString("famille"),
                obj.getString("image"),
                obj.getInt("moyenne_kgHa"),
                obj.getInt("inter_apicole"),
                obj.getString("exposition"),
                obj.getInt("nectarifere"),
                obj.getInt("pollinifere"),
                obj.getInt("debut_floraison"),
                obj.getInt("fin_floraison"),
                obj.getString("description"),
                obj.getString("hash"));
    }

    @PrimaryKey
    public int id;

    @ColumnInfo(name = "type")
    public String type;

    @ColumnInfo(name = "nomFr")
    public String nomFr;

    @ColumnInfo(name = "nomLat")
    public String nomLat;

    @ColumnInfo(name = "famille")
    public String famille;

    @ColumnInfo(name = "image")
    public String image;

    @ColumnInfo(name = "moyenne_kgHa")
    public int moyenne_kg_Ha;

    @ColumnInfo(name = "inter_apicole")
    public int inter_apicole;

    @ColumnInfo(name = "exposition")
    public String exposition;

    @ColumnInfo(name = "nectarifere")
    public int nectarifere;

    @ColumnInfo(name = "pollinifere")
    public int pollinifere;

    @ColumnInfo(name = "debut_floraison")
    public int debut_floraison;

    @ColumnInfo(name = "fin_floraison")
    public int fin_floraison;

    @ColumnInfo(name = "description")
    public String description;

    @ColumnInfo(name = "hash")
    public String hash;


    @Override
    public String toString() {
        return "Plante{" +
                "id=" + id +
                ", type='" + type + '\'' +
                ", nomFr='" + nomFr + '\'' +
                ", nomLat='" + nomLat + '\'' +
                ", famille='" + famille + '\'' +
                ", image='" + image + '\'' +
                ", moyenne_kg_Ha=" + moyenne_kg_Ha +
                ", inter_apicole=" + inter_apicole +
                ", exposition='" + exposition + '\'' +
                ", nectarifere=" + nectarifere +
                ", pollinifere=" + pollinifere +
                ", debut_floraison=" + debut_floraison +
                ", fin_floraison=" + fin_floraison +
                ", description='" + description + '\'' +
                ", hash='" + hash + '\'' +
                '}';
    }

    public int getPlanteId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getNomFr() {
        return nomFr;
    }

    public void setNomFr(String nomFr) {
        this.nomFr = nomFr;
    }

    public String getNomLat() {
        return nomLat;
    }

    public void setNomLat(String nomLat) {
        this.nomLat = nomLat;
    }

    public String getFamille() {
        return famille;
    }

    public void setFamille(String famille) {
        this.famille = famille;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public int getMoyenne_kg_Ha() {
        return moyenne_kg_Ha;
    }

    public void setMoyenne_kg_Ha(int moyenne_kg_Ha) {
        this.moyenne_kg_Ha = moyenne_kg_Ha;
    }

    public int getInter_apicole() {
        return inter_apicole;
    }

    public void setInter_apicole(int inter_apicole) {
        this.inter_apicole = inter_apicole;
    }

    public String getExposition() {
        return exposition;
    }

    public void setExposition(String exposition) {
        this.exposition = exposition;
    }

    public int getNectarifere() {
        return nectarifere;
    }

    public void setNectarifere(int nectarifere) {
        this.nectarifere = nectarifere;
    }

    public int getPollinifere() {
        return pollinifere;
    }

    public void setPollinifere(int pollinifere) {
        this.pollinifere = pollinifere;
    }

    public int getDebut_floraison() {
        return debut_floraison;
    }

    public void setDebut_floraison(int debut_floraison) {
        this.debut_floraison = debut_floraison;
    }

    public int getFin_floraison() {
        return fin_floraison;
    }

    public void setFin_floraison(int fin_floraison) {
        this.fin_floraison = fin_floraison;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getHash() {
        return hash;
    }

    public void setHash(String hash) {
        this.hash = hash;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Plante plante = (Plante) o;
        return Objects.equals(id, plante.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(hash); //impossible a convertir de base 32 -> 10 car trop gros (= overflow et collisions dans les hash)
    }
}
