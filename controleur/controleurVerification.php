<?php
require_once PATH_MODELE."/DAO.php";
require_once PATH_VUE."/vue.php";
include_once "controleurNavigation.php";

class ControleurVerification{

	private $modele;
  private $vue;
	private $ctrlNavigation;


  public function __construct(){
    $this->modele = new modele();
    $this->vue= new Vue();
		$this->ctrlNavigation  = new ControleurNavigation();
  }

	public function route(){
		if(isset($_GET["key"])){
			if($this->modele->verifMail($_GET["key"]) > 0){
				$this->vue->afficherMessage("Compte vérifié","Votre compte à bien été vérifié.<br/> Bon séjour sur plantes-mellifères.");
				$this->modele->deleteVerifKey($_GET["key"]);
			}else{
				$this->vue->afficherMessage("Erreur","Ce lien à déjà été verifié ou est invalide.");
			}
		}else{
			$this->ctrlNavigation->afficherAccueil();
		}

	}


}



 ?>
