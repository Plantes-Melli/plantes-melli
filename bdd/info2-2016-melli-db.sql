-- phpMyAdmin SQL Dump
-- version 4.7.9
-- https://www.phpmyadmin.net/
--
-- Hôte : 127.0.0.1:3306
-- Généré le :  ven. 05 avr. 2019 à 07:56
-- Version du serveur :  5.7.21
-- Version de PHP :  5.6.35

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `info2-2016-melli-db`
--

-- --------------------------------------------------------

--
-- Structure de la table `forgotpass`
--

DROP TABLE IF EXISTS `forgotpass`;
CREATE TABLE IF NOT EXISTS `forgotpass` (
  `id` int(11) NOT NULL,
  `link` varchar(130) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `forgotpass`
--

INSERT INTO `forgotpass` (`id`, `link`) VALUES
(66, '5c48cea675c5b5.46921673'),
(66, '5c48cff03029b8.47826078'),
(59, '5c496dc8995ae2.28879696'),
(59, '5c497adea2a892.66066337'),
(59, '5c497cb13b8bf7.96155478'),
(59, '5c497cfbf2dfc2.10000539'),
(59, '5c497e1581d113.95118652'),
(59, '5c497e6adefa13.01500472'),
(59, '5c497f0c0bddb1.14329893'),
(59, '5c497fa70ddc48.15477858'),
(59, '5c4982789709c7.22635996'),
(59, '5c49832855fa51.58481937'),
(60, '5c4a1b9736e364.36875664'),
(60, '5c4a4145e1b0d6.41362941');

-- --------------------------------------------------------

--
-- Structure de la table `listeadherent`
--

DROP TABLE IF EXISTS `listeadherent`;
CREATE TABLE IF NOT EXISTS `listeadherent` (
  `pseudo` varchar(36) NOT NULL,
  `idPlante` int(11) NOT NULL,
  PRIMARY KEY (`pseudo`,`idPlante`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `listeadherent`
--

INSERT INTO `listeadherent` (`pseudo`, `idPlante`) VALUES
('Axel44', 2),
('test', 10),
('test', 17),
('test', 22),
('test', 57),
('test', 63),
('test2', 16);

-- --------------------------------------------------------

--
-- Structure de la table `listespubliques`
--

DROP TABLE IF EXISTS `listespubliques`;
CREATE TABLE IF NOT EXISTS `listespubliques` (
  `src` varchar(36) NOT NULL,
  `dst` varchar(36) NOT NULL,
  PRIMARY KEY (`src`,`dst`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `listespubliques`
--

INSERT INTO `listespubliques` (`src`, `dst`) VALUES
('test', 'test2');

-- --------------------------------------------------------

--
-- Structure de la table `plantes_locales`
--

DROP TABLE IF EXISTS `plantes_locales`;
CREATE TABLE IF NOT EXISTS `plantes_locales` (
  `pseudo` varchar(36) NOT NULL,
  `id` int(11) NOT NULL,
  `moyenne_kgHa` int(5) NOT NULL,
  `inter_apicole` int(3) NOT NULL,
  `exposition` varchar(10) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `nectarifere` int(5) NOT NULL,
  `pollinifere` int(5) NOT NULL,
  `debut_floraison` int(3) NOT NULL,
  `fin_floraison` int(3) NOT NULL,
  `description` text NOT NULL,
  UNIQUE KEY `pseudo` (`pseudo`,`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `plantes_locales`
--

INSERT INTO `plantes_locales` (`pseudo`, `id`, `moyenne_kgHa`, `inter_apicole`, `exposition`, `nectarifere`, `pollinifere`, `debut_floraison`, `fin_floraison`, `description`) VALUES
('test', 17, 100, 3, 'Soleil', 2, 3, 5, 6, 'Aucune description définie.'),
('test', 63, 0, 0, 'Soleil', 0, 0, 1, 1, 'Cette plante n\'a absolument aucun interet.'),
('test', 10, 100, 3, 'Soleil', 3, 3, 5, 8, 'Description locale de la plante test');

-- --------------------------------------------------------

--
-- Structure de la table `plant_melli_apk`
--

DROP TABLE IF EXISTS `plant_melli_apk`;
CREATE TABLE IF NOT EXISTS `plant_melli_apk` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `nomFr` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `nomLat` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `famille` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `image` text CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `moyenne_kgHa` int(5) NOT NULL,
  `inter_apicole` int(3) NOT NULL,
  `exposition` varchar(10) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `nectarifere` int(5) NOT NULL,
  `pollinifere` int(5) NOT NULL,
  `debut_floraison` int(3) NOT NULL,
  `fin_floraison` int(3) NOT NULL,
  `description` text NOT NULL,
  `hash` varchar(34) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=61 DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `plant_melli_apk`
--

INSERT INTO `plant_melli_apk` (`id`, `type`, `nomFr`, `nomLat`, `famille`, `image`, `moyenne_kgHa`, `inter_apicole`, `exposition`, `nectarifere`, `pollinifere`, `debut_floraison`, `fin_floraison`, `description`, `hash`) VALUES
(1, 'Arbres', 'Mimosa des fleuristes', 'Acacia dealbata', 'Fabacea', 'AcciaDealbata.jpg', 0, 2, 's', 2, 2, 1, 3, 'Cette plante aux fleurs hermaphrodites se reproduit par entomogamie.\r\n\r\nLa floraison survient de janvier à mars.\r\n\r\nLes fleurs se présentent sous forme de petits pompons jaunes et soyeux disposés en grappes ramifiées. Chaque fleur comprend un calice constitué de 5 sépales très petits, duveteux. La corolle est constituée de 5 petits pétales libres de couleur jaune. Ces pièces florales sont rapidement dépassées par de nombreuses étamines qui, s\'épanouissant au bout de leur long filet, forment les pompons d\'un jaune lumineux. L\'ovaire, situé au-dessus du point d\'insertion du calice et de la corolle (on parle d\'ovaire supère), ne comporte qu\'un seul carpelle2.\r\n\r\nLes fruits sont des gousses articulées, plates et brunes à maturité.', 'eb3ebd302ccc014520a225beebbee591'),
(2, 'Annuelle et bi-annulle', 'Angélique sauvage', 'Angelica sylvestris', 'Apiaceae', 'AngelicaSylvestris.jpg', 50, 2, 'mo', 3, 1, 7, 9, 'Autrefois, Angelica sylvestris était considérée comme une plante magique associée à la magie blanche. Selon la légende, l\'herbe à la fièvre fut apportée par un ange à un moine en lui révélant ses vertus : elle protège les enfants, combat la peste, guérit des morsures des bêtes enragées et chasse le diable1.Elle peut atteindre 3 m de haut.\r\n\r\nLes feuilles peuvent être utilisées en salade et les graines en pâtisserie. Elle entre dans la composition de l\'eau de mélisse.', 'c745177e200dc9d8fefc0e69f91adb52'),
(3, 'Arbres', 'Bouleau noir', 'Betula nigra', 'Betulaceae', 'BetulaNigra.jpg', 0, 0, 's', 0, 2, 4, 5, 'Le Betula nigra (Bouleau noir) est une espèce de bouleau; il est commun dans les plaines alluviales et les marais de l\'est des États-Unis, du New Hampshire au Minnesota, de la Floride à l\'est du Texas.', '20c3c73be67ef70fad542b2ee3f1243b'),
(4, 'Arbres', 'Bouleau à papier', 'Betula papyrifera', 'Betulaceae', 'BetulaPapyrifera.jpg', 0, 0, 's', 0, 2, 4, 5, 'Betula papyrifera est un arbre de la famille des Betulaceae. C\'est notamment l\'espèce du bouleau à papier, mais l\'espèce comprend une seconde variété.', 'bba0cbb5c29ef6205475516d3ebec374'),
(5, 'Arbres', 'Bouleau verruqueux', 'Betula pendula', 'Betulaceae', 'BetulaPendula.jpg', 0, 2, 's', 0, 2, 4, 5, 'Le bouleau verruqueux ou bouleau blanc (Betula pendula, syn. B. verrucosa), ou encore bouleau d\'Europe, mais aussi bouleau blanc d\'Europe5 est un arbre pouvant atteindre 25 mètres de hauteur. Originaire d\'Europe et d\'Asie, c\'est un arbre très rustique et qui a besoin de lumière. Il aime les sols secs à frais, siliceux de préférence. Les sols acides à alcalins ne le dérangent pas outre mesure. C\'est une essence pionnière, colonisatrice et très adaptable. Il résiste parfaitement aux embruns. Il peut être isolé, en alignement, bosquet, etc.', '2237c76b161a935bdd075aed52d198b1'),
(6, 'Annuelle et bi-annulle', 'Bourrache', 'Borago officinalis', 'Boraginaceae', 'BoragoOfficinalis.jpg', 200, 3, 'mo - s', 3, 2, 5, 8, 'En climat tempéré, la floraison intervient de mai-juin à août. Dans le Midi de la France elle fleurit fin mars début avril. Les fleurs bleues, blanches plus rarement roses sont disposées en une cyme recourbée en crosse. Le calice possède cinq sépales, la corolle possède cinq pétales égaux soudés à leur base. La couleur de la fleur est bleue, plus rarement rose ou blanche.\r\n\r\nChaque pétale porte un repli saillant à l\'intérieur ; cinq étamines à longues anthères sont insérées sur la base de la corolle entre les pétales ; leur filet porte en dehors un appendice conique. Le pistil est constitué par deux carpelles soudés ; l\'ovaire est creusé de deux loges contenant chacune deux ovules ; le style, au lieu de surmonter les ovaires, se détache de leur base.\r\n\r\nLe fruit est formé par quatre akènes, parfois moins, logés au fond du calice persistant.', 'bfd2eea4704419e11fcf45bd1e4e4cf5'),
(7, 'Annuelle et bi-annulle', 'Colza', 'Brassica napus', 'Brassicaceae', 'BrassicaNapus.jpg', 200, 3, 's', 3, 3, 4, 5, 'D\'après Jan Kops (en) dans Flora Batava, vol. 4 de 1822, la plante se distingue de Brassica rapa subsp. oleifera (navette (chou) sauvage) par ses feuilles inférieures lisses, sans poil rude, et plus larges ; ses feuilles supérieures sont moins pointues ; ses fleurs sont d\'un jaune plus foncé ; et la pointe du bec des siliques est moins subulé et plus anguleuse.Les semences sont antiscorbutiques et diurétiques.', 'd3ab1013dc154437a762a70f1b11e45f'),
(8, 'Sous-Arbrusseau', 'Bruyère', 'Calluna vulgaris', 'Ericaceae', 'CallunaVulgaris.jpg', 200, 2, 'mo - s', 2, 1, 7, 9, 'La Callune est un sous-arbrisseau vivace de 20 à 50 cm de hauteur (rarement 1 m).\r\n\r\nLa plante supporte un pâturage modéré et peut se régénérer après un incendie. Parfois géré en utilisant une méthode de brûlis.\r\n\r\nElle se distingue du genre Erica par ses feuilles opposées en forme de petites écailles sessiles imbriquées sur 4 rangs, et son calice pétaloïde, qui enferme la corolle et est entouré à sa base par un calicule. Le calice et la corolle sont divisés en 4 parties. Les fleurs apparaissent en fin d\'été, et pour les espèces sauvages sont généralement violet ou mauve.\r\n\r\nLa callune est une importante source de nourriture pour les moutons ou les chevreuils qui peuvent paître lorsque la neige recouvre la végétation. Le lagopède d\'Écosse se nourrit de ses jeunes pousses et graines.', 'a540ab7f9bb8fdd0fa767321b5e9100d'),
(9, 'Vivaces', 'Campanules', 'Campanula rapunculoides', 'Campanulaceae', 'CampanulaRapunculoides.jpg', 50, 2, 'mo - s', 3, 2, 5, 9, 'Les campanules (genre Campanula du latin campana, « petite cloche ») sont des plantes herbacées vivaces ou bisannuelles de la famille des Campanulacées. Leurs fleurs, bleues, violettes ou blanches, sont hermaphrodites. Le calice présente cinq dents, souvent assez étroites. La corolle, en cloche, s\'ouvre en cinq lobes. Cinq étamines. Ovaire infère. Style solitaire. Le fruit est une capsule à nombreuses graines. Leur large dispersion dans le monde montre qu\'il s\'agit d\'un genre hémérochore.', '3c7a71ac5b62992ea5e1583b7e924857'),
(10, 'Vivaces', 'Chardons', 'Carduus nutans', 'Asteraceae', 'CarduusNutans.jpg', 100, 3, 's', 3, 2, 5, 8, 'C\'est une plante épineuse à port dressé qui peut atteindre un mètre de haut. Sa racine pivotante est assez développée. Les feuilles ont un limbe découpé et bordé d\'épines nombreuses. Elle se prolongent en ailes sur la tige.\r\n\r\nLes fleurs, rose pourpre, parfois blanches, sont réunies en capitules de 3 à 5 cm de diamètre, qui prennent un port penché lorsqu\'ils sont épanouis. Elles sont toutes tubulées stamino-pistillées et donnent des akènes équipés de pappus. L\'involucre est constitué de bractées nombreuses terminées par une épine simple, forte et piquante, les plus extérieures, étalées et renversées, rayonnent autour du capitule.', '8479647fea8be68cb6cbefadfe72a9ed'),
(11, 'Arbres', 'Chataigner', 'Castanea sativa', 'Fabacea', 'CastaneaSativa.jpg', 200, 3, 'mo-s', 2, 3, 6, 7, 'Le châtaignier est un arbre majestueux à cime large bien branchue et à croissance rapide. Il peut mesurer 25 à 35 m de haut et 4 mètres de diamètre. Il a une grande longévité et peut dépasser le millénaire2.\r\n\r\nL’écorce jeune est lisse et de couleur brun verdâtre, puis devient brun foncé avec le rhytidome qui se fissure longitudinalement. Avec l\'âge, ces rhytidomes tendant à se vriller selon une spirale lévogyre3 et le tronc a tendance à devenir creux1.\r\n\r\nSes grandes feuilles caduques vert luisantes dessus sont de forme oblongue-lancéolée aiguë, aux bords en dents de scie et pétiole court. Elles sont disposées en spirale et peuvent mesurer jusqu\'à 25 cm de long sur 4 à 8 cm de large). Elles sont riches en tanins (pour l’essentiel des tanins ellagiques tels que castalagine et vescalagine).\r\n\r\n\r\nUne bogue ouverte, dévoilant deux châtaignes.\r\nCet arbre monoïque à croissance sympodiale4 fleurit de la mi-juin à la mi-juillet (les fleurs étant des chatons cylindriques jaune pâle), les chatons mâles, dressés à la floraison et disposés à la base des rameaux, apparaissent les premiers et répandent alors une forte odeur de sperme ou de miel, les chatons femelles se réunissent par trois et sont disposés plus au sommet5.\r\n\r\nL\'espèce étant auto-stérile, il faut toujours planter au moins deux variétés compatibles entre elles pour obtenir des fruits (par exemple Marigoule et Belle épine).\r\n\r\nLa bogue, involucre vert épineux, enveloppe les fruits et dissuade certains prédateurs de s\'attaquer aux châtaignes. Elle correspond à une transformation des bractées. À l\'intérieur de la bogue se trouvent les châtaignes, au nombre de 1 à 3, qui sont, au sens botanique, des fruits secs de type akènes enveloppés par une pellicule astringente et par un tégument.\r\n\r\nOn ramasse ou récolte les châtaignes à partir du mois d\'octobre.', 'b7f17fee7b8fa6544850737140ad22fd'),
(12, 'Annuelle et bi-annulle', 'Bleuet', 'Centaurea cyanus', 'Asteraceae', 'CentaureaCyanus.jpg', 200, 2, 's', 3, 2, 5, 8, 'C\'est une plante annuelle2 aux tiges de couleur vert grisâtre, d\'aspect velouté, aux feuilles lancéolées ou pennatilobées, aux fleurs bleues, pourpres, blanches ou roses aux fleurons périphériques étalés. Sous le capitule, s\'observent des bractées à bords ciliés (cils courts et réguliers, généralement bruns). La floraison a lieu entre les mois d\'avril et novembre3.', 'bd2f1e234c988cd4b35c90172046fcae'),
(13, 'Vivaces', 'Chicoré sauvage', 'Cichorium', 'Asteraceae', 'Cichorium.jpg', 100, 2, 's', 2, 1, 7, 10, 'Les Chicorées (Cichorium) sont un genre botanique qui rassemble des plantes de la famille des Astéracées (composées). Ce genre comporte à la fois des espèces sauvages et des plantes qui sont cultivées, soit des variétés à feuilles (salades, endives), soit des variétés à racines (succédané de café appelé chicorée).', 'b516e7a72d66e5b8ef098ebcc69512e5'),
(14, 'Vivaces', 'Coriande', 'Coriandrum sativum', 'Apiaceae', 'CoriandrumSativum.jpg', 200, 3, 's', 3, 1, 6, 7, 'La coriandre est une plante annuelle élancée, ramifiée, mesurant généralement en floraison de 30 à 60 cm mais pouvant atteindre 1,40 m11. Le feuillage et la tige sont verts ou vert clair tirant parfois sur le rouge ou le violet pendant la floraison, glabres, luisants (notamment les faces inférieures des feuilles). L\'inflorescence, blanche ou rose-mauve très pâle, est typique des Apiacées (Ombellifères) : petites fleurs pentamères disposées en ombelles composées. L\'odeur de la plante est souvent décrite comme fétide12,13, surtout en floraison ou début de fructification.', '2a9d789eefb41a5794b3c9384e9e9aef'),
(15, 'Arbres', 'Aubépine à 2 styles', 'Crataegus laevigata', 'Rosaceae', 'CrataegusLaevigata.jpg', 100, 0, 's', 2, 3, 5, 6, 'L\'Aubépine épineuse est un arbuste de 2 à 3 m mais pouvant atteindre jusqu\'à 10 m, caducifolié. Sa longévité peut atteindre 500 ans. L\'arbre rejette de souche ; hermaphrodite, sa floraison va d\'avril à mai. L\'espèce est pollinisée par les insectes ; la semence est dispersée par les oiseaux.\r\n\r\nC\'est une espèce sensible au feu bactérien.\r\n\r\nLa cécidomyie de l\'aubépine, Dasineura crataegi, provoque des galles sur les pousses de la plante.', 'c8435667a8901e3b00fac1a879672722'),
(16, 'Arbres', 'Aubébine rouge \'Paul\'s Scarlet\'', 'Crataegus media \'Paul\'s Scarlet\'', 'Rosaceae', 'CrataegusMediaPaulsScarlet.jpg', 100, 3, 's', 2, 3, 4, 6, 'Aucune description définie', '4c66ed5e67134bc5d951f9f1fa2d7885'),
(17, 'Arbres', 'Aubépine à 1 style', 'Crataegus monogyna', 'Rosaceae', 'CrataegusMonogyna.jpg', 100, 3, 'mo - s', 2, 3, 5, 6, 'L\'Aubépine monogyne est un arbrisseau hermaphrodite pouvant mesurer de 4 à 10 m. Sa longévité peut atteindre 500 ans (record de 1 700 ans en Mayenne).\r\n\r\nSon feuillage est caducifolié.\r\n\r\nL\'arbuste fleurit au printemps, il est pollinisé par les insectes (pollinisation entomogame).\r\n\r\nC\'est une espèce pionnière dont les drupes rouges sont dispersées par les oiseaux.\r\n\r\nL\'espèce est sensible au feu bactérien.', 'e8e75209a98919da7704899886bdb64c'),
(18, 'Arbres', 'Aubépine persimilis \'Splendens\'', 'Crataegus persimilis \'Splendens\'', 'Rosaceae', 'CrataegusPersimilisSplendens.jpg', 100, 0, 's', 2, 3, 5, 6, 'Aucune description définie', 'fd466be8fcc9a110272df8fac7932003'),
(19, 'Arbres', 'Aubépine', 'Crataegus x lavallei', 'Rosaceae', 'CrataegusXLavallei.jpg', 100, 0, 'Soleil', 2, 4, 5, 5, 'Aucune description définie', 'dfbe0c54b440d78ea6445c69e4d138ae'),
(21, 'Arbuste', 'Genêt à balais', 'Cytisus scoparius', 'Fabacea', 'CytisusScoparius.jpg', 50, 2, 's', 2, 2, 5, 6, 'Le genêt à balais atteint sa maturité sexuelle à l’âge de trois ans. La floraison, avec une très importante production de fleurs, se déroule entre fin avril et début juillet. Il se couvre alors entièrement d\'une multitude de fleurs jaunes, de 15 à 20 mm de largeur et de 20 à 30 mm de longueur, qui laissent rapidement voir les étamines. La fleur de structure complexe et dépourvue de nectar est pollinisée par les bourdons. Une autre source écrit à propos du nectar : \" Les abeilles recherchent avidement sur les fleurs le nectar très sucré et très condensé qui perle en très petites gouttelettes à la base du tube des étamines extérieurement et aussi à la partie interne du calice. Lorsque les abeilles reviennent à la ruche après la visite de ces fleurs, elles ont leur corps jauni par le pollen qui s\'y est attaché de tous les côtés. Elle est fermée jusqu\'à ce qu\'elle soit visitée par l\'insecte puis reste ensuite ouverte.\r\n\r\nÀ la fin de l\'été, ses gousses oblongues, de 2 à 3 cm de long, 8 mm de large et 2 à 3 mm d\'épaisseur, deviennent noires, éclatent avec un bruit sec et répandent leurs graines (entre 5 et 6 graines par gousse) autour de la plante mère, soit entre 1 060 et 5 000 graines par adulte. Cette production élevée de graines, associée à une croissance rapide, explique le pouvoir colonisateur de ce genêt qui mène à un appauvrissement de la diversité végétale par compétition spatiale et temporelle. L’introduction d’herbivores spécialistes permet de rompre ce processus invasif,', '3e976aabc1bbb53101ac1e5aeeac113e'),
(22, 'Annuelle et bi-annulle', 'Carotte sauvage', 'Daucus carota', 'Apiaceae', 'DaucusCarota.jpg', 100, 2, 'S-mo', 3, 1, 6, 9, 'La carotte sauvage est une plante herbacée érigée, souvent annuelle, parfois bisannuelle, faisant jusqu\'à 50 cm de hauteur au stade de maturité végétative, et jusqu\'à 150 cm au moment de la floraison. La racine pivotante droite, conique à cylindrique, de 5 à 50 cm de long et de 2 à 5 cm de diamètre au niveau du collet, comporte de nombreuses racines secondaires fibreuses. Tout comme le radis et la betterave, la tubérisation de cette racine correspond à l\'hypertrophie de l\'hypocotyle et d\'une petite partie de la racine pivot. Cette transformation de la racine en organe de réserve, le tubercule, « est due principalement au développement du phloème secondaire. Le xylème secondaire développé participe néanmoins au stockage des glucides, principalement du saccharose, dans les vacuoles des cellules parenchymateuses »4. La plante a une solide tige, pleine, avec une consistance plutôt coriace, à section ronde. Elle a une surface striée, rêche et hispide. Elle est parfois ramifiée dans sa partie supérieure. Elle est parcourue par des canaux sécréteurs, particulièrement abondants dans les cannelures de la hampe florale5 qui fait de 10 à 55 cm de long.\r\n\r\nLes feuilles basales de la rosette ont des pétioles de 2 à 12 cm de long. Oblongues à triangulaires, elles sont composées pennées (feuille bi à tripennatiséquée), formées de 10 à 15 folioles, elles-mêmes formées de segments séparés linéaires à lancéolés (de 2 à 15 mm de long, 0.8 à 4 mm de large). Leur face supérieure est glabre ou glabrescente, leur face inférieure poilue. Les feuilles caulinaires sont subpétiolées ou ont des pétioles dressés et des lobules terminaux petits ou minces.', '7dbc023c10007d4421c5b758e2daedd4'),
(23, 'Bi-annuelle', 'Cardère', 'Dipsacus fullonum', 'Caprifoliaceae', 'DipsacusFullonum.jpg', 200, 2, 'mo', 2, 1, 7, 8, 'C\'est une plante bisannuelle, de 70 cm à 1,5 m de haut. Les feuilles opposées par paires le long de la tige sont soudées par leur base deux à deux et forment une cuvette dans laquelle l\'eau de pluie peut s\'accumuler, d\'où le nom vernaculaire de « cabaret des oiseaux ».\r\n\r\nLes fleurs, de couleur rose lilas, sont groupées en capitules ovales de 5 à 9 cm de long. Ces capitules sont entourés d\'un involucre formé de longues bractées munies d\'aiguillons piquants. De petites bractées piquantes sont insérées entre les fleurs.\r\n\r\nCette espèce est originaire d\'Afrique du Nord (Maghreb), du Proche-Orient et d\'Europe, depuis les îles Britanniques, le Benelux, l\'Allemagne et la Pologne jusqu\'au bassin méditerranéen. Elle s\'est naturalisée dans toutes les régions tempérées, notamment en Amérique du Nord.', '4441b0dc5f244d783e5faaba1388e306'),
(24, 'Arbres', 'Eucalyptus', 'Eucalyptus globulus', 'Myrtaceae', 'EucalyptusGlobulus.jpg', 0, 3, 's', 3, 1, 5, 8, 'L\'écorce du gommier bleu pèle en larges bandes. Les feuilles des arbres juvéniles apparaissent par paires sur des tiges carrées. Elles mesurent de 6 à 15 cm de long et sont couvertes d\'une pruine cireuse bleu-gris, qui est à l\'origine du nom de « gommier bleu ». Les feuilles des arbres matures sont alternes, étroites, en forme de faux et d\'un vert foncé luisant. Elles poussent sur des tiges cylindriques et mesurent de 15 à 35 cm de long. Les boutons floraux en forme de toupie sont côtelés et recouverts d\'un opercule aplati portant un bouton central. Les fleurs couleur crème sont solitaires à l\'aisselle des feuilles et produisent un abondant nectar que les abeilles transforment en un miel à saveur prononcée. Les fruits ligneux mesurent de 1,5 à 2,5 cm de diamètre ont une capsule très dure. De nombreuses petites graines s\'échappent par des valves qui s\'ouvrent sur le dessus du fruit', '7e7eb5e3012631f36b7cf98235b3eaeb'),
(25, 'Arbres', 'Eucalyptus', 'Eucalyptus gunnii', 'Myrtaceae', 'EucalyptusGunnii.jpg', 0, 3, 's', 3, 1, 5, 8, 'Cet eucalyptus à croissance très rapide (1 à 1,5 m par an les 5 premières années) peut atteindre 25 m de haut et 15 m de large. Son port est touffu, dressé puis étalé. L\'écorce, lisse, vert blanchâtre, se détache tous les ans, en laissant apparaître la nouvelle écorce vert jaunâtre à grisâtre, parfois teintée de rose. Il forme souvent des troncs multiples.\r\n\r\nSon beau feuillage bleu argenté est persistant toute l\'année (sauf en cas de grands froids). Les jeunes feuilles aromatiques, vert grisé, presque rondes, s\'allongent en vieillissant. Elles dégagent des huiles essentielles lorsqu’on les froisse ou qu’on les brûle.\r\n\r\nSi le climat est assez chaud, il donne, en fin d\'été et dès sa cinquième année, de nombreuses ombelles de 3 fleurs blanches à crème.\r\n\r\nLes petits fruits en forme de cupules produisent les graines, où l’huile essentielle est très concentrée.', 'f96e2195205b1a4655dcf6b221c6266d'),
(26, 'Arbuste', 'Fusain d\'Europe', 'Euonymus europaeus', 'Celastraceae', 'EuonymusEuropaeus.jpg', 0, 2, 'mo-s', 2, 1, 4, 5, 'C\'est un arbuste commun en France, pouvant mesurer de 3 à 8 mètres, tiges dressées, ramifiées, vert mat, presque quadrangulaires (souvent marquées de 4 crêtes blanchâtres plus ou moins liégeuses) aux feuilles opposées, aux feuilles finement dentées, aux petites fleurs vert-jaunâtre. Cet arbuste est surtout remarquable par ses fruits à l\'automne, des capsules roses laissant voir à maturité des graines orange, ou capsules rose vif laissant voir des graines rouges brillantes. La graine est en fait entièrement enveloppée d\'une arille, qui lui donne sa couleur. Leur aspect les a fait surnommer « bonnets d\'évêque ». À l\'automne, son feuillage se colore partiellement en rouge, parfois vif, ce qui en fait une plante très appréciée dans les haies pour son aspect décoratif.\r\n\r\nIl a une croissance rapide, mais une faible longévité1.\r\n\r\nIl est commun partout sauf en méditerranée.\r\n\r\nEspèce héliophile ou mi-ombre.\r\n\r\nOn le trouve dans les haies, les lisières, les bois, fruticée, les hêtraies.\r\n\r\nIl se multiplie facilement par semis, bouture, marcotte.', '1419af3ca57a5a97c6a16fe89cec596c'),
(27, 'Vivaces', 'Eupatoire chanvrine', 'Eupatorium cannabinum', 'Asteraceae', 'EupatoriumCannabinum.jpg', 50, 2, 'mo-s', 2, 3, 7, 10, 'L\'eupatoire chanvrine est une grande plante vivace, de 60 cm à 1,50 m de haut, à tige dressée, simple ou rameuse, en général rougeâtre, pubérulente, poussant sur un court rhizome.\r\n\r\nLes feuilles opposées sont à 3-5 segments pétiolulés et ont une nervation pédalée, chaque segment est lancéolé et denté. Elles sont glanduleuses en dessous, rappelant celles du chanvre. Parfois, les feuilles supérieures sont simples, lancéolées et légèrement alternes.\r\n\r\nLes inflorescences sont des racèmes corymbiformes de capitules, terminaux, compacts, en têtes pourprées aux fleurons blancs, rosés ou purpurins. La floraison a lieu à la fin de l\'été, au début d\'automne.\r\n\r\nLes fruits sont des akènes avec le calice adhérent (cypselae) de 2-3 mm et une aigrette de 20-30 poils de 3-5 mm.', '031fa9cf501c95661c0d82591be1a3d1'),
(28, 'Arbuste', 'Bourdaine', 'Frangula alnus', 'Ramnaceae', 'FrangulaAlnus.jpg', 200, 1, 'mo - s', 2, 2, 6, 10, 'C\'est un arbrisseau haut de 1 à 5 mètres.\r\n\r\nL\'écorce se dédouble facilement : l\'externe est brun-noir, l\'interne est verte. De nombreuses lenticelles grisâtres et allongées sont apparentes en surface ; l\'écorce exhale une odeur forte et désagréable.\r\n\r\nLes tiges sont élancées, les rameaux sont alternes.\r\n\r\nLes feuilles sont alternes, abovales, apiculées, à bords lisses, glabres, à 7-9 paires de nervures arquées courtement pétiolées, et vert brillant au-dessus3.\r\n\r\nLes fleurs, petites et verdâtres et dont la floraison s\'étale d\'avril à juillet, donnent des fruits globuleux d\'abord verts puis rouges et enfin noirs à maturité. Ils sont alors frais, juteux, un peu sucrés mais toxiques.\r\n\r\nLeurs bourgeons sont brun et nus, ils ne possèdent pas d\'écailles.', 'f4a16e939838836760678d31412a65da'),
(29, 'Non defini', 'Frêne à fleurs \'Meczek\'', 'Fraxinus ornus \'Meczek\'', 'Oleaceae', 'FraxinusOrnusMeczek.jpg', 0, 2, 's', 3, 2, 5, 6, 'Le frêne à fleurs est un arbre de petite taille (7 à 10 mètres de haut) originaire du Sud de l\'Europe. Son écorce grisâtre et lisse le distingue de son « cousin » méditerranéen, le Frêne oxyphylle (Fraxinus angustifolia).\r\n\r\nIl possède des feuilles composées de 20 à 30 cm de long avec 5 à 9 folioles de 5 à 10 cm de long et 2 à 4 cm de large, à bord finement dentelé et ondulé, et des pétioles de 5-15 mm de long. Les bourgeons sont cendrés.\r\n\r\nLa période de floraison s\'étend d\'avril à juin. Sa floraison blanche odorante qui apparaît en même temps que les feuilles lui confère une beauté remarquable au printemps. Les fleurs à quatre minces pétales blanc crème de 5-6 mm de long sont produites en denses panicules de 10-20 cm de long au milieu du printemps.', '4ed72ee2011b6f29f50af19ee34f77b8'),
(30, 'Bulbeuse', 'Perce-neige', 'Galanthus nivalis', 'Amaryllidaceae', 'GalanthusNivalis.jpg', 50, 1, 'mo - s', 1, 1, 2, 3, 'Plante à bulbe de 15 à 25 cm de hauteur. Les fleurs sont blanches, solitaires. Floraison en janvier et février selon les régions (parfois début mars). Deux feuilles vert glauque de 4 à 8 mm de large. Fruit ovoïde et allongé.', '73039cf7eea967bfe6385774e4e2a201'),
(31, 'Vivaces', 'Lierre terrestre', 'Glechoma hederacea', 'Lamiaceae', 'GlechomaHederacea.jpg', 200, 2, 'o -mo - s', 3, 1, 3, 5, 'C\'est une plante vivace et rampante de 5 à 40 cm de hauteur. Hémicryptophyte stolonifère, elle assure sa multiplication végétative par des stolons épigés (avec présence quelquefois de rosettes) qui la rendent facilement envahissante. Au printemps, les tiges horizontales de cette plante tapissante poussent vers le haut, à la verticale, et émettent de nombreuses petites fleurs violacées au niveau des nœuds. Elle est nommée « lierre terrestre » en raison de sa ressemblance approximative avec le lierre grimpant et de son port rampant sur le sol sans s\'élever sur un support.\r\n\r\nLes tiges à section quadrangulaire, souvent rougeâtres à la base, sont glabres ou subglabres. Les feuilles à pétioles (de 4,5 à 8 cm de longueur) et sans stipules se présentent opposées par paires sur des tiges plus ou moins quadrangulaires. Ces feuilles vert-jaune à nervation palmée sont réniformes (forme de rein) à la base, cordiformes (forme de cœur) plus haut ; elles sont souvent légèrement duveteuses, généralement vert foncé luisant sur le dessus mais peuvent aussi être légèrement violacées ; leur face inférieure est plus claire et porte des glandes d\'huiles essentielles ; leurs bords ont des crénelures arrondies régulières et sont fréquemment ciliés ; leurs pétioles sont de longueur variable mais plus courte que les entre-nœud8', 'cb4aa08af9ff327b0c1f466b992a9ed4'),
(32, 'Sous-Arbrusseau', 'Lierre grimpant', 'Hedera helix', 'Araliaceae', 'HederaHelix.jpg', 400, 3, 'o - mo - s', 3, 3, 9, 10, 'C\'est une liane arborescente, dont l\'ancêtre est probablement d\'origine tropicale. C\'est une des rares lianes que l\'on trouve en Europe et en Asie Mineure (avec la clématite, le houblon ou le chèvrefeuille) qui forme des tiges ligneuses rampantes ou grimpantes de taille indéfinie (il atteint facilement 100 mètres de long et 30 m en hauteur, avec une croissance annuelle de 0,5 à un mètre). Ce lierre vit habituellement une centaine d\'années, pouvant atteindre 1 000 ans si le support s’y prête.', 'c82b064303ac592bbc3828601fb0bad4'),
(33, 'Vivaces', 'Sainfoin d\'espagne', 'hedysarum coronarium', 'Fabacea', 'hedysarumCoronarium.jpg', 0, 3, 's', 3, 2, 4, 7, 'Aucune description définie', 'c39eca90b2abe94b1b0fbe8e52d4a845'),
(34, 'Vivaces', 'Berce commune', 'Heracleum sphondylium', 'Apiaceae', 'HeracleumSphondylium.jpg', 200, 3, 'mo - s', 2, 2, 5, 10, 'Elle affectionne les sols riches et humides. Elle est commune dans les lisières, les fossés, les lieux embroussaillés, aux abords des haies, aux rebords des prés et des fourrières, ainsi qu\'en moyenne montagne humide. \r\nElle mesure habituellement entre 50 et 150 cm2, et atteint rarement 2 m de hauteur. Elle est nettement moins grande que la dangereuse (risque de brûlures graves à son contact par très forte photosensibilisation) et invasive Berce du Caucase (Heracleum mantegazzianum) qui peut atteindre 4 m de haut, elle est également moins phototoxique mais conserve les mêmes risques pour les personnes présentant un terrain favorable.', '4273bc87a6e8de3df9232ad6f98cbedb'),
(35, 'Bulbeuse', 'Jacinthe des bois', 'Hyacinthoides non-scripta', 'Asparagaceae', 'HyacinthoidesNonScripta.jpg', 50, 1, 'o -mo', 1, 2, 4, 5, 'La jacinthe des bois est une vivace haute de 20 à 40 centimètres. Elle a un bulbe de la taille d\'une noisette qui est muni de racines contractiles qui le font glisser plus profondément dans des couches du sol plus humides. Ses feuilles basales linéaires, par groupe de 3 ou 6, sont dressées puis recourbées. De forme lancéolée, leur limbe a une largeur de 7 à 16 millimètres.\r\n\r\nLors de la floraison (avril à mai), les fleurs sont regroupées sur un racème unilatéral semi_pendant (généralement 5–12 fleurs, exceptionnellement 3–32) qui donne à la plante l\'aspect de dormir. Leurs épales sont bleu mauve, recourbés ou enroulés à leur extrémité, donnant à la fleur une forme d\'entonnoir long de 14-18 mm, muni de deux bractées à la base. La hampe florale qui monte jusqu\'à 500 mm persiste, sèche, après la disparition des feuilles en juin', 'dd77b8196b05e541d844618587ad9177'),
(36, 'Sous-Arbrusseau', 'Millepertuis à grandes feuilles', 'Hypericum calycinum', 'Hypericaceae', 'HypericumCalycinum.jpg', 0, 0, 'mo - s', 0, 2, 6, 9, 'Atteint 2 mètres de haut, à feuillage persistant, fleurissant de juin à octobre. Fleurs grandes et jaune vif.', '8c9015e305923565f4d9769b52a299dd'),
(37, 'Vivaces', 'Millepertuis étalé', 'Hypericum patulum', 'Hypericaceae', 'HypericumPatulum.jpg', 0, 0, 'mo - s', 0, 2, 6, 9, 'Aucune description définie', 'ae007c48e4ed9341aeb76309d84689c6'),
(38, 'Non defini', 'Millepertuis commun', 'Hyperricum perforatum', 'Hypericaceae', 'HyperricumPerforatum.jpg', 0, 1, 's', 0, 2, 6, 9, 'Aucune description définie', '32e411079b604a1bffc58843ddf62e8a'),
(39, 'Non defini', 'Knautie des champs', 'Knautia arvensis', 'Caprifoliaceae', 'KnautiaArvensis.jpg', 50, 2, 's', 2, 1, 4, 10, 'Plante vert grisâtre de 30 à 60 cm, avec des fleurs en capitule rayonnant.\r\n\r\nfeuilles : opposées, simple, de couleur gris-vert; feuilles basales pétiolées et feuilles caulinaires pennatifides ou sans lobes1.\r\ntige : à poils écartés au-dessous du capitule2.\r\nfleurs : capitule de 1,5-3 cm de diamètre, de couleur bleu-violet, entouré d\'un involucre, réceptacle hémisphérique hérissé de soies, calice terminé par des arêtes dressées1.', '56048dc6d38c4d1c1a9f187c1f469929'),
(40, 'Sous-Arbrusseau', 'Lavande', 'Lavandula angustifolia', 'Lamiaceae', 'LavandulaAngustifolia.jpg', 0, 3, 's', 4, 1, 4, 6, 'La lavande est une plante vivace, qui prend la forme d’un sous arbrisseau. Elle est composée de hampes florales comportant un seul épi. Elle mesure 30 à 60 cm de haut', '1fb993859f2028aecd7eab0e184fd5cf'),
(41, 'Non defini', 'Lycope d\'Europe', 'Lycopus europeaus', 'Lamiaceae', 'LycopusEuropeaus.jpg', 0, 2, 'Mo-o', 3, 1, 7, 9, 'Le Lycope d\'Europe est une plante herbacée vivace, non aromatique, pouvant atteindre de 30 centimètre à un mètre de haut, voire 1,20 m, à souche rampante et à tiges dressées à section carrée, marquée d\'un sillon sur chacune des faces. La plante est glabre ou légèrement pubescente. La plante est vivace par sa souche qui produit de nombreux stolons. C\'est une hélophyte-hémicryptophyte (cf. Classification de Raunkier).\r\n\r\nLes feuilles, généralement de couleur verte, opposées-décussées, ont un limbe de forme ovale-lancéolée à pointe aiguë et profondément denté, et même parfois divisé à la base pour les feuilles inférieures. Le pétiole est court, voire absent pour les feuilles supérieures. .Les fleurs petites (environ 4 mm) sont groupées à l\'aisselle des feuilles en faux verticilles de forme globuleuse. Le calice denté, couvert de poils, comporte cinq divisions terminées en pointe. La corolle est formée de quatre lobes de couleur blanche ponctuée de rouge, formant un entonnoir terminé par deux lèvres, le lobe supérieur étant légèrement échancré. Les deux étamines fertiles dépassent la corolle, les deux étamines inférieures sont avortées. La floraison a lieu de juillet à septembre.\r\n\r\nLes graines sont dispersées notamment par les oiseaux aquatiques', 'cfe313decbbe9aaa408ef025b79db1be'),
(42, 'Vivaces', 'Salicaire commune', 'Lythrum salicaria', 'Lythraceae', 'LythrumSalicaria.jpg', 200, 2, 'mo', 3, 1, 6, 9, 'C\'est une plante eurasiatique subocécanique, circumboréale qu\'on rencontre pratiquement partout en Europe, à l\'exception des régions boréales. Elle est présente en Amérique du Nord et en Australie où elle est devenue une plante invasive; elle est classée parmi les espèces les plus invasives au xxie siècle, notamment en Amérique du Nord. Plante assez haute, dépassant souvent un mètre. La tige, velue, de couleur brun rougeâtre, porte quatre lignes longitudinales saillantes. Feuilles le plus souvent opposées, les supérieures sessiles, assez étroites et lancéolées.', '91dca6960ff190b17eaa8cc9f13e84a5'),
(43, 'Vivaces', 'Mauve musquée', 'Malva moschata', 'Malvaceae', 'MalvaMoschata.jpg', 0, 2, 'mo-s', 1, 3, 6, 8, 'La plante touffue, ramifiée et vivace, couverte de poils simples, est de taille moyenne (jusqu’à 60 cm). Les tiges sont munies de feuilles profondément lobées en segments étroits. Il existe un fort polymorphisme foliaire : les feuilles bractéales supérieurs sont en général profondément découpées. Les feuilles palmatilobées de 2–8 cm de long et 2–8 cm de large possèdent de 5 à 7 lobes plus pointus que Malva alcea, tous pennatifides. L\'inflorescence est composée de fleurs isolées à l\'aisselle des bractées inférieures et médianes. Les grandes fleurs (3,2 – 5 cm) roses qui s\'épanouissent de juin à octobre ont une odeur musquée caractéristique et un calicule formé de trois pièces libres (3 divisions linéaires) très peu velues sur le dos. Leurs pétales varient autour du mauve (roses, lilas, violacés) et bleuissent à la dessiccation. Les carpelles velus, lisses, noircissent à maturité. Le fruit est formé d\'un verticille de 10 à 16 akènes (disposés en tranches d\'orange et hérissés sur tout le dos de longs poils).', '5f9fd0cd91dd4fc50cb98aac56427cbe'),
(44, 'Non defini', 'Marrube blanc', 'Marrubium vulgare', 'Lamiaceae', 'MarrubiumVulgare.jpg', 0, 2, 's', 3, 1, 5, 9, 'Son odeur de thym la distingue d\'autres plantes.\r\n\r\nC’est une plante pérenne de couleur grisonnante ressemblant légèrement à la menthe, et qui peut atteindre 25 à 45 cm de hauteur. Ses feuilles duveteuses ont une longueur de 2 à 5 cm et un aspect froissé. Les fleurs sont blanches et comme beaucoup d’autres Lamiacées, le marrube a une tige carrée.', '4035755cfeb0d184fef922a65c66a9ac'),
(45, 'Annuelle et bi-annulle', 'Luzerne cultivée', 'Medicago sativa', 'Fabacea', 'MedicagoSativa.jpg', 350, 3, 's', 3, 1, 6, 9, 'C\'est une plante herbacée de 30 à 80 cm de hauteur, vivace par ses tiges souterraines ramifiées.\r\n\r\nLes feuilles, à trois folioles oblongues, pubescentes, dentées au sommet, sont d\'un vert gris.\r\n\r\nSes fleurs violettes groupées en grappes fournies sont très reconnaissables.\r\n\r\nLes fruits sont des gousses recourbées en hélice senestre sur deux à trois tours.\r\n\r\nLe système racinaire de la luzerne est particulièrement développé et lui permet d\'atteindre des profondeurs importantes (plusieurs mètres). Cette particularité lui confère une excellente résistance à la sécheresse ainsi qu\'une certaine capacité à décolmater les sols et à améliorer leur perméabilité . En outre les nodosités qui se forment sur ses racines, comme pour les autres légumineuses, lui confèrent la capacité de fixer l\'azote atmosphérique et d\'enrichir ainsi le sol.', 'c1cb8b8f07b5bd1b3fb9327555306cb0'),
(46, 'Annuelle et bi-annulle', 'Mélilot blanc', 'Melilotus albus', 'Fabacea', 'MelilotusAlbus.jpg', 200, 3, 's', 3, 2, 6, 9, 'Aucune description définie', '1d8322563daefc6b22dccb6be43fe38d'),
(47, 'Arbres', 'Eucalyptus', 'melliodora', 'Myrtaceae', 'Melliodora.jpg', 0, 3, 's', 3, 1, 5, 8, 'Le gommier jaune ou eucalyptus à miel (Eucalyptus melliodora) est une espèce d\'eucalyptus originaire du sud-est de l\'Australie.\r\n\r\nIl doit son nom à la qualité du miel produit à partir de ses fleurs.', '65eb1ec11b3335b772a7cd64d3b25170'),
(48, 'Vivaces', 'Sainfoin', 'Onobrychis viciifolia Scop', 'Fabacea', 'OnobrychisViciifoliaScop.jpg', 200, 2, 's', 2, 0, 5, 9, 'C\'est une plante érigée, aux feuilles pennées (entre 6 et 14 paires de folioles oblongues à linéaires).\r\n\r\nSon système racinaire est puissant. La plante mesure de 50 à 70 cm de hauteur.\r\n\r\nLes fleurs apparaissent à l\'aisselle des feuilles supérieures en grappes longuement pédonculées. Elles sont de couleur rose marquées par des nervures pourpres.\r\n\r\nLes fruits sont des petites gousses dentées.', '581973482ca5c84ea8007cdb8f8220d3'),
(49, 'Vivaces', 'Petasite officinale', 'Petasites hybridus', 'Asteraceae', 'PetasitesHybridus.jpg', 0, 2, 'mo - s', 1, 3, 2, 4, 'Plante à rhizome. Les feuilles apparaissent après la floraison printanière. Elles ont leur limbe arrondi ; elles sont molles et veloutées et dépassent 50 cm de diamètre.', '2bb449dfb7e9349c3e972686dbc189f2'),
(50, 'Annuelle et bi-annulle', 'Phacélie', 'Phacelia tanacetifolia', 'Boraginaceae', 'PhaceliaTanacetifolia.jpg', 400, 3, 's', 3, 2, 6, 9, 'La tige pleine et rigide, couverte de poils raides et assez épaisse, qui peut atteindre un mètre, est teintée de rouge. Son système racinaire est dense.\r\n\r\nLes feuilles, alternes, sont profondément divisées et rappellent celles de la tanaisie (Tanecetum).\r\n\r\nLes fleurs très parfumées sont groupées en inflorescences scorpioïdes (en forme de crosse qui se déroule progressivement au fur et à mesure de l\'épanouissement de petites fleurs de 1 cm qui commence par le haut) serrées du genre cyme. Les éléments floraux ont des nuances bleu-lavande ; ce sont les cinq étamines et les deux styles qui émergent nettement de la corolle à cinq pétales. Étroit calice aux sépales poilus.\r\n\r\nLa période de floraison se situe au printemps et peut se prolonger jusqu\'en automne.\r\n\r\nLes fruits sont de petites capsules déhiscentes contenant deux à quatre petites graines noires (3 mm de long).', '436f7da3296732e8d4dfd002e9a637d1'),
(51, 'Vivaces', 'Brunelle', 'Prunella vulgaris', 'Lamiaceae', 'PrunellaVulgaris.jpg', 200, 1, 'mo - s', 3, 2, 6, 9, 'La tige carrée et dressée porte des feuilles opposées ovales ou oblongues, en coin ou arrondies à la base, pétiolées, dentées ou pinnatifides, un peu velues, dont la paire supérieure entoure l\'épi floral1,2..\r\n\r\nLes fleurs bleu-violets, parfois ocre, roses, mauve ou blanches, sont disposées en épi terminal serré et cylindrique, séparées entre elles par de larges bractées opposées, ciliées et colorées. Les fleurs, assez petites, sont en forme de calice à deux lèvres, la lèvre supérieure à trois dents peu prononcées et la lèvre inférieure est fendue jusqu\'au milieu en deux lobes1,2.\r\n\r\nRarement cultivée, la Brunelle commune se multiplie par semis ou par division de la racine.', '8c80e3ddadcb69692708e63d49ac3f0d'),
(52, 'Non defini', 'Rhododendron ferrugineux', 'Rhododendron ferrugineum', 'Ericaceae', 'RhododendronFerrugineum.jpg', 0, 3, 's', 3, 1, 6, 7, 'Ce Rhododendron est un arbuste sempervirent à longue durée de vie (pouvant atteindre 300 ans au moins), qui domine et structure des landes à éricacées sur les versants nord à nord-ouest de l’étage subalpin (1 400 m-2 200 m). Sa floraison spectaculaire (en moyenne 300 inflorescences de 5 à 22 fleurs/m2) et sa croissance débutent simultanément environ 15 jours après la fonte des neiges et se déroulent sur à peine plus d’un mois lorsque les conditions climatiques lui sont favorables (entre fin mai et début août selon les altitudes)', '82be3e839be80e89cece61e0edb489ea'),
(53, 'Arbuste', 'Groseiller à grappes', 'Ribes rubrum', 'Grossulariaceae', 'RibesRubrum.jpg', 200, 3, 'mo - s', 3, 3, 3, 5, 'Selon les variétés, les fruits peuvent prendre diverses couleurs à maturité.\r\n\r\nPériode de floraison : mars, avril\r\nCouleur des fleurs : jaune-vert\r\nExposition : soleil, mi-ombre\r\nType de sol : ordinaire, riche en humus, bien drainé\r\nUtilisation : jardin fruitier\r\nHauteur : 150 cm\r\nType de plante : arbuste fruitier\r\nType de végétation : vivace\r\nType de feuillage : caduc\r\nRusticité : rustique\r\nToxicité : fruits comestibles', '9641bec48de34c483e51aa2c60a1c5f3'),
(54, 'Arbuste', 'Groseiller sanguin', 'Ribes sanguineum', 'Grossulariaceae', 'RibesSanguineum.jpg', 200, 3, 'mo - s', 3, 3, 3, 5, 'Le groseillier à fleurs est un arbuste au port arrondi à feuilles caduques pouvant mesurer 2 à 2,5 m de haut et de large.\r\n\r\nL\'écorce est gris brunâtre avec des lenticelles brun pâle proéminentes.\r\n\r\nLes feuilles palmées à cinq lobes font 7 cm de long et 2 cm de large; lorsqu\'elles sont jeunes au printemps, elles ont un fort parfum résineux.\r\n\r\nLes fleurs sont produites au début du printemps en même temps que les feuilles apparaissent, sur des grappes ballantes de 3-7 cm de long avec 5-30 fleurs; chaque fleur mesure 5-10 mm de diamètre, avec cinq pétales roses, rouges ou blancs.\r\n\r\nLe fruit est une baie ovale violet foncé d\'environ 8 mm de long, comestibles, mais sèche et au goût fade.', 'd0f9edc62a3b745f70f2f913504a052c'),
(55, 'Arbuste', 'Groseiller à maquereaux', 'Ribes uva-crispa', 'Grossulariaceae', 'RibesUvaCrispa.jpg', 200, 3, 'mo - s', 3, 3, 3, 5, 'Cet arbuste fruitier rustique à feuillage caduc et à rameaux plus ou moins épineux atteint 50 à 150 cm de haut.\r\n\r\nPériode de floraison : printemps\r\nCouleur des fleurs : blanc\r\nExposition : soleil, mi-ombre\r\nType de sol : ordinaire\r\nUtilisation : jardin fruitier\r\nCes groseilles doivent leur nom au fait que leur jus peut servir d\'assaisonnement aux plats à base de maquereaux. Il peut également aciduler les sauces. Ces arbustes ressemblent aux groseilliers classiques (Ribes rubrum, groseillier à grappes ou Ribes nigrum, cassissier) avec des feuilles un peu plus grandes et de fines épines aux branches.\r\n\r\nLa véritable différence se voit sur les fruits, qui ne sont pas en grappes mais individuels, ovales ou ronds mais bien plus gros. De la taille d\'une cerise à eau-de-vie, la groseille à maquereau garde le goût caractéristique acidulé des groseilles en grappes.\r\n\r\nLes fruits de certaines variétés atteignent la taille d\'un œuf de pigeon.', '331b859f69fb26a414d09bd5f694da28'),
(56, 'Non defini', 'Robinier faux acacia', 'Robinia pseudoacacia', 'Fabacea', 'RobiniaPseudoacacia.jpg', 0, 3, 's', 3, 3, 5, 7, 'C\'est un arbre qui atteint 20 à 30 m de haut pour 1 m de diamètre 2. Il est très souvent drageonnant et forme des bosquets parfois envahissants. Le tronc gris brun est souvent double avec une écorce épaisse profondément crevassée dans le sens longitudinal. Les drageons et jeunes branches sont épineux. En taillis, il peut faire des pousses de 20 m. Plus le terrain est mauvais et plus il drageonne et les fourrés font leur action protectrice. Il pourrait préparer l\'arrivée d\'autres espèces mais il est assez inexpugnable.\r\n\r\nLes feuilles caduques apparaissent tard au printemps. Elles sont imparipennées, avec un grand nombre (de 9 à 23) de folioles ovales; les stipules des feuilles portées par les rameaux non florifères sont transformées en aiguillons aiguës, qui persistent plusieurs années après la chute des feuilles.\r\n\r\n\r\nRameau de Robinier faux-Acacia\r\nLes fleurs qui apparaissent entre mai et juin sont blanches, en grappes pendantes parfumées et mellifères de 10 à 25 cm de long. Le robinier est une importante plante mellifère mais la sécrétion de nectar n’est abondante que par une température supérieure à 20°C et est arrêtée par les températures basses et par la pluie. Le miel du robinier faux-acacia n’est jamais commercialisé sous ce nom, mais sous celui de \"miel d’acacia\", ce qui est botaniquement faux mais est une appellation tolérée par l’usage.\r\n\r\nLes fruits sont des gousses aplaties, de 7 à 12 cm de long, contenant 4 à 12 graines brunes de six à sept millimètres de long à tégument très dur. Elles restent fixées à l\'arbre bien après la chute des feuilles.\r\n\r\nLa rhizosphère du robinier encourage des bactéries fixatrices d\'azote. Le système radiculaire peut s’étendre sur un rayon de 15 mètres autour du tronc sur les terrains secs.\r\n\r\nChaque année le bois de printemps apparaît comme un anneau poreux. Entre les cellules du parenchyme de gros vaisseaux sont visibles à l\'œil nu. À l\'automne, ces vaisseaux sont obturés par des excroissances des cellules qui les bordent, les thylles. Chez le robinier, les gros vaisseaux du bois de printemps ne conduisent la sève qu\'une saison.', 'd3d08a8b17250a344c1b7128c27678e0'),
(57, 'Non defini', 'Lilas', 'Syringa vulgaris', 'Oleaceae', 'SyringaVulgaris.jpg', 0, 2, 'mo-s', 2, 2, 4, 5, 'L\'arbuste, le plus souvent à plusieurs tiges, peut atteindre une hauteur de 6 à 7 mètres. Il produit des pousses secondaires (\"drageons\") depuis sa base ou ses racines. Sa souche peut alors mesurer jusqu\'à 20 cm de diamètre et, avec le temps, peut produire un petit bosquet clonal. Son écorce est grise à gris brun, lisse sur les jeunes tiges, sillonnée longitudinalement et se desquamant sur les plus vieilles tiges.\r\n\r\nSes feuilles caduques simples non coriaces mesurent 4 à 12 cm et 3-8 cm de large. Elles sont vert clair à glauque, opposées ou parfois verticillées, pétiolées et pointues au sommet.\r\n\r\nL\'inflorescence est une grappe composée appelée thyrse. Leur amertume leur évite d\'être broutées.\r\n\r\nLes fleurs parfumées comportent quatre pétales, la corolle formant un tube de 6-10 mm de long à la base. Elles sont le plus souvent de couleur lilas, mais elles peuvent aussi être blanches ou rougeâtres en passant par le bleu violacé et le rose carmin. Il existe des variétés à fleurs simples ainsi qu\'à fleurs doubles. Elles sont disposées en denses panicules terminales de 8-18 cm de long.\r\n\r\nSon fruit est une capsule brune de 1-2 cm de long, se fractionnant en deux pour libérer deux graines ailées.', '4676cfb71e2dc6e968d79872f0c8562f'),
(58, 'Annuelle et bi-annulle', 'Pissenlit', 'Taraxacum officinale', 'Asteraceae', 'TaraxacumOfficinale.jpg', 200, 3, 's - mo', 3, 3, 4, 10, 'Taraxacum officinale est un nom scientifique qui a décrit une espèce puis un agrégat d\'espèces du genre Taraxacum. C\'est-à-dire, en termes vernaculaires, que ce nom a été donné à un pissenlit puis à un ensemble de plusieurs espèces de pissenlits.\r\n\r\nLe nom Taraxacum officinale n\'est plus valide même s\'il reste très employé. On l\'utilise par facilité, même s\'il est source de confusion dans une classification du genre Taraxacum déjà très complexe.', 'a92a682f12bacb0bb3d91b30a54ee726'),
(59, 'Vivaces', 'Germandrée scorodoine', 'Teucrium scorodonia', 'Lamiaceae', 'TeucriumScorodonia.jpg', 200, 3, 's - mo', 3, 1, 6, 9, 'C\'est une plante mellifère moyenne (15–50 cm) à feuilles opposées ressemblant à celles de la sauge officinale. Les fleurs sont blanc-jaunâtre avec des étamines à filet violet.', '02cf7cf68abee20016c7f2a81131f437'),
(60, 'Vivaces', 'Petite pervenche', 'Vinca minor', 'Apocynaceae', 'VincaMinor.jpg', 50, 2, 'o - mo', 3, 1, 3, 5, 'Sur une base sarmenteuse, se forment des tiges rampantes, érigées, portant des feuilles opposées, ovales, vert foncé, parcheminées, qui produisent des fleurs terminales solitaires, bleues, à 5 pétales carrés asymétriques.', 'b3f5b1e2bcf19bcc0a4515b48e992dec');

-- --------------------------------------------------------

--
-- Structure de la table `role`
--

DROP TABLE IF EXISTS `role`;
CREATE TABLE IF NOT EXISTS `role` (
  `name` varchar(20) DEFAULT NULL,
  `type` int(11) NOT NULL,
  PRIMARY KEY (`type`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `role`
--

INSERT INTO `role` (`name`, `type`) VALUES
('Administrator', 1),
('User', 0);

-- --------------------------------------------------------

--
-- Structure de la table `utilisateurs`
--

DROP TABLE IF EXISTS `utilisateurs`;
CREATE TABLE IF NOT EXISTS `utilisateurs` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `pseudo` varchar(36) CHARACTER SET utf8 NOT NULL,
  `motDePasse` varchar(200) CHARACTER SET utf8 NOT NULL,
  `mail` varchar(30) DEFAULT NULL,
  `prenom` varchar(30) DEFAULT NULL,
  `nom` varchar(30) DEFAULT NULL,
  `privileges` int(11) DEFAULT NULL,
  `verif` int(11) NOT NULL DEFAULT '0',
  `tempemail` varchar(100) NOT NULL,
  `ban` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=61 DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `utilisateurs`
--

INSERT INTO `utilisateurs` (`id`, `pseudo`, `motDePasse`, `mail`, `prenom`, `nom`, `privileges`, `verif`, `tempemail`, `ban`) VALUES
(59, 'Axel44', '$2y$10$SNyhSlTPPVr4iSZigvo6Xe3mQeL2Gm03jma8FgCsrFlznfZVgspdG', 'machin@yopmail.com', 'Axel', 'Soulard', 1, 1, 'machin@yopmail.com', 0),
(60, 'test2', '$2y$10$hncKXpu1Dd3FASssgxDJVeOANxxzpkOxCrevX7GoaC0xy0FeVM3WG', 'truc1@yopmail.com', 'john', 'fdghj', 0, 1, '', 0);

-- --------------------------------------------------------

--
-- Structure de la table `veriflink`
--

DROP TABLE IF EXISTS `veriflink`;
CREATE TABLE IF NOT EXISTS `veriflink` (
  `id` int(11) NOT NULL,
  `link` varchar(130) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `veriflink`
--

INSERT INTO `veriflink` (`id`, `link`) VALUES
(67, '5c498a5f29b028.44647140'),
(68, '5c498a8574c990.57529288'),
(59, '5c4ae7a931d297.07802338'),
(59, '5c4ae7d01f8671.34274965'),
(59, '5c4ae8311c84c6.53726217'),
(59, '5c4ae86fc44f64.33564421'),
(59, '5c4ae88b31d633.32121344'),
(59, '5c4ae89fd03230.62033850'),
(59, '5c4ae8b45c2156.88314225'),
(59, '5c4ae8b994ee37.11707776'),
(59, '5c4ae92fc15fa7.59975522'),
(59, '5c4ae915627118.53770071'),
(59, '5c4ae95518e152.64649070'),
(59, '5c4ae98f70c4b3.39149641');
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
