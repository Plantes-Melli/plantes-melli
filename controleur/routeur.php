<?php

include_once "controleurInscription.php";
include_once "controleurCompte.php";
include_once "controleurPlantes.php";
include_once "controleurNavigation.php";
include_once "controleurAdministration.php";
include_once "controleurVerification.php";
include_once "controleurListes.php";
include_once "controleurForgot.php";
include_once PATH_MODELE."/Compte.php";



class Routeur
{
	private $ctrlInscription;
	private $ctrlCompte;
	private $ctrlPlantes;
	private $ctrlNavigation;
	private $ctrlAdministration;
	private $ctrlVerification;
	private $ctrlListes;
	private $ctrlForgot;

	public function __construct()
	{
		$this->ctrlInscription = new ControleurInscription();
		$this->ctrlCompte			 = new ControleurCompte();
		$this->ctrlPlantes 		 = new ControleurPlantes();
		$this->ctrlNavigation  = new ControleurNavigation();
		$this->ctrlAdministration = new ControleurAdministration();
		$this->ctrlVerification = new ControleurVerification();
		$this->ctrlForgot = new ControleurForgot();
		$this->ctrlListes = new ControleurListes();
	}

	// Traite une requête entrante
	public function routerRequete()
	{



		/////////////////////NAVIGATION///////////////////////////
		$page = "Accueil";
		$pseudo = NULL;

		if(isset($_GET["page"])){
				$page = $_GET["page"];
		}

		if (isset($_GET["deconnexion"])) {
			$this->ctrlCompte->deconnexion();
		}

		if ($this->ctrlCompte->estConnecte()){
			$pseudo = $_SESSION["compte"]->getPseudo(); //on devrait pouvoir remplacer par controleurCompte->getPseudo();
		}


		$this->ctrlNavigation->afficherHeader($page,$pseudo);

		//le switch permet le chamgement de page dans le site web.
		switch ($page) {
			case 'Compte':
				$this->ctrlCompte->route();
			break;


			case 'inscription':
				$this->ctrlInscription->route();
			break;


			case 'MentionsLégalesCGU':
			$this->ctrlNavigation->afficheMentionsLégalesCGU();
			break;

			case 'PolitiqueConfidentialite':
			$this->ctrlNavigation->affichePolitiquesConfidentialites();
			break;

			case 'SavoirPlus':
			$this->ctrlNavigation->afficheSavoirPlus();;
			break;

			case 'Contact':
			$this->ctrlNavigation->afficheContact();
			break;

			case 'errorjs':
				$this->ctrlNavigation->afficherErreur();
			break;

			case 'Plantes':
				$this->ctrlPlantes->route();
			break;

			case 'Administration':
				$this->ctrlAdministration->route();
			break;

			case 'Verify':
				$this->ctrlVerification->route();
			break;

			case 'forgotpass':
				$this->ctrlForgot->route();
				break;
			case 'Listes':
				$this->ctrlListes->route();
				break;


			default:
				$this->ctrlNavigation->afficherAccueil();
			break;
		}




		$this->ctrlNavigation->afficherFooter();
	}
}
?>
