package fr.iut.plantes_melli.vue;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Environment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import java.io.File;
import java.util.List;

import androidx.annotation.NonNull;
import fr.iut.plantes_melli.R;
import fr.iut.plantes_melli.modele.Plante;
import fr.iut.plantes_melli.utils.ImageDecoder;

//adapter pour afficher une plante dans un listeView
public class PlanteAdapter extends ArrayAdapter {
    List<Plante> dataModel;


    public PlanteAdapter(@NonNull Context context, @NonNull List<Plante> objects) {
        super(context, R.layout.plante, objects);
        this.dataModel = objects;
    }


    public class ViewHolder{
        ImageView img;
        TextView nomFr;
        TextView nomLat;
        RatingBar apicole;
    }

    @NonNull
    @Override
    public View getView(int position,View convertView,ViewGroup parent) {
        View row = convertView;
        ViewHolder viewHolder;

        if(row == null){
            LayoutInflater inflater = LayoutInflater.from(getContext());
            row = inflater.inflate(R.layout.plante,parent,false);
            viewHolder = new ViewHolder();
            viewHolder.img = row.findViewById(R.id.image);
            viewHolder.nomFr = row.findViewById(R.id.Nom);
            viewHolder.nomLat = row.findViewById(R.id.NomLat);
            viewHolder.apicole = row.findViewById(R.id.apicole);
            row.setTag(viewHolder);
        }else{
            viewHolder = (ViewHolder) row.getTag();
        }




        Plante p = (Plante) getItem(position);
        viewHolder.img.setImageBitmap(ImageDecoder.getBitMapImage("/mini/"+p.getImage()));
        viewHolder.nomFr.setText(p.getNomFr());
        viewHolder.nomLat.setText(p.getNomLat());
        viewHolder.apicole.setRating(p.getInter_apicole());

        return row;
    }
}
