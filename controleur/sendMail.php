	<?php

use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;

require 'PHPMailer/src/Exception.php';
require 'PHPMailer/src/PHPMailer.php';
require 'PHPMailer/src/SMTP.php';



class Mailer{
	private $error;
	function __construct(){

	}


	function verifEmail($email,$pseudo,$key){


		// on récupère le contenu des variables soit elle est récupéré soit elle affiche un espace (cad rien)

		$subject = "Validation de l'Email"; //Titre global du mail
		$textMsg = '
		<html>
			<body style="text-align:justify;">
				<h1 style="color:#010620; text-decoration:underline"><center><b>Plantes mellifères</b></center></h1>
				<p><b>Bonjour </b>'.$pseudo.' !<br>
					Veuillez verifier votre compte en cliquant sur <a href="'.SITENAME.'/index.php?page=Verify&key='.$key.'">ce lien</a>.<br>
					Si ce n\'est pas vous qui vous etes inscrit sur ce site ignorez juste ce message.
				</p>
			 </body>
			</html>
			 ';
	//A voir pour mettre une image

		$to = $email;
		$from = GMAIL;
		$from_name = GUSER;

		if(!$this->sendMail($to, $from, $from_name, $subject, $textMsg)){
			return false;
		}


		return true;

	}


	function newPassEmail($email,$key){


		// on récupère le contenu des variables soit elle est récupéré soit elle affiche un espace (cad rien)

		$subject = "Recuperer votre compte"; //Titre global du mail
		$textMsg = '
		<html>
			<body style="text-align:justify;">
				<h1 style="color:#010620; text-decoration:underline"><center><b>Plantes mellifères</b></center></h1>
				<p>
					Cliquez sur <a href="'.SITENAME.'/index.php?page=forgotpass&key='.$key.'">ce lien</a>. pour renitialiser votre mot de passe<br>
					Si vous n\'avez pas demander la renitialisation de votre mot de passe ignorez juste ce message.
				</p>
			 </body>
			</html>
			 ';
	//A voir pour mettre une image

		$to = $email;
		$from = GMAIL;
		$from_name = GUSER;

		if(!$this->sendMail($to, $from, $from_name, $subject, $textMsg)){
			return false;
		}

		return true;

	}

	function sendMailAllUser($email,$header,$body){
		$subject = $header;
		$textMsg = $body;
			 $to = $email;
			 $from = GMAIL;
			 $from_name = GUSER;


			 if(!$this->sendMail($to, $from, $from_name, $subject, $textMsg)){
				 return false;
			 }


			 return true;


	}

	function sendListShared($pseudo,$email){


		// on récupère le contenu des variables soit elle est récupéré soit elle affiche un espace (cad rien)

		$subject = $pseudo." à partagé sa liste avec vous"; //Titre global du mail
		$textMsg = '
		<html>
			<body style="text-align:justify;">
				<h1 style="color:#010620; text-decoration:underline"><center><b>Plantes mellifères</b></center></h1>
					'.$pseudo.' vient de partager sa liste avec vous.<br/>
					Vous pouvez consulter sa liste via la section <i>Listes partagées</i> de l\'onglet Compte ou via <a href="'.SITENAME.'/index.php?page=Listes&user='.$pseudo.'">ce lien</a>.<br>
				</p>
			 </body>
			</html>
			 ';
	//A voir pour mettre une image

		$to = $email;
		$from = GMAIL;
		$from_name = GUSER;

		if(!$this->sendMail($to, $from, $from_name, $subject, $textMsg)){
			return false;
		}


		return true;

	}


/*
	function sendCustomMail($liste,$pseudo,$key){


		// on récupère le contenu des variables soit elle est récupéré soit elle affiche un espace (cad rien)

		$subject = "Validation de l'Email"; //Titre global du mail
		$textMsg = '
		<html>
			<body style="text-align:justify;">
				<h1 style="color:#010620; text-decoration:underline"><center><b>Plantes mellifères</b></center></h1>
				<p><b>Bonjour </b>'.$pseudo.' !<br>
					Veuillez verifier votre compte en cliquant sur <a href="'.SITENAME.'/index.php?page=Verify&key='.$key.'">ce lien</a>.<br>
					Si ce n\'est pas vous qui vous etes inscrit sur ce site ignorez juste ce message.
				</p>
			 </body>
			</html>
			 ';
	//A voir pour mettre une image
		$listeStr = "";
		foreach ($liste as $key => $value) {
			$listeStr = $listeStr.$value.",";
		}

		if($listeStr != ""){
			$listeStr = substr($listeStr,0,-1);
		}

		$to = $listeStr;
		$from = GMAIL;
		$from_name = GUSER;

		if(!$this->sendMail($to, $from, $from_name, $subject, $textMsg)){
			echo "<pre>err:".$this->error."</pre>";
			return false;
		}


		return true;

	}

*/


	  function sendMail($to, $from, $from_name, $subject, $textMsg) {
	  	$mail = new PHPMailer();  // create a new object
	  	$mail->IsSMTP(); // enable SMTP
	    $mail->isHTML(true);
	  	$mail->SMTPDebug = 0;  // debugging: 1 = errors and messages, 2 = messages only
	  	$mail->SMTPAuth = true;  // authentication enabled
	  	$mail->SMTPSecure = 'tls'; // secure transfer enabled REQUIRED for GMail
	  	$mail->Host = 'smtp.gmail.com';
	  	$mail->Port = 587;
	  	$mail->Username = GMAIL;
	  	$mail->Password = GPWD;
	    $mail->msgHTML($textMsg);
	  	$mail->SetFrom($from, $from_name);
	  	$mail->Subject = $subject;
	  	$mail->Body = $textMsg;
	  	$mail->AddAddress($to);
			//Si pas de ssl
			$mail->SMTPOptions = array(
    'ssl' => array(
        'verify_peer' => false,
        'verify_peer_name' => false,
        'allow_self_signed' => true
    )
);

	  	if(!$mail->Send()) {
	  		$this->error = 'Mail error: '.$mail->ErrorInfo;
	  		return false;
	  	} else {
	  		$this->error = 'Message sent!';
	  		return true;
	  	}
	  }



}
?>
