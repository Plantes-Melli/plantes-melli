$(document).ready(function(){
	$(function(){


		$("#champPlante").autocomplete(
			{
				source: (requete,reponse) => {
					$.ajax({
						url: "api/plantes.php",
						dataType: "json",
						data : {
							plante: 	$("#champPlante").val(),
							maxRows : 10,
							lang: 	$("input:checked").val()
						},
						success: (data) => {
							if($("input:checked").val() == "fr" )
								reponse(Array.from (new Set(data.map(element => element.nomFr)) ));
							else
								reponse(Array.from (new Set(data.map(element => element.nomLat)) ));
						},
					})
				}
			}
		)
	})
});
