<?php
require_once PATH_MODELE."/DAO.php";
require_once PATH_VUE."/vue.php";
require_once "controleurNavigation.php";
require_once "sendMail.php";
require_once PATH_CONTROLEUR."/regexUtils.php";

/*https://www.grafikart.fr/tutoriels/php/internationaliser-site-gettext-104*/
//Pour internationaliser site
class ControleurInscription{
	private $modele;
	private $vue;
	private $sendMail;
	private $error;


	public function __construct(){
		$this->modele = new Modele();
		$this->vue= new Vue();
		$this->sendMail = new Mailer();
	}

	public function route(){
		$this->error ="";
		$verif = $this->checkForm();

		if($verif == "success"){
				if(	$this->inscription()){
						$this->vue->afficherMessage("Inscription reussie","Votre inscription est terminée.<br/> Un email va vous être envoyé pour valider votre compte"); //TODO: afficher une vue de fin d'inscription
				}else{
						$this->vue->afficherMessage("Erreur serveur","L'email n'a pas pu être envoyé. Conctactez un administrateur via notre page de <a class='link' href=\"index.php?page=Contact\">contact</a>");
				}

		}else if($verif == "error"){
			$this->vue->afficherPageInscription($this->error);
		}else{
			$this->vue->afficherPageInscription();
		}

	}


	public function inscription(){
			$key = uniqid('',true);
			if($this->sendMail->verifEmail($_POST["Email"],$_POST["pseudoIns"],$key)){
				$this->modele->inscription($_POST["pseudoIns"],$_POST["prenom"],$_POST["nom"],$_POST["Email"],$_POST["mdpIns"]);
				$this->modele->addVerifLink($_POST["pseudoIns"],$key);
			}else{
				return false;
			}

			return true;
	}

	public function checkForm(){

		//Tous les champs sont remplis


		if(isset($_POST["pseudoIns"]) &&
		isset($_POST["prenom"]) &&
		isset($_POST["nom"]) &&
		isset($_POST["Email"]) &&
		isset($_POST["mdpIns"]) &&
		isset($_POST["mdpInsConf"]) ){



			// Ma clé privée
			$secret = CAPTCHA_PRIVATE;
			// Paramètre renvoyé par le recaptcha
			$response = $_POST['g-recaptcha-response'];
			// On récupère l'IP de l'utilisateur
			$remoteip = $_SERVER['REMOTE_ADDR'];

			$api_url = "https://www.google.com/recaptcha/api/siteverify?secret="
			. $secret
			. "&response=" . $response
			. "&remoteip=" . $remoteip ;

			$decode = json_decode(file_get_contents($api_url), true);

			if ($decode['success'] == false) {
				$this->error = "Merci de valider le captcha";
				return "error";
			}


			//Gestion erreur
			if(!isset($_POST["condition"])){
				$this->error = "Vous devez accepter nos conditions d'utilisation pour pouvoir vous inscrire";
				return "error";
			}

			$ndc = htmlspecialchars($_POST["pseudoIns"]);
			$prenom =   htmlspecialchars($_POST["prenom"]);
			$nom =   htmlspecialchars($_POST["nom"]);
			$mail =  htmlspecialchars($_POST["Email"]);
			$mdp =   htmlspecialchars($_POST["mdpIns"]);
			$cmdp =  htmlspecialchars($_POST["mdpInsConf"]);

			if(empty($ndc) ||empty($mail) || empty($mdp) || empty($cmdp) || empty($prenom) || empty($nom)){
				$this->error ="Merci de remplir tous les champs";
				return "error";
			}
			//Gestion d'un pseudo déjà existant en retournant une erreur
			if($this->modele->existsPseudo($_POST["pseudoIns"])){
				$this->error = "Le pseudo existe déjà";
				return "error";
			}

			if($this->modele->existsEmail($_POST["Email"])){
				$this->error = "Cette email est déjà utilisée";
				return "error";
			}


			if($mdp != $cmdp){
				$this->error = "Les mots de passes doivent être identiques";
				return "error";
			}

			if(RegexUtils::checkPassword($mdp) == false){
				$this->error ="Le mot de passe comporte des caracteres autres que a-z A-Z 0-9 () [] {} \ . ? + , ; / : ! - * ou est d'une taille non comprise entre 0 et 20";
				return "error";
			}

			if(RegexUtils::checkNom($prenom) == false){
				$this->error = " Le prenom comporte des caractères interdits ou est de taille invalide ( 2 - 20 caractères )";
				return "error";
			}

			if(RegexUtils::checkNom($nom) == false){
				$this->error = " Le nom comporte des caractères interdits ou est de taille invalide ( 2 - 20 caractères )";
				return "error";
			}

			if(RegexUtils::checkPseudo($ndc) == false){
				$this->error = " Le nom de compte comporte des caractères interdits";
				return "error";
			}


			if(RegexUtils::checkMail($mail) == false){
				$this->error = "l'email n'est pas valide ou le fournisseur à été bannit";
				return "error";
			}

			return "success";


			//Un des champs est remplis mais pas tous
		}else if(isset($_POST["pseudoIns"]) ||
		isset($_POST["Email"]) ||
		isset($_POST["mdp1"]) ||
		isset($_POST["prenom"]) ||
		isset($_POST["nom"]) ||
		isset($_POST["mdp2"])) {

			foreach($_POST as $key => $value){
				if(empty($value)){
					$this->error = "Un ou plusieurs champs sont vides";
					return "emptyfield";
				}
			}



			//Tous les champs sont vide
		}else{
			return NULL;
		}

	}

}
?>
