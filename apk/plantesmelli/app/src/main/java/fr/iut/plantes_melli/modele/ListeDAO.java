package fr.iut.plantes_melli.modele;


import java.util.List;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Entity;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;

@Dao
public interface ListeDAO {

    @Query("SELECT * FROM listePlantes")
    List<PlanteId> getAll();

    @Query("SELECT * FROM listePlantes where id = :id")
    PlanteId getPlanteId(int id);

    @Insert
    void insertPlanteId(PlanteId p);

    @Delete
    void removePlanteId(PlanteId p);

}
