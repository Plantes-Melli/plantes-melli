function hide(event){
	var parent = this.parentElement;
	var childs = Array.from(parent.childNodes);
	console.log(childs);
	if (this.className === "visible"){
		this.className = "invisible"
		childs.forEach(function(element){
			if(typeof element.style !== "undefined" && element.tagName !== "LEGEND" && element.tagName !== "SCRIPT" )
				element.style.display = "none";
		});

	}else{
		this.className = "visible"
		childs.forEach(function(element){
			if(typeof element.style !== "undefined" && element.tagName !== "LEGEND" && element.tagName !== "SCRIPT")
				element.style.display = "inline-block";
		});
	}
}

(function(){
	var array = sidePan.getElementsByTagName("legend");
	Array.from(array).forEach(function(element){
		element.addEventListener("click",hide,false);
	});
})()


function checkAll(){
	var array = document.getElementsByClassName("miniatures")[0];
	var checkboxes = Array.from(array.getElementsByTagName("input"));
	checkboxes.forEach(function(element){
		if(element.type == "checkbox")
			element.checked = true;
	});
}

function reverse(){
	var array = document.getElementsByClassName("miniatures")[0];
	var checkboxes = Array.from(array.getElementsByTagName("input"));
	checkboxes.forEach(function(element){
		if(element.type == "checkbox")
			if (element.checked){
				element.checked = false
			}else{
				element.checked = true
			}
	});
}

function confirmAction(){

	var index = 0;


	var array = document.getElementsByClassName("miniatures")[0];
	var checkboxes = Array.from(array.getElementsByTagName("input"));
	checkboxes.forEach(function(element){
		if(element.type == "checkbox" && element.checked)
			index++;
	});
		var list = document.getElementById("action");
		var selected = list.options[list.selectedIndex].value;
		var string = "Etes vous sûr de vouloir "+selected+" ces "+ index+" éléments?";
		if(index > 0)
			return confirm(string);
		else {
			return false;
		}
}
