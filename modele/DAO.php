<?php

require_once PATH_MODELE."/Compte.php";

header("Content-Type: text/html; charset=utf-8");

// Classe generale de definition d'exception
class MonException extends Exception{
	private $chaine;
	public function __construct($chaine){
		$this->chaine=$chaine;
	}

	public function afficher(){
		return $this->chaine;
	}

}


// Exception relative à un probleme de connexion
class ConnexionException extends MonException{
}

// Exception relative à un probleme d'accès à une table
class TableAccesException extends MonException{
}


// Classe qui gère les accès à la base de données

class Modele{
	private $connexion;
	private $compte;

	// Constructeur de la classe
	// remplacer X par les informations qui vous concernent

	public function __construct(){
		try{
			$chaine="mysql:host=".HOST.";dbname=".BD;
			$this->connexion = new PDO($chaine,LOGIN,PASSWORD);
			$this->connexion->setAttribute(PDO::ATTR_ERRMODE,PDO::ERRMODE_EXCEPTION);
		}
		catch(PDOException $e){
			$exception=new ConnexionException("problème de connection à la base");
			throw $exception;
		}
		$this->connexion->query("SET NAMES UTF8");//premet de ne pas avoir des ? à la place des accents
	}



	// A développer
	// méthode qui permet de se deconnecter de la base
	public function deconnexion(){
		$this->connexion=null;
	}


	public function getAccountByPseudo($pseudo){
		try{
			$statement = $this->connexion->prepare("SELECT id,pseudo,mail,prenom,nom,privileges FROM utilisateurs  WHERE pseudo=?");
			$statement->bindParam(1,$pseudo);
			$statement->execute();
			$statement->setFetchMode(PDO::FETCH_CLASS,'Compte');
			return $statement->fetch();

		}catch(PDOException $e){
			$this->deconnexion();
			throw new TableAccesException("Problème avec la table utiliateurs");
		}
	}

	public function getEmailByPseudo($pseudo){
		try{
			$statement = $this->connexion->prepare("SELECT mail FROM utilisateurs  WHERE pseudo=?");
			$statement->bindParam(1,$pseudo);
			$statement->execute();
			return $statement->fetch()["mail"];

		}catch(PDOException $e){
			$this->deconnexion();
			throw new TableAccesException("Problème avec la table utiliateurs");
		}
	}

public function getEmail(){
	try{
	$requete = "SELECT mail FROM utilisateurs;";
	$statement = $this->connexion->query($requete);
	return $statement->fetchAll(PDO::FETCH_ASSOC);
}
	catch(PDOException $e){
		$this->deconnexion();
		throw new TableAccesException("problème avec la table utilisateurs dans les mails");
	}
}

public function getNumberOfMail(){
	try{
		$requete = "SELECT count(mail) FROM utilisateurs;";
		$statement = $this->connexion->query($requete);
		return $statement->fetch()[0];
	}catch(PDOException $e){
		$this->deconnexion();
		throw new TableAccesException("problème avec la table utilisateurs");
	}
}

	public function getIdByEmail($email){
		try{
			$statement = $this->connexion->prepare("SELECT id FROM utilisateurs  WHERE mail=?");
			$statement->bindParam(1,$email);
			$statement->execute();
			return $statement->fetch(PDO::FETCH_ASSOC)["id"];

		}catch(PDOException $e){
			$this->deconnexion();
			throw new TableAccesException("Problème avec la table utiliateurs");
		}
	}

	public function inscription($pseudo,$prenom,$nom,$mail,$mdp){
		try{
			//TODO: pas de salt
			$mdpCrypt =password_hash($mdp,PASSWORD_DEFAULT);
			$statement = $this->connexion->prepare("INSERT INTO utilisateurs(pseudo, prenom, nom, tempemail, motDePasse) VALUES (:pseudo, :prenom, :nom, :mail, :motDePasse)");
			$statement->bindParam(1, $pseudo);
			$statement->bindParam(2, $prenom);
			$statement->bindParam(3, $nom);
			$statement->bindParam(4, $mail);
			$statement->bindParam(5, $mdpCrypt);

			$statement->execute(array("pseudo" => $pseudo, "prenom"=> $prenom, "nom"=> $nom, "mail"=>$mail,"motDePasse" => $mdpCrypt));
		}
		catch(PDOException $e){
			$this->deconnexion();
			throw new TableAccesException("Problème avec la table utiliateurs");
		}

	}

	public function existsPseudo($pseudo){
		try{
			$stmt= $this->connexion->prepare("SELECT * FROM utilisateurs WHERE pseudo=?");
			$stmt->bindParam(1, $pseudo);
			$stmt->execute();
			$result=$stmt->fetch(PDO::FETCH_ASSOC);

			if(!empty($result))
				return true;
			else
				return false;
		}catch(PDOException $e){
			$this->deconnexion();
			throw new TableAccesException("Problème avec la table utiliateurs");
		}


	}

	public function existsEmail($email){
		try{
			$stmt= $this->connexion->prepare("SELECT * FROM utilisateurs WHERE mail=?");
			$stmt->bindParam(1, $email);
			$stmt->execute();
			$result=$stmt->fetch(PDO::FETCH_ASSOC);

			if(!empty($result))
				return true;
			else
				return false;
		}catch(PDOException $e){
			$this->deconnexion();
			throw new TableAccesException("Problème avec la table utiliateurs");
		}


	}

	public function identifiantsValides($pseudo, $mdp){
		try{


			$statement = $this->connexion->prepare("SELECT motDePasse FROM `utilisateurs` WHERE pseudo=? ");
			$pseudoParam=$pseudo;
			$statement->bindParam(1, $pseudoParam);
			$statement->execute();
			$result=$statement->fetch(PDO::FETCH_ASSOC);
			if(password_verify($mdp, $result["motDePasse"])){
				return true;
			}
			else {
				return false;
			}
		}
		catch(PDOException $e){
			$this->deconnexion();
			throw new TableAccesException("problème avec la plant_melli_apk");
		}
	}

	function getListPlants($page,$numplantes,$filters = NULL){
		try{
			$filtersRqt = "where ";
			foreach ($filters as $key => $value) {
				if($key != "tri" && $key != "triV" && $value != "tous")
					if($key == "debut_floraison"){
							$filtersRqt = $filtersRqt.$key.">=? and ";
					}elseif ($key == "fin_floraison") {
							$filtersRqt = $filtersRqt.$key."<=? and ";
					}else{
								$filtersRqt = $filtersRqt.$key."=? and ";
					}
			}


			if(strlen($filtersRqt) > 6)
				$filtersRqt = substr($filtersRqt,0,-4); // on enleve le dernier 'and '
			else {
				$filtersRqt =""; // on enleve where
			}

			$order = "order by ";
			if(isset($filters["tri"])){
				$orderType = $filters["tri"];

				//pas de moyen d'echapper l'ordre avec bindParam,value,... => soluce echapper a la main
				if($orderType != "nomFr" &&
				 	 $orderType != "nomLat" &&
					 $orderType != "type" &&
					 $orderType != "famille" &&
					 $orderType != "moyenne_kgHa" &&
					 $orderType != "inter_apicole" &&
					 $orderType != "nectarifere" &&
					 $orderType != "pollinifere" &&
					 $orderType != "debut_floraison" &&
					 $orderType != "fin_floraison"
				 ){
					 $orderType = "nomFr";
				 }

				 $order = $order." ".$orderType." ";
				if($filters["triV"] == "asc")
					$order = $order."asc";
				else
					$order = $order."desc";
			}else{
					$order = $order."nomFr asc";
			}

			$requete = "SELECT * FROM plant_melli_apk ".$filtersRqt." ".$order." limit ".intval($page*$numplantes).",".$numplantes.";";


			$statement = $this->connexion->prepare($requete);
			$index = 1;
			foreach ($filters as $key => $value) {
				if($key != "tri" && $key != "triV" && $value != "tous"){
						$statement->bindParam($index,$filters[$key]);
						$index++;
				}
			}
			$statement->execute();
			$result = $statement->fetchAll();
			return $result;
		}catch(PDOException $e){
			$this->deconnexion();
			throw new TableAccesException("problème avec la plant_melli_apk");
		}
	}
	function nbResult($filters){
		try{
			$filtersRqt = "where ";
			foreach ($filters as $key => $value) {
				if($key != "tri" && $key != "triV" && $value != "tous")
					if($key == "debut_floraison"){
							$filtersRqt = $filtersRqt.$key.">=? and ";
					}elseif ($key == "fin_floraison") {
							$filtersRqt = $filtersRqt.$key."<=? and ";
					}else{
								$filtersRqt = $filtersRqt.$key."=? and ";
					}
			}


			if(strlen($filtersRqt) > 6)
				$filtersRqt = substr($filtersRqt,0,-4); // on enleve le dernier 'and '
			else {
				$filtersRqt =""; // on enleve where
			}
			$requete = "SELECT count(*) FROM plant_melli_apk ".$filtersRqt;


			$statement = $this->connexion->prepare($requete);
			$index = 1;
			foreach ($filters as $key => $value) {
				if($key != "tri" && $key != "triV" && $value != "tous"){
						$statement->bindParam($index,$filters[$key]);
						$index++;
				}
			}
			$statement->execute();
			$result = $statement->fetch()[0];
			return $result;
		}catch(PDOException $e){
			$this->deconnexion();
			throw new TableAccesException("problème avec la plant_melli_apk");
		}
	}


	function getNbPlants(){
		try{
			$requete = "SELECT count(*) FROM plant_melli_apk;";
			$statement = $this->connexion->query($requete);
			return $statement->fetch()[0];
		}catch(PDOException $e){
			$this->deconnexion();
			throw new TableAccesException("problème avec la plant_melli_apk");
		}
	}

	public function levenshtein($input,$lang){
		try{
			$requete= "select ".$lang." from plant_melli_apk;";
			$statement=$this->connexion->prepare($requete);
			$statement->execute();

			// tableau de mots à vérifier
			$words = $statement->fetchAll(PDO::FETCH_ASSOC);



			// aucune distance de trouvée pour le moment
			$shortest = -1;

			// boucle sur les des mots pour trouver le plus près
			foreach ($words as $word) {
				$word = $word[$lang];
				// calcule la distance avec le mot mis en entrée,
				// et le mot courant
				$lev = levenshtein($input, $word);

				// cherche une correspondance exacte
				if ($lev == 0) {

					// le mot le plus près est celui-ci (correspondance exacte)
					$closest = $word;
					$shortest = 0;

					// on sort de la boucle ; nous avons trouvé une correspondance exacte
					break;
				}

				// Si la distance est plus petite que la prochaine distance trouvée
				// OU, si le prochain mot le plus près n'a pas encore été trouvé
				if ($lev <= $shortest || $shortest < 0) {
					// définition du mot le plus près ainsi que la distance
					$closest  = $word;
					$shortest = $lev;
				}

			}


			return $closest;


		}


		catch(PDOException $e){
			$exception=new TableAccesException($e);
			echo $e->getMessage();
			return $exception;
		}
	}

	function getPlanteByName($nomPlante,$pseudo,$langue = "fr" ,$approximateName = false){

		if($langue == "fr"){
			$langue = "nomFr";
		}else{
			$langue = "nomLat";
		}


		if($approximateName){
			$nomPlante = $this->levenshtein($nomPlante,$langue);
		}
		try{
			//	$requete = "SELECT * FROM plant_melli_apk p,plant_melli_apk_CARACTERISTIQUES c where p.nomLat = c.nomLat and ?=?";
			$requete = "SELECT * FROM plant_melli_apk where ".$langue."=?;"; //pas beau mais bindParam accepte pas $langue

			$statement = $this->connexion->prepare($requete);
			$statement->bindParam(1,$nomPlante);
			$statement->execute();

			$array =  $statement->fetch(PDO::FETCH_ASSOC);

			if($pseudo != NULL){
				$requete = "SELECT * FROM plantes_locales where pseudo = ? and id = ?;";
				$statement = $this->connexion->prepare($requete);
				$statement->bindParam(1,$pseudo);
				$statement->bindParam(2,$array["id"]);
				$statement->execute();
				$array2 = $statement->fetchAll(PDO::FETCH_ASSOC);
				if(sizeof($array2) != 0)
					foreach ($array2[0] as $key => $value) {
						if($key != "pseudo" && $key != "nomLat"){
							$array[$key] = $array2[0][$key];
						}
					}

			}


			return $array;

		}catch(PDOException $e){
			$exception=new TableAccesException($e);
			echo $e->getMessage();
			return $exception;
		}



		return null;

	}



		function getPlanteById($id,$pseudo){

			try{
				//	$requete = "SELECT * FROM plant_melli_apk p,plant_melli_apk_CARACTERISTIQUES c where p.nomLat = c.nomLat and ?=?";
				$requete = "SELECT * FROM plant_melli_apk where id=?"; //pas beau mais bindParam accepte pas $langue

				$statement = $this->connexion->prepare($requete);
				$statement->bindParam(1,$id);
				$statement->execute();

				$array =  $statement->fetch(PDO::FETCH_ASSOC);

				if($pseudo != NULL){
					$requete = "SELECT * FROM plantes_locales where pseudo = ? and id = ?;";
					$statement = $this->connexion->prepare($requete);
					$statement->bindParam(1,$pseudo);
					$statement->bindParam(2,$id);
					$statement->execute();
					$array2 = $statement->fetchAll(PDO::FETCH_ASSOC);

					if(sizeof($array2) != 0)
						foreach ($array2[0] as $key => $value) {
							if($key != "pseudo"){
								$array[$key] = $array2[0][$key];
							}
						}

				}


				return $array;

			}catch(PDOException $e){
				$exception=new TableAccesException($e);
				echo $e->getMessage();
				return $exception;
			}



			return null;

		}

	// SELECT DISTINCT `Moyenne_kgHa` FROM `plant_melli_apk` WHERE `Moyenne_kgHa` order by `Moyenne_kgHa` asc;

	function getAllFilters(){
		try{

			$array = array("type","famille","inter_apicole","moyenne_kgHa","nectarifere","pollinifere","debut_floraison","fin_floraison");
			$resultat;

			foreach($array as $filtre){
				$requete = "SELECT DISTINCT ".$filtre." FROM `plant_melli_apk` order by ".$filtre." asc"; //pas beau mais bindParam accepte pas $langue
				$statement = $this->connexion->query($requete);
				$resultat[$filtre] = $statement->fetchAll(PDO::FETCH_ASSOC);
			}

			return $resultat;

		}catch(PDOException $e){
			$exception=new TableAccesException($e);
			echo $e->getMessage();
			return $exception;
		}
	}

	function getAllPlants(){
		try{
				$requete = "SELECT * FROM `plant_melli_apk`"; //pas beau mais bindParam accepte pas $langue
				$statement = $this->connexion->query($requete);
				return $statement->fetchAll(PDO::FETCH_ASSOC);
		}catch(PDOException $e){
			$this->deconnexion();
			throw new TableAccesException("problème avec la table plant_melli_apk");
		}
	}


	public function removePlants($array){
		try{
				$requete = "delete from plant_melli_apk where ";
				foreach ($array as $key => $value) {
					$requete = $requete." id = ? or ";
				}
				$requete = substr($requete,0,-3);
				$requete = $requete.";";

					$statement = $this->connexion->prepare($requete);
					$index = 1;
					foreach ($array as $key => $value) {
						$statement->bindValue($index,$array[$key]);
						$index++;
					}

					$statement->execute();

		}catch(PDOException $e){
			$this->deconnexion();
			print($e);
			throw new TableAccesException("problème avec la table plant_melli_apk");
		}
	}


	public function getImages($array){
		try{
				$requete = "select image from plant_melli_apk where ";
				foreach ($array as $key => $value) {
					$requete = $requete." id = ? or ";
				}
				$requete = substr($requete,0,-3);
				$requete = $requete.";";

					$statement = $this->connexion->prepare($requete);
					$index = 1;
					foreach ($array as $key => $value) {
						$statement->bindValue($index,$array[$key]);
						$index++;
					}

					$statement->execute();
					return $statement->fetchAll(PDO::FETCH_ASSOC);

		}catch(PDOException $e){
			$this->deconnexion();
			print($e);
			throw new TableAccesException("problème avec la table plant_melli_apk");
		}
	}

	public function get3PlantesSaison() {
		try{
			$ecart = 1;
			do {
				$statement=$this->connexion->query("SELECT * FROM `plant_melli_apk` WHERE (debut_floraison-$ecart<MONTH(NOW()) and
				fin_floraison+$ecart>MONTH(NOW())
			)");
			$result=$statement->fetchAll();
			$ecart++;
		} while (sizeof($result)<3);
		shuffle($result);
		return array_slice($result,0,3);
	}
	catch(PDOException $e){
		$this->deconnexion();
		throw new TableAccesException("problème avec la table plant_melli_apk");
	}
}

public function getAllUsers(){
	try{
		$requete ="SELECT  pseudo,privileges FROM utilisateurs";
		$statement = $this->connexion->query($requete);
		return $statement->fetchAll(PDO::FETCH_ASSOC);
	}catch(PDOException $e){
		$this->deconnexion();
		throw new TableAccesException("problème avec la table users");

	}
}

public function changePrivileges($pseudo,$privilege){
	try{
		$stmt= $this->connexion->prepare("UPDATE utilisateurs set privileges = ? WHERE pseudo=?;");
		$stmt->bindParam(1, $privilege);
		$stmt->bindParam(2,$pseudo);
		$stmt->execute();
		return $stmt->rowCount() > 0;
	}catch(PDOException $e){
		$this->deconnexion();
		throw new TableAccesException("problème avec la table users");

	}
}

public function banUser($pseudo,$bool){
	try{
		$stmt= $this->connexion->prepare("UPDATE utilisateurs set ban = ? WHERE pseudo=?;");
		$stmt->bindParam(1, $bool);
		$stmt->bindParam(2,$pseudo);
		$stmt->execute();
		return $stmt->rowCount() > 0;
	}catch(PDOException $e){
		$this->deconnexion();
		throw new TableAccesException("problème avec la table users");

	}
}

public function isBanned($pseudo){
	try{
		$stmt= $this->connexion->prepare("SELECT ban FROM utilisateurs WHERE pseudo=?;");
		$stmt->bindParam(1,$pseudo);
		$stmt->execute();
		return $stmt->fetch()["ban"] == 1;
	}catch(PDOException $e){
		$this->deconnexion();
		throw new TableAccesException("problème avec la table users");
	}
}

	public function addToList($pseudo,$plante){
		try{
			$stmt= $this->connexion->prepare("INSERT INTO listeadherent values (?,?);");
			$stmt->bindParam(1,$pseudo);
			$stmt->bindParam(2,$plante);
			$stmt->execute();
		}catch(PDOException $e){
			$this->deconnexion();
			throw new TableAccesException("problème avec la table listeadherent");
		}
	}

	public function removeFromList($pseudo,$plante){
		try{
			$stmt= $this->connexion->prepare("DELETE FROM listeadherent where pseudo=? and idPlante=?");
			$stmt->bindParam(1,$pseudo);
			$stmt->bindParam(2,$plante);
			$stmt->execute();
		}catch(PDOException $e){
			$this->deconnexion();
			throw new TableAccesException("problème avec la table listeadherent");
		}
	}

	public function getListUser($pseudo){
		try{
			$stmt= $this->connexion->prepare("SELECT id,nomFr,nomLat,type,image,famille,inter_apicole,moyenne_kgHa,nectarifere,pollinifere,debut_floraison,fin_floraison
																				from plant_melli_apk ,listeadherent
																				where pseudo = ? and
																							id = idPlante");
			$stmt->bindParam(1,$pseudo);
			$stmt->execute();
			return $stmt->fetchAll(PDO::FETCH_ASSOC);
		}catch(PDOException $e){
			$this->deconnexion();
			throw new TableAccesException("problème avec la table listeadherent");
		}
	}




public function addPlant($type,$nomPlantFr,$nomPlantLat,$famille,$image,$moyenne_kgHa,$apicole,$exposition,$nectarifere,$pollinifere,$debut_floraison,$fin_floraison,$description){
	try{
		$stmt= $this->connexion->prepare("insert into plant_melli_apk (type,nomFr,nomLat,famille,image,moyenne_kgHa,inter_apicole,exposition,nectarifere,pollinifere,debut_floraison,fin_floraison,description,hash)
																				values (?,?,?,?,?,?,?,?,?,?,?,?,?,?);");
		$hash = $this->computeHash($type,$nomPlantFr,$nomPlantLat,$famille,$image,$moyenne_kgHa,$apicole,$exposition,$nectarifere,$pollinifere,$debut_floraison,$fin_floraison,$description);
		$stmt->bindParam(1,$type);
		$stmt->bindParam(2,$nomPlantFr);
		$stmt->bindParam(3,$nomPlantLat);
		$stmt->bindParam(4,$famille);
		$stmt->bindParam(5,$image);
		$stmt->bindParam(6,$moyenne_kgHa);
		$stmt->bindParam(7,$apicole);
		$stmt->bindParam(8,$exposition);
		$stmt->bindParam(9,$nectarifere);
		$stmt->bindParam(10,$pollinifere);
		$stmt->bindParam(11,$debut_floraison);
		$stmt->bindParam(12,$fin_floraison);
		$stmt->bindParam(13,$description);
		$stmt->bindParam(14,$hash);
		$stmt->execute();
	}catch(PDOException $e){
		$this->deconnexion();
		throw new TableAccesException("problème avec la table plant_melli_apk");
	}
}

public function savePlantLocally($pseudo,$id,$apicole,$pollinifere,$nectarifere,$moyenne_kgHa,$exposition,$debut_floraison,$fin_floraison,$description){
	try{
		$stmt= $this->connexion->prepare("select * from plantes_locales where pseudo = ? and id = ?");
		$stmt->bindParam(1,$pseudo);
		$stmt->bindParam(2,$id);
		$stmt->execute();
		$array = $stmt->fetchAll(PDO::FETCH_ASSOC);


		if(sizeof($array) == 0){
				$stmt= $this->connexion->prepare("insert into plantes_locales
																																			values (:pseudo,
																																							:id,
																																						  :moyenne_kgHa,
																																						  :inter_apicole,
																																						  :exposition,
																																						  :nectarifere,
																																						  :pollinifere,
																																						  :debut_floraison,
																																					  	:fin_floraison,
																																						  :description);");
		}else{
				$stmt= $this->connexion->prepare("update plantes_locales set moyenne_kgHa=:moyenne_kgHa,
																																		 inter_apicole=:inter_apicole ,
																																		 exposition=:exposition ,
																																		 nectarifere=:nectarifere ,
																																		 pollinifere=:pollinifere ,
																																		 debut_floraison=:debut_floraison ,
																																		 fin_floraison=:fin_floraison ,
																																		 description=:description
																								  								 	 where id = :id and pseudo = :pseudo");
		}

		$stmt->bindParam(":pseudo",$pseudo);
		$stmt->bindParam(":id",$id);
		$stmt->bindParam(":moyenne_kgHa",$moyenne_kgHa);
		$stmt->bindParam(":inter_apicole",$apicole);
		$stmt->bindParam(":exposition",$exposition);
		$stmt->bindParam(":nectarifere",$nectarifere);
		$stmt->bindParam(":pollinifere",$pollinifere);
		$stmt->bindParam(":debut_floraison",$debut_floraison);
		$stmt->bindParam(":fin_floraison",$fin_floraison);
		$stmt->bindParam(":description",$description);
		$stmt->execute();
	}catch(PDOException $e){
		$this->deconnexion();
		throw new TableAccesException("problème avec la table plantes_locales");
	}
}


public function removePlantLocally($pseudo,$id){
	try{
		$stmt= $this->connexion->prepare("delete from plantes_locales where pseudo = ? and id = ?");
		$stmt->bindParam(1,$pseudo);
		$stmt->bindParam(2,$id);
		$stmt->execute();
	}catch(PDOException $e){
		$this->deconnexion();
		throw new TableAccesException("problème avec la table plantes_locales");
	}
}


public function modifyPlant($type,$id,$nomPlantFr,$nomPlantLat,$famille,$image,$moyenne_kgHa,$apicole,$exposition,$nectarifere,$pollinifere,$debut_floraison,$fin_floraison,$description){
		try{
			$stmt= $this->connexion->prepare("update plant_melli_apk set type=?,
																																	 nomFr=?,
																																	 nomLat=?,
																																	 famille=?,
																																	 image=?,
																																	 moyenne_kgHa=?,
																																	 inter_apicole=?,
																																	 exposition=?,
																																	 nectarifere=?,
																																	 pollinifere=?,
																																	 debut_floraison=?,
																																	 fin_floraison=?,
																																	 description=?,
																																	 hash=?
																									where id=?;");

      $hash = $this->computeHash($type,$nomPlantFr,$nomPlantLat,$famille,$image,$moyenne_kgHa,$apicole,$exposition,$nectarifere,$pollinifere,$debut_floraison,$fin_floraison,$description);
			$stmt->bindParam(1,$type);
			$stmt->bindParam(2,$nomPlantFr);
			$stmt->bindParam(3,$nomPlantLat);
			$stmt->bindParam(4,$famille);
			$stmt->bindParam(5,$image);
			$stmt->bindParam(6,$moyenne_kgHa);
			$stmt->bindParam(7,$apicole);
			$stmt->bindParam(8,$exposition);
			$stmt->bindParam(9,$nectarifere);
			$stmt->bindParam(10,$pollinifere);
			$stmt->bindParam(11,$debut_floraison);
			$stmt->bindParam(12,$fin_floraison);
			$stmt->bindParam(13,$description);
			$stmt->bindParam(14,$hash);
			$stmt->bindParam(15,$id);
			$stmt->execute();
		}catch(PDOException $e){
			$this->deconnexion();
			throw new TableAccesException("problème avec la table plant_melli_apk");
		}

}

function computeHash($type,$nomPlantFr,$nomPlantLat,$famille,$image,$moyenne_kgHa,$apicole,$exposition,$nectarifere,$pollinifere,$debut_floraison,$fin_floraison,$description){
	return md5(json_encode(array(
		"type" => $type ,
		"nomPlantFr" => $nomPlantFr ,
		"nomPlantLat" => $nomPlantLat,
		"famille" => $famille,
		"image" => $image,
		"moyenne_kgHa" => $moyenne_kgHa,
		"apicole" => $apicole,
		"exposition" => $exposition,
		"nectarifere" => $nectarifere,
		"pollinifere" => $pollinifere,
		"debut_floraison" => $debut_floraison,
		"fin_floraison" => $fin_floraison,
		"description" => $description
	)));
}

function setHash($id,$hash){
	try{
		$stmt= $this->connexion->prepare("update plant_melli_apk set hash=?
																								where id=?;");
		$stmt->bindParam(1,$hash);
		$stmt->bindParam(2,$id);
		$stmt->execute();
	}catch(PDOException $e){
		$this->deconnexion();
		throw new TableAccesException("problème avec la table plant_melli_apk");
	}

}

//email verifié?
function isEmailVerified($pseudo){
	try{
		$statement = $this->connexion->prepare("SELECT verif FROM utilisateurs WHERE pseudo=?");
		$statement->bindParam(1,$pseudo);
		$statement->execute();
		$result = $statement->fetch(PDO::FETCH_BOTH);
		return $result["verif"];


	}catch(PDOException $e){
		$this->deconnexion();
		throw new TableAccesException("Problème avec la table accounts");
	}
}


//ajout de lien de verification pour le mail
function addVerifLink($ndc,$key){

	try{
		$statement = $this->connexion->prepare("INSERT INTO veriflink select id,? from utilisateurs where pseudo = ?;");
		$statement->bindParam(1,$key);
		$statement->bindParam(2,$ndc);

		return $statement->execute();

	}catch(PDOException $e){
		$this->deconnexion();
		throw new TableAccesException("Problème avec la table verif");
	}
}

//verifie le mail
function verifMail($key){
	try{
		$statement = $this->connexion->prepare("update utilisateurs set verif=1,mail=tempemail where id = (select id from veriflink where link=?)");
		$statement->bindParam(1,$key);
		$statement->execute();
		return $statement->rowCount();


	}catch(PDOException $e){
		$this->deconnexion();
		throw new TableAccesException("Problème avec la table verif");
	}
}

function deleteVerifKey($key){
	try{
		$statement = $this->connexion->prepare("DELETE FROM veriflink WHERE link=? ");
		$statement->bindParam(1,$key);
		$statement->execute();

	}catch(PDOException $e){
		$this->deconnexion();
		throw new TableAccesException("Problème avec la table verif");
	}
}


function existsForgotkey($key){
	try{
		$statement = $this->connexion->prepare("SELECT count(*) FROM forgotpass where link=?");
		$statement->bindParam(1,$key);
		$statement->execute();
		return $statement->fetch(PDO::FETCH_NUM)[0] != 0;
	}catch(PDOException $e){
		$this->deconnexion();
		throw new TableAccesException("Problème avec la table forgot");
	}
}
function deleteForgotKey($key){
	try{
		$statement = $this->connexion->prepare("DELETE FROM forgotpass WHERE link=? ");
		$statement->bindParam(1,$key);
		$statement->execute();

	}catch(PDOException $e){
		$this->deconnexion();
		throw new TableAccesException("Problème avec la table forgot");
	}
}


//ajout d'un lien de mail pour le changement de mdp
function getNewPass($ndc,$key){
	try{
		$statement = $this->connexion->prepare("INSERT INTO forgotpass select id,? from utilisateurs where id = ?;");
		$statement->bindParam(1,$key);
		$statement->bindParam(2,$ndc);

		return $statement->execute();

	}catch(PDOException $e){
		$this->deconnexion();
		throw new TableAccesException("Problème avec la table verif");
	}
}

//change le mdp
function changePassword($key,$newpass){
	try{
		$statement = $this->connexion->prepare("update utilisateurs set motDePasse=? where id = (select id from forgotpass where link=?);");
		$hash = password_hash($newpass,PASSWORD_DEFAULT);
		$statement->bindParam(1,$hash);
		$statement->bindParam(2,$key);
		$statement->execute();
		return $statement->rowCount();


	}catch(PDOException $e){
		$this->deconnexion();
		throw new TableAccesException("Problème avec la table verif");
	}
}
function updateAccount($id,$pseudo,$prenom,$nom,$email){
	try{
		$statement = $this->connexion->prepare("update utilisateurs set pseudo=?,
																																		prenom=?,
																																		nom=?,
																																		tempemail=?
		 																													where id = ?;");

		$statement->bindParam(1,$pseudo);
		$statement->bindParam(2,$prenom);
		$statement->bindParam(3,$nom);
		$statement->bindParam(4,$email);
		$statement->bindParam(5,$id);
		$statement->execute();

	}catch(PDOException $e){
		$this->deconnexion();
		throw new TableAccesException("Problème avec la table verif");
	}
}
public function getSharedLists($pseudo){
	try{
		$statement = $this->connexion->prepare("select DISTINCT src from listesPubliques where dst = ? or dst='*' ;");
		$statement->bindParam(1,$pseudo);
		$statement->execute();
		$array = array();
		return $statement->fetchAll(PDO::FETCH_ASSOC);


	}catch(PDOException $e){
		$this->deconnexion();
		throw new TableAccesException("Problème avec la table listesPubliques");
	}
}

public function shareList($src,$dst){
	try{


		if(!$this->canSeeList($dst,$src)){
			$statement = $this->connexion->prepare("INSERT INTO listespubliques values (?,?);");
			$statement->bindParam(1,$src);
			$statement->bindParam(2,$dst);
			$statement->execute();
			return $statement->rowCount();
		}else{
			return false;
		}



	}catch(PDOException $e){
		$this->deconnexion();
		throw new TableAccesException("Problème avec la table listesPubliques");
	}
}


function canSeeList($user,$target){
	try{
		$statement = $this->connexion->prepare("SELECT * FROM listespubliques where src=? and (dst=? or dst='*') ;");
		$statement->bindParam(1,$target);
		$statement->bindParam(2,$user);
		$statement->execute();
		$result = $statement->fetchAll(PDO::FETCH_ASSOC);
		if(sizeof($result) != 0){
			return true;
		}else{
			return false;
		}

	}catch(PDOException $e){
		$this->deconnexion();
		throw new TableAccesException("Problème avec la table listesPubliques");
	}
}






}
?>
