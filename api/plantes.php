<?php
require "../config/config.php";
header('Content-type: application/json');
header("Access-Control-Allow-Origin: *");

$connexion;

try{
	$chaine="mysql:host=".HOST.";dbname=".BD;
	$connexion = new PDO($chaine,LOGIN,PASSWORD);
	$connexion->setAttribute(PDO::ATTR_ERRMODE,PDO::ERRMODE_EXCEPTION);
}catch(PDOException $e){
	$exception=new ConnexionException("problème de connection à la base");
	throw $exception;
}
$connexion->query("SET NAMES UTF8");//premet de ne pas avoir des ? à la place des accents


$strQuery = "SELECT * FROM plant_melli_apk WHERE ";
if (isset($_GET["plante"]))
{
		//Nom Français
		$lang = isset($_GET["lang"]) ? $_GET["lang"] : "fr";
		if($lang == "fr")
    	$strQuery .= "nomFr LIKE :nom ";
		else
			$strQuery .= "nomLat LIKE :nom ";
}else{
		$query = $connexion->query("SELECT * FROM plant_melli_apk");
		$query->execute();
		echo json_encode($query->fetchAll(PDO::FETCH_ASSOC));
		return;
}
//Limite
if (isset($_GET["maxRows"]))
{
    $strQuery .= "LIMIT 0, :maxRows";
}
$query = $connexion->prepare($strQuery);
if (isset($_GET["plante"]))

{
    $value = $_GET["plante"]."%";
    $query->bindParam(":nom", $value, PDO::PARAM_STR);
}
//Limite
if (isset($_GET["maxRows"]))
{
    $valueRows = intval($_GET["maxRows"]);
    $query->bindParam(":maxRows", $valueRows, PDO::PARAM_INT);
}

$query->execute();

echo json_encode($query->fetchAll(PDO::FETCH_ASSOC));

?>
