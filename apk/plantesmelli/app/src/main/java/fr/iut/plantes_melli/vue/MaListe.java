package fr.iut.plantes_melli.vue;

import android.content.Intent;
import android.os.AsyncTask;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.List;

import androidx.appcompat.app.AppCompatActivity;
import fr.iut.plantes_melli.R;
import fr.iut.plantes_melli.modele.DataBaseFactory;
import fr.iut.plantes_melli.modele.Plante;
import fr.iut.plantes_melli.modele.PlanteId;


//Activité contenant la liste des plantes sauvegardées par l'utilisateur
public class MaListe extends AppCompatActivity {

    @Override
    protected void onResume() {
        super.onResume();
        setContentView(R.layout.activity_ma_liste);


        new AsyncTask(){

            //recupere la liste des plantes sauvegardées
            @Override
            protected Object doInBackground(Object[] objects) {
                List<PlanteId> listeplantes = DataBaseFactory.getDataBase().listeDAO().getAll();
                ArrayList<Plante> p = new ArrayList<>();
                for(PlanteId pid : listeplantes){
                    p.add(DataBaseFactory.getDataBase().planteDAO().getPlantById(pid.id));
                }


                return p;
            }

            @Override
            protected void onPostExecute(Object o) {
                final List<Plante> listeplantes =  (List<Plante>) o;
                ListView listView = findViewById(R.id.liste);
                PlanteAdapter adapter = new PlanteAdapter(getBaseContext(),listeplantes);
                listView.setAdapter(adapter);
                listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {

                    //On peut pas utiliser OnClickItemAccueilControleur car pas de possiblité de recuperer l'id de la plante cliquée sans onItemClick
                    @Override
                    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                        Intent intent = new Intent(getApplicationContext(),detailPlante.class);
                        intent.putExtra(accueil.PLANTE,listeplantes.get(position));
                        startActivity(intent);
                    }
                });
                super.onPostExecute(o);
            }
        }.execute();
    }
}
