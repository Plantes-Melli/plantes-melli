<?php
require_once PATH_MODELE."/DAO.php";
require_once PATH_VUE."/vue.php";
require_once "sendMail.php";
require_once PATH_CONTROLEUR."/regexUtils.php";

class ControleurCompte{
  private $modele;
  private $vue;
	private $error;
	private $mailer;

  public function __construct(){
    $this->modele = new modele();
    $this->vue= new Vue();
		$this->mailer = new Mailer();
  }

  public function route(){


    if($this->estConnecte()){
			$message =NULL;
			$listUsrs = NULL;
			if($_SESSION["compte"]->hasPrivileges(ADMINISTRATOR))
				$listUsrs = $this->modele->getAllUsers();

			$sharedLists  = $this->modele->getSharedLists($_SESSION["compte"]->getPseudo());


			if(isset($_POST["saveProfil"])){
					$pseudo = htmlspecialchars($_POST["pseudo"]);
					$prenom =  htmlspecialchars($_POST["prenom"]);
					$nom =  htmlspecialchars($_POST["nom"]);
					$email = htmlspecialchars($_POST["email"]);

					if(RegexUtils::checkPseudo($pseudo)&&
						 RegexUtils::checkNom($prenom) &&
					 	 RegexUtils::checkNom($prenom)&&
					   RegexUtils::checkMail($email)){

							 //Si l'email a changé, on renvoit un email de verification
							 	if($email != $_SESSION["compte"]->getMail()){
											$key = uniqid('',true);
											$this->modele->addVerifLink($_SESSION["compte"]->getPseudo(),$key);
											$this->mailer->verifEmail($email,$pseudo,$key);
												$message = "<p class='successMsg'> L'email sera changé lors de votre prochaine connexion après avoir validé votre email via le lien envoyé à ".$email."</p>";
								}

								$_SESSION["compte"]->changePseudo($pseudo);
								$_SESSION["compte"]->changePrenom($prenom);
								$_SESSION["compte"]->changeNom($nom);
								$this->modele->updateAccount($_SESSION["compte"]->getId(),$pseudo,$prenom,$nom,$email);


					}else{
							$message ="Un ou plusieurs champs ne sont pas valides";
					}



			}

			if(isset($_POST["mode"])){
				switch ($_POST["mode"]) {
					case 'share':
						if(isset($_POST["pseudo"]) && !empty(trim($_POST["pseudo"]))){
								$pseudo = trim($_POST["pseudo"]);
								if($pseudo == "*" || $pseudo != $_SESSION["compte"]->getPseudo() && $this->modele->existsPseudo($pseudo)){
									$this->modele->shareList($_SESSION["compte"]->getPseudo(),$pseudo);
									if($pseudo != "*")
										$this->mailer->sendListShared($_SESSION["compte"]->getPseudo(),$this->modele->getEmailByPseudo($pseudo));

									$message = "<p class='successMsg'>Liste partagée avec ".$pseudo."</p>";
								}else{
									$message = "<p class='errorMsg'>Ce pseudo n'existe pas ou n'est pas valide</p>";
								}
						}
						break;

					default:
						// code...
						break;
				}
			}

			$this->vue->afficherCompte($_SESSION["compte"],$sharedLists,$listUsrs,$message);
        return true;
    }else if(isset($_POST["pseudo"]) && isset($_POST["mdp"])){


      if($this->connexion()){
				$listUsrs = NULL;

				if($_SESSION["compte"]->hasPrivileges(ADMINISTRATOR))
					$listUsrs = $this->modele->getAllUsers();

				$sharedLists  = $this->modele->getSharedLists($_SESSION["compte"]->getPseudo());
	      $this->vue->afficherCompte($_SESSION["compte"],$sharedLists,$listUsrs);
      } else {
          $this->vue->afficherPageConnexion($this->error);
          return false;
      }
    }else{
      $this->vue->afficherPageConnexion();
      return false;
    }
  }


  //Connexion au site
  public function connexion() {

			if($this->isValidAccount($_POST["pseudo"])){
				$_SESSION["compte"] = $this->modele->getAccountByPseudo($_POST["pseudo"]);
				$_SESSION["compte"]->nomsPlantes = $this->modele->getListUser($_POST["pseudo"]);
        return $this->estConnecte();
      }
      return false;
  }

	public function isValidAccount($pseudo){
		if(!$this->modele->identifiantsValides($_POST["pseudo"], $_POST["mdp"])!=NULL){
				$this->error= "Le pseudo ou le mot de passe est Incorrect!";
				return false;
		}

		if(!$this->modele->isEmailVerified($_POST["pseudo"])){
			$this->error= "Merci de valider votre email";
			return false;
		}

		if($this->modele->isBanned($_POST["pseudo"])){
			$this->error="Votre compte à été banni par un administrateur";
			return false;
		}
		return true;
	}


  //Precondition : l'utilisateur est connecté
  //Post-condition :  les variables de session sont détruites
  public function deconnexion() {
    unset($_SESSION["compte"]);
    session_destroy();
  }
  //Vérif du USER connecté
  public function estConnecte(){
    return isset($_SESSION["compte"]);
  }
  //Return le pseudo de l'utilisateur
  public function getPseudo(){
    return $this->compte->getPseudo();
  }




}
?>
