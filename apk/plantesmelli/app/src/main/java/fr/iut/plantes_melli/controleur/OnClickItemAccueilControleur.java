package fr.iut.plantes_melli.controleur;

import android.app.Activity;
import android.content.Intent;
import android.os.AsyncTask;
import android.view.View;

import fr.iut.plantes_melli.modele.Plante;
import fr.iut.plantes_melli.modele.DataBaseFactory;
import fr.iut.plantes_melli.vue.accueil;
import fr.iut.plantes_melli.vue.detailPlante;

public class OnClickItemAccueilControleur implements View.OnClickListener {

    private int id;
    private Activity act;


    public OnClickItemAccueilControleur(Activity act, int id){
        this.act = act;
        this.id = id;
    }

    //affiche le detail d'une plante
    @Override
    public void onClick(View v) {
        new AsyncTask(){
            @Override
            protected Plante doInBackground(Object[] objects) {
                    return DataBaseFactory.getDataBase().planteDAO().getPlantById(id);
            }

            @Override
            protected void onPostExecute(Object o) {
                super.onPostExecute(o);
                Intent in = new Intent(act,detailPlante.class);
                Plante p = (Plante) o;
                in.putExtra(accueil.PLANTE,p);
                act.startActivity(in);
            }
        }.execute();

    }
}
