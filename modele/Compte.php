<?php
require_once PATH_MODELE."/DAO.php";
class Compte{
  public $id;
  public $pseudo;
  public $mail;
  public $prenom;
  public $nom;
  public $privileges;
	public $nomsPlantes;
	private $modele;


 public function __construct(){
	 //$this->modele = new modele();
 }


	public function getId(){
		return $this->id;
	}
  public function getPseudo(){
    return $this->pseudo;
  }

  public function getMail(){
    return $this->mail;
  }

  public function getPrenom(){
    return $this->prenom;
  }

  public function getNom(){
    return $this->nom;
  }

  public function getPrivileges(){
    return $this->$privilege;
  }



  public function hasPrivileges($requieredPriv){
      return $this->privileges >= $requieredPriv;
  }

  public function changePassword($pass){
    $this->password = $pass;
  }

  public function changePseudo($pseudo){
    $this->pseudo = $pseudo;
  }

  public function changeEmail($mail){
		$this->mail = $mail;
  }

	public function changeNom($nom){
		$this->nom = $nom;
	}

	public function changePrenom($prenom){
		$this->prenom = $prenom;
	}

	public function isInList($plante){
			return in_array($plante["id"],array_column($this->nomsPlantes,"id"));
	}



	public function addToList($plante){
		if(!$this->isInList($plante)){
				array_push($this->nomsPlantes,$plante);
				return true;
		}else{
			return false;
		}

	}

	public function removeFromList($plante){
		foreach ($this->nomsPlantes as $key => $mainData){
            if($mainData['id'] == $plante['id']){
                unset($this->nomsPlantes[$key]);
                break;
            }
    }


	}

	public function getListPlants(){
		if($this->nomsPlantes == NULL){
			return array();
		}else{
			return $this->nomsPlantes;
		}
	}


}



?>
