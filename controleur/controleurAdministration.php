<?php
require_once PATH_MODELE."/DAO.php";
require_once PATH_VUE."/vue.php";
require_once PATH_CONTROLEUR."/regexUtils.php";
require_once PATH_CONTROLEUR."/sendMail.php";
require_once PATH_CONTROLEUR."/controleurPlantes.php";

class ControleurAdministration{
	private $modele;
	private $vue;
	private $mailer;
	private $error;
	private $message;
	private $ctrlPlantes;

	public function __construct(){
		$this->modele = new modele();
		$this->vue= new Vue();
		$this->mailer = new Mailer();
		$this->message = "";
		$this->ctrlPlantes = new ControleurPlantes();

	}

	public function route(){

		if(!isset($_SESSION["compte"])){
			$this->vue->afficherPageConnexion();
			return;
		}


		$compte = $_SESSION["compte"];
		$sharedLists  = $this->modele->getSharedLists($compte->getPseudo());

		//Affichage du compte admin ou utilisateur simple
		if(!$compte->hasPrivileges(ADMINISTRATOR)){
			$this->vue->afficherCompte($compte,$sharedLists);
		}else{

			if(isset($_POST["mode"])){
				$mode =  $_POST["mode"];
			}else {
				$mode =  "none";
			}
			//Ce sont les modes qui correspondent aux panels de fonctionnalité de l'administrateur
			switch($mode){
				case "nameAdmin":
				$user = $_POST["users"];
				if(isset($user))
				if($this->modele->changePrivileges($user,ADMINISTRATOR)){
					$this->message = "<p class='successMsg'>".$user." à été promu</p>";
				}else{
					$this->message = "<p class='errorMsg'>".$user." n'a pas été promu</p>";
				}
				break;

				case "demoteAdmin":
				$user = $_POST["admins"];
				if(isset($user))
				if($this->modele->changePrivileges($user,USER)){
					$this->message = "<p class='successMsg'>".$user." à été rétrogradé</p>";
				}else{
					$this->message = "<p class='errorMsg'>".$user." n'a pas été rétrogradé</p>";
				}
				break;
				case "addPlant":
					$fileUploadBig = $_FILES["fileToUploadBig"];
					$fileUploadMini = $_FILES["fileToUploadMini"];
					$nomPlantFr = htmlspecialchars($_POST["nomPlantFr"]);
					$nomPlantLat = htmlspecialchars($_POST["nomPlantLat"]);
					$famille = htmlspecialchars($_POST["famille"]);
					$type = htmlspecialchars($_POST["type"]);
					$apicole = htmlspecialchars($_POST["apicole"]);
					$pollinifere = htmlspecialchars($_POST["pollinifere"]);
					$nectarifere = htmlspecialchars($_POST["nectarifere"]);
					$moyenne_kgHa = htmlspecialchars($_POST["moyenne_kgHa"]);
					$exposition = htmlspecialchars($_POST["exposition"]);
					$debut_floraison = htmlspecialchars($_POST["debut_floraison"]);
					$fin_floraison = htmlspecialchars($_POST["fin_floraison"]);
					$description = htmlspecialchars($_POST["description"]);

					if(RegexUtils::isUploadable($fileUploadBig,$this->message)&&
					   RegexUtils::isUploadable($fileUploadMini,$this->message)&&
						 RegexUtils::isText($nomPlantFr,$this->message) &&
						 RegexUtils::isText($nomPlantLat,$this->message) &&
					   RegexUtils::isText($famille,$this->message) &&
					   RegexUtils::isText($type,$this->message) &&
					   RegexUtils::isNumber($apicole,$this->message) &&
					   RegexUtils::isNumber($pollinifere,$this->message) &&
					   RegexUtils::isNumber($nectarifere,$this->message) &&
					   RegexUtils::isNumber($moyenne_kgHa,$this->message,500)&&
						 RegexUtils::verifTextArea($description,$this->message)&&
						 RegexUtils::isValidMonth($debut_floraison,$this->message)&&
						 RegexUtils::isValidMonth($fin_floraison,$this->message)&&
						 RegexUtils::isValidExpo($exposition,$this->message)
					 ){

				 	 	 $imageFileType = strtolower(pathinfo($fileUploadBig["name"],PATHINFO_EXTENSION));
						 $name = uniqid('',true).".".$imageFileType;
						 RegexUtils::upload($fileUploadBig,HOME_SITE."/vue/imagesPlantes/",$name);
						 RegexUtils::upload($fileUploadMini,HOME_SITE."/vue/miniatures/",$name);
						 $this->modele->addPlant($type,$nomPlantFr,$nomPlantLat,$famille,$name,$moyenne_kgHa,$apicole,$exposition,$nectarifere,$pollinifere,$debut_floraison,$fin_floraison,$description);
						$this->message = "<p class='successMsg'>La plante à bien été ajouté</p>";
					}
				break;
				case "sendMailAllUser":
				$mailContent = htmlspecialchars($_POST["Body"]);
				$headerContent = htmlspecialchars($_POST["Header"]);
				$array = $this->modele->getEmail();
					for($i=0; $i< sizeof($array); $i++){
						$this->mailer->sendMailAllUser($array[$i]["mail"],$headerContent,$mailContent);
						$this->message = "<p class='successMsg'>Le mail est bien envoyé</p>";
						var_dump($mailContent);
						var_dump($array[$i]["mail"]);
					}

				break;
				case "banUser":
				$user = $_POST["users"];
				if(isset($user))
				if($this->modele->banUser($user,1)){
					$this->message = "<p class='successMsg'>".$user." à été banni</p>";
				}else{
					$this->message = "<p class='errorMsg'>".$user." n'a pas été banni</p>";
				}
				break;
				case "unbanUser":
				$user = $_POST["users"];
				if(isset($user))
				if($this->modele->banUser($user,0)){
					$this->message = "<p class='successMsg'>".$user." à été gracié</p>";
				}else{
					$this->message = "<p class='errorMsg'>".$user." n'a pas été gracié</p>";
				}
				break;
			}

			$this->vue->afficherCompte($compte,$sharedLists,$this->modele->getAllUsers(),$this->message);
		}
	}

}
?>
