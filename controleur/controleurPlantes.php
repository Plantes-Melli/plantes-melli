<?php

require_once PATH_MODELE."/DAO.php";
require_once PATH_VUE."/vue.php";
require_once "controleurNavigation.php";
require_once "ControleurRecherche.php";
include_once "regexUtils.php";

class ControleurPlantes{
  private $modele;
  private $vue;
  private $ctrlRecherche;


  public function __construct(){
    $this->modele = new modele();
    $this->vue= new Vue();
    $this->ctrlNavigation =new ControleurNavigation();
    $this->ctrlRecherche = new ControleurRecherche();
  }


public function route(){
/* utilisé pour set le hash de chaque plantes
	$plantes = $this->modele->getAllPlants();
	foreach ($plantes as $key => $value) {
			$hash = $this->modele->computeHash($value["type"],
																					$value["nomFr"],
																					$value["nomLat"],
																					$value["famille"],
																					$value["image"],
																					$value["moyenne_kgHa"],
																					$value["inter_apicole"],
																					$value["exposition"],
																					$value["nectarifere"],
																					$value["pollinifere"],
																					$value["debut_floraison"],
																					$value["fin_floraison"],
																					$value["description"]
																				);
    $this->modele->setHash($value["id"],$hash);
	}
	die();
	*/

	if(!isset($_GET["id"])){
			$this->vue->afficherBarreRecherche();
	}



	$message = NULL;
  //Permet d'ajouter une plante à la liste
	if(isset($_SESSION["compte"])){

		$plante = NULL;

		if(isset($_GET["id"]))
			$plante = $this->modele->getPlanteById($_GET["id"],NULL,"lat",false);


		if(isset($_POST["ajouter"])){
			$_SESSION["compte"]->addToList($plante);
			$this->modele->addToList($_SESSION["compte"]->getPseudo(),$plante["id"]);
		}
    //Permet d'enlever une plante de la liste puis sauvgarde la nouvelle liste voulu
		if(isset($_POST["supprimer"])){
			$_SESSION["compte"]->removeFromList($plante);
			$this->modele->removeFromList($_SESSION["compte"]->getPseudo(),$plante["id"]);
			$this->modele->removePlantLocally($_SESSION["compte"]->getPseudo(),$_GET["id"]);
		}
    //Permet de modifier la liste
		if(isset($_POST["modifierListe"])){
			$this->vue->afficherModifsLocales($plante);
			$this->vue->afficherPanel("historique");
			return;
		}

		if(isset($_POST["savePlantLocally"])){
				$message = NULL;

				if(RegexUtils::isNumber($_POST["apicole"],$message)&&
					 RegexUtils::isNumber($_POST["pollinifere"],$message)&&
					 RegexUtils::isNumber($_POST["nectarifere"],$message)&&
					 RegexUtils::isNumber($_POST["moyenne_kgHa"],$message,500)&&
					 RegexUtils::isValidExpo($_POST["exposition"],$message)&&
					 RegexUtils::isValidMonth($_POST["debut_floraison"],$message)&&
					 RegexUtils::isValidMonth($_POST["fin_floraison"],$message)&&
					 RegexUtils::verifTextArea($_POST["description"],$message)&&
				 	is_numeric($_POST["id"])){

					 	$this->modele->savePlantLocally($_SESSION["compte"]->getPseudo(),
																						$_POST["id"],
																						$_POST["apicole"],
																						$_POST["pollinifere"],
																						$_POST["nectarifere"],
																						$_POST["moyenne_kgHa"],
																						$_POST["exposition"],
																						$_POST["debut_floraison"],
																						$_POST["fin_floraison"],
																						$_POST["description"]);
					 }else{
						 $this->vue->afficherModifsLocales($plante,$message);
						 $this->vue->afficherPanel("historique");
						 return;
					 }
		}

		if($_SESSION["compte"]->hasPrivileges(ADMINISTRATOR)){
			if(isset($_POST["selectType"]) ){
				switch (strtolower($_POST["selectType"])) {
					case 'supprimer':
					$plantsAr = array();
						foreach ($_POST as $key => $value) {
							if($value == "on"){
								array_push($plantsAr,$key);
							}
						}
						if(sizeof($plantsAr) > 0){
							$images = $this->modele->getImages($plantsAr);
							foreach ($images as $key => $value) {
									unlink(HOME_SITE."/vue/imagesPlantes/".$value["image"]);
									unlink(HOME_SITE."/vue/miniatures/".$value["image"]);
							}
							$r = $this->modele->removePlants($plantsAr);
							$message = $r." élément(s) traité(s)";
						}
						unset($_GET["id"]);
						break;

					case 'modifier':
						$message = NULL;
						$this->vue->afficherModifsGlobales($this->modele->getPlanteById($_GET["id"],NULL,"lat"),$message);
						$this->vue->afficherPanel("historique");
						return;
						break;
					case 'saveplantglobally':
					$message = NULL;
							$id = $_POST["id"];

							$nomPlantFr = htmlspecialchars($_POST["nomPlantFr"]);
							$nomPlantLat = htmlspecialchars($_POST["nomPlantLat"]);
							$famille = htmlspecialchars($_POST["famille"]);
							$type = htmlspecialchars($_POST["type"]);
							$apicole = htmlspecialchars($_POST["apicole"]);
							$pollinifere = htmlspecialchars($_POST["pollinifere"]);
							$nectarifere = htmlspecialchars($_POST["nectarifere"]);
							$moyenne_kgHa = htmlspecialchars($_POST["moyenne_kgHa"]);
							$exposition = htmlspecialchars($_POST["exposition"]);
							$debut_floraison = htmlspecialchars($_POST["debut_floraison"]);
							$fin_floraison = htmlspecialchars($_POST["fin_floraison"]);
							$description = htmlspecialchars($_POST["description"]);

							if(
								 RegexUtils::isText($nomPlantFr,$message) &&
								 RegexUtils::isText($nomPlantLat,$message) &&
								 RegexUtils::isText($famille,$message) &&
								 RegexUtils::isText($type,$message) &&
								 RegexUtils::isNumber($apicole,$message) &&
								 RegexUtils::isNumber($pollinifere,$message) &&
								 RegexUtils::isNumber($nectarifere,$message) &&
								 RegexUtils::isNumber($moyenne_kgHa,$message,500)&&
								 RegexUtils::verifTextArea($description,$message)&&
								 RegexUtils::isValidMonth($debut_floraison,$message)&&
								 RegexUtils::isValidMonth($fin_floraison,$message)&&
								 RegexUtils::isValidExpo($exposition,$message) &&
								 is_numeric($id)
							 ){

								 	if(isset($_FILES["fileToUploadBig"]) &&
										 isset($_FILES["fileToUploadMini"]) &&
										 $_FILES["fileToUploadBig"]["size"] >0 &&
										 $_FILES["fileToUploadMini"]["size"] >0 &&
										 RegexUtils::isUploadable($_FILES["fileToUploadBig"],$message)&&
 	 							     RegexUtils::isUploadable($_FILES["fileToUploadMini"],$message)){

										$fileUploadBig = $_FILES["fileToUploadBig"];
										$fileUploadMini = $_FILES["fileToUploadMini"];
										$name = uniqid('',true);
	 								  RegexUtils::upload($fileUploadBig,HOME_SITE."/vue/imagesPlantes/",$name);
	 								  RegexUtils::upload($fileUploadMini,HOME_SITE."/vue/miniatures/",$name);
									}else{
										$name = $_POST["defaultLink"];
									}

								 $this->modele->modifyPlant($type,$id,$nomPlantFr,$nomPlantLat,$famille,$name,$moyenne_kgHa,$apicole,$exposition,$nectarifere,$pollinifere,$debut_floraison,$fin_floraison,$description);
							   $message = "<p class='successMsg'>La plante à bien été ajouté</p>";
								 $_GET["plante"] = $nomPlantLat;


							}else{
								$this->vue->afficherModifsGlobales($this->modele->getPlanteById($_GET["plante"],NULL,"lat"),$message);
								$this->vue->afficherPanel("historique");
								return;
							}

						break;
				}
			}
		}

	}

	$pseudo  = NULL;
	if(isset($_SESSION["compte"])){
		$pseudo = $_SESSION["compte"]->getPseudo();
	}

  if(isset($_POST["langRecherche"]) && isset($_POST["nomPlant"])){
		$plante = $this->modele->getPlanteByName($_POST["nomPlant"],$pseudo,$_POST["langRecherche"],true);
		$_GET["id"] = $plante["id"];
		$this->vue->afficherDetailPlante($plante);
		$this->vue->afficherPanel("historique");
  }else if(isset($_GET["id"])){
		$this->vue->afficherDetailPlante($this->modele->getPlanteById($_GET["id"],$pseudo));
		$this->vue->afficherPanel("historique");

	}else{
      $page = isset($_POST["numPage"])?intval($_POST["numPage"]-1):0;
      $numplantes = isset($_POST["numPlantes"])?$_POST["numPlantes"]:30;
      $this->vue->afficherPanel("filtres-historique",$this->modele->getAllFilters());
			$vue = "mosaic";
      //$vue change en fonction de l'utilisateur, pour pouvoir changer de vue en forme mosaïc ou en forme de liste
			if(isset($_POST["mosaic"]))
				$vue = "mosaic";
			if(isset($_POST["list"]))
				$vue = "list";



				$filters = array();
				$nbResults = $this->modele->getNbPlants();
        //Méthode de filtre
				if(isset($_POST["filters"])){
					$filters = array(
													 "type" => $_POST["type"],
													 "famille" => $_POST["famille"],
													 "inter_apicole" => $_POST["inter_apicole"],
													 "moyenne_kgHa" =>$_POST["moyenne_kgHa"],
													 "nectarifere"=> $_POST["nectarifere"],
													 "pollinifere" => $_POST["pollinifere"],
													 "debut_floraison" => $_POST["debut_floraison"],
													 "fin_floraison" => $_POST["fin_floraison"],
													 "tri" => $_POST["tri"],
													 "triV" => $_POST["triV"]);
					$nbResults = $this->modele->nbResult($filters);
					$message = $nbResults." plante(s) trouvée(s)";
				}

			$nbPages = intval($nbResults/$numplantes);
			if($nbPages*$numplantes - $nbResults == 0){
				$nbPages--;
			}
      $this->vue->afficherListePlantes($this->modele->getListPlants($page,$numplantes,$filters),$nbPages,$vue,$message );
  }
}



function serializePlants(){


	$plantes = $this->modele->getAllPlants();

	$file = fopen(HOME_SITE."/plantes.json", "w");
	$txt = json_encode($plantes,JSON_PRETTY_PRINT);
	fwrite($file, $txt);
	fclose($file);


	$infos  =fopen(HOME_SITE."/infos.json", "w");
	$array = [
			"lastUpdate" => date("d:m:y-H:i:s"), // l'heure est en UTC (Temps universel coordonné)
			"nbPlants" => count($plantes),
			"checkSum" => md5_file(HOME_SITE."/plantes.json")
	];

	$txt = json_encode($array,JSON_PRETTY_PRINT);
	fwrite($infos, $txt);
	fclose($infos);
}


}
?>
