package fr.iut.plantes_melli.utils;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Environment;

import java.io.File;

public class ImageDecoder {

    public static Bitmap getBitMapImage(String relativePath){

        File folder = new File(Environment.getExternalStorageDirectory().getAbsolutePath(),"/plantesmelli/res/images/");
        return BitmapFactory.decodeFile(folder+relativePath);

    }
}
