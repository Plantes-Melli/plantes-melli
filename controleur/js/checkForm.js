/*  function init(){
    document.getElementById('envoyer').onsubmit = verifier ;
  }*/

  function verifier(){
    if (verifierNom() && verifierPrenom() && verifiermdp() && verifierConfMdp() && verifierEmail() && verifierPseudo()){

      return true;
    }
    else{
      //alert("Veuillez saisir tous les champs");
      return false;
      alert("Veuillez compléter tous les champs.");
    }
  }

  function verifierPrenom(){
    var regex = /([a-z,A-Z]){3,10}/;
    var prenom = document.getElementById('prenom').value;
    if(regex.test(prenom)){
        document.getElementById("prenom").style.backgroundColor = "#D3D3D3";
        document.getElementById("prenom").style.boxShadow = "0px 0px 0px #8B0000";
        return true;
    }
    else {
      document.getElementById("prenom").style.backgroundColor = "rgba(255, 0, 0, 0.2)";
      document.getElementById("prenom").style.boxShadow = "0px 0px 2px #8B0000";
      return false;
    }

  }
  function verifierNom(){
    var regex = /([a-z,A-Z]){1,25}/;
    var nom = document.getElementById('nom').value;
    if(regex.test(nom)){
        document.getElementById("nom").style.backgroundColor = "#D3D3D3";
        document.getElementById("nom").style.boxShadow = "0px 0px 0px #8B0000";
      return true;

    }
    else {

        document.getElementById("nom").style.backgroundColor = "rgba(255, 0, 0, 0.2)";
        document.getElementById("nom").style.boxShadow = "0px 0px 2px #8B0000";
      return false;
    }

  }

  function verifierPseudo(){
    var regex = /(([0-9])*([a-z,A-Z])+([0-9]*))+/;
    var pseudo = document.getElementById("pseudoForm").value;
    if (regex.test(pseudo)){
        document.getElementById("pseudoForm").style.backgroundColor = "#D3D3D3";
        document.getElementById("pseudoForm").style.boxShadow = "0px 0px 0px #8B0000";
      // évite que les données du formulaire soient envoyées au serveur
      return true;
    }
    else {

        document.getElementById("pseudoForm").style.backgroundColor = "rgba(255, 0, 0, 0.2)";
        document.getElementById("pseudoForm").style.boxShadow = "0px 0px 2px #8B0000";
      return false;
    }

  }
  function verifierEmail(){
    var regex = /.*@.*\..{2,5}/;
    var mail = document.getElementById("Email").value;
    if (regex.test(mail)){
        document.getElementById("Email").style.backgroundColor = "#D3D3D3";
        document.getElementById("Email").style.boxShadow = "0px 0px 0px #8B0000";
      // évite que les données du formulaire soient envoyées au serveur
      return true;

    }
    else {

        document.getElementById("Email").style.backgroundColor = "rgba(255, 0, 0, 0.2)";
        document.getElementById("Email").style.boxShadow = "0px 0px 2px #8B0000";
      return false;
    }
  }
  function verifiermdp(){
    var regex = /[a-zA-Z|0-9]+/;
    var mdp = document.getElementById("mdp1").value;
    if (regex.test(mdp)){
        document.getElementById("mdp1").style.backgroundColor = "#D3D3D3";
        document.getElementById("mdp1").style.boxShadow = "0px 0px 0px #8B0000";
      // évite que les données du formulaire soient envoyées au serveur
      return true;

    }
    else {

        document.getElementById("mdp1").style.backgroundColor = "rgba(255, 0, 0, 0.2)";
        document.getElementById("mdp1").style.boxShadow = "0px 0px 2px #8B0000";
      return false;
    }
  }
  function verifierConfMdp(){
    var mdp1 = document.getElementById("mdp1").value;
    console.log(mdp1);
    var mdp2 = document.getElementById("mdp2").value;
    if (mdp1==mdp2){
        document.getElementById("mdp2").style.backgroundColor = "#D3D3D3";
        document.getElementById("mdp2").style.boxShadow = "0px 0px 0px #8B0000";
      // évite que les données du formulaire soient envoyées au serveur
      return true;

    }
    else {

        document.getElementById("mdp2").style.backgroundColor = "rgba(255, 0, 0, 0.2)";
        document.getElementById("mdp2").style.boxShadow = "0px 0px 2px #8B0000";
      return false;
    }
  }

	function verifyPlantForm(){
		let form = document.getElementsByClassName("addPlantForm")[0];
		let numberInput = form.querySelectorAll("input[type='number']");
		let textInput = form.querySelectorAll("input[type='text']");
		let ret = true;
		Array.from(numberInput).forEach(function(elem){
			var limit = 5;
			if(elem.name == "moyenne_kgHa" ){
				limit = 500;
			}

				if(!isNumber(elem.value,limit)){
					elem.style.backgroundColor = "#b55c5c";
					elem.style.color = "white";
					ret  =false;
				}else{
					elem.style.backgroundColor = "white";
					elem.style.color = "black";
				}
		});

		Array.from(textInput).forEach(function(elem){
				if(!isText(elem.value)){
					elem.style.backgroundColor = "#b55c5c";
					elem.style.color = "white";
					elem.style.placeholder ="white";
					ret  =false;
				}else{
					elem.style.backgroundColor = "white";
					elem.style.color = "black";
				}
		});

		let textArea = form.getElementsByTagName("textarea")[0];
		if(!verifTextArea(textArea.value)){
			textArea.style.backgroundColor = "#b55c5c";
			textArea.style.color = "white";
			ret  =false;
		}else{
			textArea.style.backgroundColor = "white";
			textArea.style.color = "black";
		}

		return ret;
	}

	function isNumber(elem,limit = 5){
		if(isNaN(elem)){
			return false;
		}

		return elem >= 0 && elem <= limit;
	}

	function isText(elem){
		var regex = /^[a-zA-Z'\u00C0-\u017F\s]{3,}$/;
		return regex.test(elem);
	}

	function verifTextArea(elem){
		return true;
	}
