package fr.iut.plantes_melli.vue;

import android.os.AsyncTask;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.android.flexbox.AlignItems;
import com.google.android.flexbox.FlexDirection;
import com.google.android.flexbox.FlexWrap;
import com.google.android.flexbox.FlexboxLayoutManager;

import java.util.List;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.RecyclerView;
import fr.iut.plantes_melli.R;
import fr.iut.plantes_melli.modele.DataBaseFactory;
import fr.iut.plantes_melli.modele.Plante;

//Fragment qui affiche les plantes sous form de mozaique
public class MozaicFragment extends Fragment {


    public MozaicFragment() {
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        final View view = inflater.inflate(R.layout.fragment_mozaic, container, false);

        //recupere toutes les plantes
        new AsyncTask(){
            @Override
            protected List<Plante> doInBackground(Object[] objects) {
                List<Plante> p =  DataBaseFactory.getDataBase().planteDAO().getAll();

                return p;

            }

            @Override
            protected void onPostExecute(Object o) {
                super.onPostExecute(o);

                RecyclerView recyclerView = view.findViewById(R.id.mozaiqueRecyclerView);
                FlexboxLayoutManager layoutManager = new FlexboxLayoutManager();
                layoutManager.setFlexWrap(FlexWrap.WRAP);
                layoutManager.setFlexDirection(FlexDirection.ROW);
                layoutManager.setAlignItems(AlignItems.CENTER);
                recyclerView.setLayoutManager(layoutManager);
                PlanteMozaicAdapter adapter = new PlanteMozaicAdapter(getActivity(), (List<Plante>) o);
                recyclerView.setAdapter(adapter);
            }
        }.execute();

        return view;
    }
}
