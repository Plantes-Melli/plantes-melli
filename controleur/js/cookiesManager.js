function setCookie(cname, cvalue, exdays) {
	var d = new Date();
	d.setTime(d.getTime() + (exdays * 24 * 60 * 60 * 1000));
	var expires = "expires="+d.toUTCString();
	document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
}

function getCookie(cname) {
	var name = cname + "=";
	var retour = "";
	var ca = document.cookie.split(';');
	ca.forEach(function(element){
		if(element.includes(cname)){
			retour = element;
		}
	});

	return retour;
}
